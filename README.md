# Base de données d'inventaire des données et métadonnées RESIF

  * `admin`: scripts de gestion de la base. En particulier, gestion des réseaux et des droits d'accès aux données
  * `migration-database`: codes liés aux divers upgrades de la base resifInv
  * `SQL` : codes sql (schema de la base, fonctions, vues, index)

