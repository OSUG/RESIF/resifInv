-- 
-- Creation des tables pour la gestion des evenement du RAP
--
-- table d'importation des données
-- 

DROP TABLE IF EXISTS admin.assembleddata_in CASCADE;
CREATE TABLE admin.assembleddata_in(
	name	TEXT, -- event name, presently : the date of the event
	event_date TIMESTAMP WITHOUT TIME ZONE, -- date of the event
	latitude numeric(8,6), 
	longitude numeric(9,6),
	depth	numeric(9,2),
	-- magnitude numeric(3,2),
	magnitude numeric(4,2),
	magtype TEXT, -- type of magnitude
	description TEXT, -- textual localization
	agency TEXT, -- the catalog producer (renass, sismalp, etc...))
	pga FLOAT, -- Pick ground acceleration, claculated strong motion parameter
	collector TEXT, -- who is in charge of the archive production
	text_file	TEXT, -- the textual file containing some parameters
	archive_file TEXT, -- the archive file, containing all the waveforms, in different format, plus the metadata, aso
	size	BIGINT -- size of the archive
	 );
	 
DROP TABLE IF EXISTS assembleddata CASCADE;
CREATE TABLE assembleddata(
	name	TEXT, -- event name, presently : the date of the event
	event_date TIMESTAMP WITHOUT TIME ZONE, -- date of the event
	latitude numeric(8,6), 
	longitude numeric(9,6),
	depth	numeric(9,2),
	-- magnitude numeric(3,2),
	magnitude numeric(4,2),
	magtype TEXT, -- type of magnitude
	description TEXT, -- textual localization
	agency TEXT, -- the catalog producer (renass, sismalp, etc...))
	pga FLOAT, -- Pick ground acceleration, claculated strong motion parameter
	collector TEXT, -- who is in charge of the archive production
	text_file	TEXT, -- the textual file containing some parameters
	archive_file TEXT, -- the archive file, containing all the waveforms, in different format, plus the metadata, aso
	size	BIGINT -- size of the archive
	 );