-- 
-- clean_temporary_schema.sql
-- [Pequegnat] mis a jour de script : 4/11/2020
-- netoyage des schemas temporaires d'extraction : dataselect, stationrequest
-- dataselect : table datant de plus de 24h
-- station : table datant de plus de 30 minutes
-- pour modifier ces valeurs, remplacer 86000 et 18000 par les valeurs désirées.
-- 
-- dataselect
-- 	schema des noms de tables : 'de'_lower(TRANSACTION)_DATETRANSACTION_table
--		exemple : de_yipm5598_1604401303_selection
-- 			TRANSACTION = YIPM5598
--				DATETRANSACTION = `date +%s` lors de l' exécution de la commande dataselect.sh
-- les table liées aux extractions ayant démarré a J-1 sont détruites

-- juste au cas ou, dans la base de Dev. Pas necéssaire dans la base de Prod
-- CREATE SCHEMA admin;

DROP TABLE IF EXISTS admin.dataselect cascade;
CREATE TABLE admin.dataselect (db TEXT, "schema" TEXT, "table" text, "ttime" bigint, "utime" BIGINT);
INSERT INTO admin.dataselect (db, "schema","table","ttime" ,"utime")	
SELECT table_catalog,
table_schema, 
table_name,
split_part (table_name,'_',3)::BIGINT,
round(extract(epoch from now()))::BIGINT
FROM information_schema.tables 
WHERE table_schema = 'dataselect';

select count(*) from admin.dataselect ;
select count(*) from admin.dataselect  where "utime" - "ttime" > '86000'::BIGINT; 

delete from admin.dataselect where "utime" - "ttime" > '86000'::BIGINT;

DO $$ DECLARE
r RECORD;
BEGIN
FOR r IN (SELECT * FROM admin.dataselect) LOOP
raise notice 'Table to delete  : %', 'dataselect.' ||  quote_ident(r.table)  ;
EXECUTE 'DROP TABLE IF EXISTS ' || 'dataselect.' || quote_ident(r.table) || ' CASCADE';
COMMIT;
END LOOP;
END $$;
--
-- station
-- 	schema des noms de tables : 'MDE'_TRANSACTION)_DATETRANSACTION_table
--		exemple : MDE_YSLZ7340_1604499185_request
-- 			TRANSACTION = YSLZ7340
--				DATETRANSACTION = `date +%s` lors de l' exécution de la commande station.sh
--    les tables liées aux extractions ayant démarré il y a plus de 30 minutes sont détruites


DROP TABLE IF EXISTS admin.stationrequest cascade;
CREATE TABLE admin.stationrequest (db TEXT, "schema" TEXT, "table" text, "ttime" bigint, "utime" BIGINT);
INSERT INTO admin.stationrequest (db, "schema","table","ttime" ,"utime")	
SELECT table_catalog,
table_schema, 
table_name,
split_part (table_name,'_',3)::BIGINT,
round(extract(epoch from now()))::BIGINT
FROM information_schema.tables 
WHERE table_schema = 'stationrequest';

select count(*) from admin.stationrequest ;
select count(*) from admin.stationrequest  where "utime" - "ttime" > '1800'::BIGINT; 

delete from admin.stationrequest where "utime" - "ttime" > '1800'::BIGINT;

DO $$ DECLARE
r RECORD;
BEGIN
FOR r IN (SELECT * FROM admin.stationrequest) LOOP
raise notice 'Table to delete  : %', 'stationrequest.' ||  quote_ident(r.table)  ;
EXECUTE 'DROP TABLE IF EXISTS ' || 'stationrequest.' || quote_ident(r.table) || ' CASCADE';
COMMIT;
END LOOP;
END $$;

-- TODO : nettoyer si ils s'en trouve des schema temporaires d'integration. Pour le moment, je n'en vois pas

