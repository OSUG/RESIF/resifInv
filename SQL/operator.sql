
/*
Requete pour tracer les récriture
select distinct  n.network, s.station, o.operator, public."operator-xml" (s.station_id, 's', n.network, n.description) from station s, networks n, operator o where s.network_id = n.network_id and o.station_id = s.station_id and s.station_id != 0 and o.level = 's'  order by  n.network,s.station;

Requete pour obtenir la liste des operateurs, tous réseaux confondus

select distinct  public."operator-xml" (s.station_id, 's', n.network, n.description) from 
station s, networks n, operator o where s.network_id = n.network_id and o.station_id = s.station_id and s.station_id != 0 and o.level = 's'  order by  n.network,s.station;
*/

CREATE OR REPLACE FUNCTION public."operator-xml" (BIGINT, TEXT, TEXT, TEXT) RETURNS TEXT AS $$
	DECLARE
	cid ALIAS FOR $1;		-- id for a network or for a station
	clevel ALIAS FOR $2; --'n', 's'
	net ALIAS FOR $3; -- code network FDSN
	description ALIAS FOR $4;
	result TEXT; -- RETURN
	coperator public."operator"%ROWTYPE; 			-- ligne dans la table operator
	noperator public."network_operator"%ROWTYPE;  -- ligne dans la vue network_operator
	BEGIN
			result:='';								-- par défaut : liste vide
	CASE 	
		WHEN (clevel = 'n') THEN
			-- Une liste d'Operator
 		 	FOR noperator IN SELECT distinct * from public.network_operator no where no.network_id = cid::BIGINT 
 		 	-- LOOP result := noperator.xml || result ; END LOOP;
 		 		LOOP
 		 		CASE 
 		 			-- seul G transmet des Operateur de réseau
					WHEN (net = 'G' or net = 'QM' ) THEN  result :=  result || noperator.xml ;
					ELSE 
					result := '';
					END CASE;
				END LOOP;
		WHEN (clevel = 's') THEN
 		 	FOR coperator IN SELECT distinct * from public.operator o WHERE o.station_id = cid::BIGINT
 		 	-- Une liste d'Operator
 		 	-- LOOP result := result||coperator.xml ; END LOOP;
				LOOP
				CASE 
					-- On ne réécrit pas les Operator des réseaux permanents
					WHEN (net ~ '^[A-W][0-9A-Z]{0,1}$') THEN  result :=  result || coperator.xml ;
  					WHEN (coperator.xml LIKE '%ALPARRAY%') THEN
						result := '<Operator><Agency>AlpArray-FR (AlpArray)</Agency><WebSite>https://www.isterre.fr/english/research-observation/research-projects/national-research-agency-projects-1163/article/alparray-fr.html</WebSite></Operator>' ||
						'<Operator><Agency>Sismomètres fond de mer INSU-IPGP (INSU-IPGP_OBS)</Agency><WebSite>https://www.ipgp.fr/fr/sismometres-fond-de-mer-insu-ipgp</WebSite></Operator>';
					-- Alparray est traite de suite, car les operateurs proviennent aussi des OSU
					WHEN (net = 'Z3' and coperator.xml LIKE '%OCA%') THEN	
						result := '<Operator><Agency>AlpArray-FR (AlpArray)</Agency><WebSite>https://www.isterre.fr/english/research-observation/research-projects/national-research-agency-projects-1163/article/alparray-fr.html</WebSite></Operator>' ||
						'<Operator><Agency>Observatoire Fonds Marins (OCA)</Agency><WebSite>https://www.oca.eu</WebSite></Operator>';
					
					WHEN (net = 'Z3' and description LIKE 'AlpArray%') THEN	
						result := '<Operator><Agency>AlpArray-FR (AlpArray)</Agency><WebSite>https://www.isterre.fr/english/research-observation/research-projects/national-research-agency-projects-1163/article/alparray-fr.html</WebSite></Operator>' ||
						'<Operator><Agency>Parc national d''instruments sismologiques mobiles terrestres de l''INSU (RESIF-SISMOB)</Agency><WebSite>http://sismob.resif.fr</WebSite></Operator>' ;
				
					WHEN (net = 'Z3' and coperator.xml  LIKE '%SISMOB%EOST%') THEN	
						result := '<Operator><Agency>AlpArray-FR (AlpArray)</Agency><WebSite>https://www.isterre.fr/english/research-observation/research-projects/national-research-agency-projects-1163/article/alparray-fr.html</WebSite></Operator>' ||
						'<Operator><Agency>Parc national d''instruments sismologiques mobiles terrestres de l''INSU (RESIF-SISMOB)</Agency><WebSite>http://sismob.resif.fr</WebSite></Operator>' ||
						'<Operator><Agency>Ecole et Observatoire des Sciences de la Terre (EOST)</Agency><Contact><Email>network.eost@resif.fr</Email></Contact><WebSite>https://eost.unistra.fr</WebSite></Operator>'; 
					WHEN (net = 'Z3' and coperator.xml  LIKE '%SISMOB%OCA%') THEN	
						result := '<Operator><Agency>AlpArray-FR (AlpArray)</Agency><WebSite>https://www.isterre.fr/english/research-observation/research-projects/national-research-agency-projects-1163/article/alparray-fr.html</WebSite></Operator>' ||
						'<Operator><Agency>Parc national d''instruments sismologiques mobiles terrestres de l''INSU (RESIF-SISMOB)</Agency><WebSite>http://sismob.resif.fr</WebSite></Operator>' ||
						'<Operator><Agency>Observatoire de la Côte d''Azur (OCA)</Agency><Contact><Email>network.oca@resif.fr</Email></Contact><WebSite>https://www.oca.eu</WebSite></Operator>' ;
					
					WHEN (net = 'Z3' and coperator.xml  LIKE '%SISMOB%ISTERRE%') THEN	
						result := '<Operator><Agency>AlpArray-FR (AlpArray)</Agency><WebSite>https://www.isterre.fr/english/research-observation/research-projects/national-research-agency-projects-1163/article/alparray-fr.html</WebSite></Operator>' ||
						'<Operator><Agency>Parc national d''instruments sismologiques mobiles terrestres de l''INSU (RESIF-SISMOB)</Agency><WebSite>http://sismob.resif.fr</WebSite></Operator>' ||
						'<Operator><Agency>Institut des Sciences de la Terre (ISTERRE)</Agency><WebSite>https://www.isterre.fr/french/recherche-observation/observation</WebSite></Operator>';
				
					WHEN (coperator.xml  LIKE '%SISMOB%EOST%') THEN	
						result := '<Operator><Agency>Parc national d''instruments sismologiques mobiles terrestres de l''INSU (RESIF-SISMOB)</Agency><WebSite>http://sismob.resif.fr</WebSite></Operator>' ||
						'<Operator><Agency>Ecole et Observatoire des Sciences de la Terre (EOST)</Agency><Contact><Email>network.eost@resif.fr</Email></Contact><WebSite>https://eost.unistra.fr</WebSite></Operator>'; 
					WHEN (coperator.xml  LIKE '%SISMOB%OCA%') THEN	
						result := '<Operator><Agency>Parc national d''instruments sismologiques mobiles terrestres de l''INSU (RESIF-SISMOB)</Agency><WebSite>http://sismob.resif.fr</WebSite></Operator>' ||
						'<Operator><Agency>Observatoire de la Côte d''Azur (OCA)</Agency><Contact><Email>network.oca@resif.fr</Email></Contact><WebSite>https://www.oca.eu</WebSite></Operator>' ;
					WHEN (coperator.xml  LIKE '%SISMOB%ISTERRE%') THEN	
						result := '<Operator><Agency>Parc national d''instruments sismologiques mobiles terrestres de l''INSU (RESIF-SISMOB)</Agency><Contact><Email>sismob@resif.fr</Email></Contact><WebSite>http://sismob.resif.fr</WebSite></Operator>' ||
						'<Operator><Agency>Institut des Sciences de la Terre (ISTERRE)</Agency><WebSite>https://www.isterre.fr/french/recherche-observation/observation</WebSite></Operator>' ;
					
					WHEN (net = '4H' and description like '%MULHOUSE_AM%') THEN
						result := '<Operator><Agency>Ecole et Observatoire des Sciences de la Terre (EOST)</Agency><Contact><Email>network.eost@resif.fr</Email></Contact><WebSite>https://eost.unistra.fr</WebSite></Operator>' ||
						'<Operator><Agency>Mulhouse Seismo Citizen</Agency></Operator>';
			
					--	
					-- les OSU où les Observatoires en général
					-- 
					WHEN (coperator.operator LIKE '%OMP%') THEN
						result := '<Operator><Agency>Observatoire Midi-Pyrénées (OMP)</Agency><Contact><Email>network.omp@resif.fr</Email></Contact><WebSite>https://www.omp.eu</WebSite></Operator>';
					
					WHEN (net = '1N' and description LIKE '%MT_CAMPAGNE%') THEN	
						result := '<Operator><Agency>Observatoire Multidisciplinaire des Instabilités de Versants (OMIV)</Agency></Operator>' ||
						'<Operator><Agency>Parc national d''instruments sismologiques mobiles terrestres de l''INSU (RESIF-SISMOB)</Agency><Contact><Email>sismob@resif.fr</Email></Contact><WebSite>http://sismob.resif.fr</WebSite></Operator>' ;

					WHEN (coperator.operator LIKE '%OMIV%') THEN
						result := '<Operator><Agency>Observatoire Multidisciplinaire des Instabilités de Versants (OMIV)</Agency></Operator>';
					
					WHEN (coperator.operator LIKE '%OSUG%') THEN
						result := '<Operator><Agency>Observatoire des Sciences de l''Univers de Grenoble (OSUG)</Agency><Contact><Email>network.osug@resif.fr</Email></Contact><WebSite>https://www.osug.fr/missions/observation</WebSite></Operator>';
					
					WHEN (coperator.operator LIKE '%OCA%' and net NOT LIKE 'Z3' ) THEN
						result := '<Operator><Agency>Observatoire de la Côte d''Azur (OCA)</Agency><Contact><Email>network.oca@resif.fr</Email></Contact><WebSite>https://www.oca.eu</WebSite></Operator>';
						
					WHEN (coperator.operator LIKE '%EOST%') THEN
						result := '<Operator><Agency>Ecole et Observatoire des Sciences de la Terre (EOST)</Agency><Contact><Email>network.eost@resif.fr</Email></Contact><WebSite>https://eost.unistra.fr</WebSite></Operator>';
					
					WHEN (coperator.operator LIKE '%OPGC%') THEN
						result := '<Operator><Agency>Observatoire de Physique du Globe de Clermont-Ferrand (OPGC)</Agency><Contact><Email>network.opgc@resif.fr</Email></Contact><WebSite>https://opgc.uca.fr</WebSite></Operator>';
					
					WHEN (coperator.operator LIKE '%OSUNA%') THEN
						result := '<Operator><Agency>Observatoire des Sciences de l''Univers Nantes Atlantique (OSUNA)</Agency><Contact><Email>network.osuna@resif.fr</Email></Contact><WebSite>https://osuna.univ-nantes.fr/services-d-observation/rlbp/</WebSite></Operator>';
					
					WHEN (coperator.operator LIKE '%OREME%') THEN
						result := '<Operator><Agency>Observatoire de Recherche Méditerranéen de l''Environnement (OREME)</Agency><WebSite>https://data.oreme.org/observation/rlbp</WebSite></Operator>';
				
					WHEN (coperator.operator LIKE '%LSBB%') THEN
						result := '<Operator><Agency>Laboratoire Souterrain à Bas Bruit (UMS 3538 LSBB)</Agency><Contact><Email>plateforme@lsbb.eu</Email></Contact><WebSite>https://lsbb.cnrs.fr</WebSite></Operator>';
					  
					 WHEN (coperator.operator LIKE '%UBO%') THEN
						result := '<Operator><Agency>Université de Bretagne Occidentale (UBO)</Agency><WebSite>https://www.univ-brest.fr</WebSite></Operator>';
						
					WHEN (coperator.operator LIKE '%OBS%MARTINIQUE%') THEN
						result := '<Operator><Agency>Observatoire Volcanologique et Sismologique de Martinique (OVSM)</Agency><WebSite>https://www.ipgp.fr/fr/ovsm/observatoire-volcanologique-sismologique-de-martinique</WebSite></Operator>' ||
						'<Operator><Agency>Institut de Physique du Globe de Paris (IPGP)</Agency><WebSite>https://www.ipgp.fr</WebSite></Operator>';
					
					WHEN (coperator.operator LIKE '%OVSM%IPGP%') THEN
						result := 
						'<Operator><Agency>Observatoire Volcanologique et Sismologique de Martinique (OVSM)</Agency><WebSite>https://www.ipgp.fr/fr/ovsm/observatoire-volcanologique-sismologique-de-martinique</WebSite></Operator>' ||
						'<Operator><Agency>Institut de Physique du Globe de Paris (IPGP)</Agency><WebSite>https://www.ipgp.fr</WebSite></Operator>';
					
					WHEN (coperator.operator LIKE '%OVSM%') THEN
						result := 
						'<Operator><Agency>Observatoire Volcanologique et Sismologique de Martinique (OVSM)</Agency><WebSite>https://www.ipgp.fr/fr/ovsm/observatoire-volcanologique-sismologique-de-martinique</WebSite></Operator>' ||
						'<Operator><Agency>Institut de Physique du Globe de Paris (IPGP)</Agency><WebSite>https://www.ipgp.fr</WebSite></Operator>';
						
					WHEN (coperator.operator LIKE '%OVSM%CGMA%') THEN
						result := 
						'<Operator><Agency>Observatoire Volcanologique et Sismologique de Martinique (OVSM)</Agency><WebSite>https://www.ipgp.fr/fr/ovsm/observatoire-volcanologique-sismologique-de-martinique</WebSite></Operator>' ||
						'<Operator><Agency>Collectivité Territoriale de Martinique (CTM) </Agency></Operator>' ||
						'<Operator><Agency>Institut de Physique du Globe de Paris (IPGP)</Agency><WebSite>https://www.ipgp.fr</WebSite></Operator>';
					
					WHEN (coperator.operator LIKE 'Conseil%CGMA%') THEN
						result := '<Operator><Agency>Collectivité Territoriale de Martinique (CTM) </Agency></Operator>'; 
					
					WHEN (coperator.operator LIKE '%OBS%GUADELOUPE%') THEN
						result := '<Operator><Agency>Observatoire volcanologique et sismologique de Guadeloupe (OVSG)</Agency><WebSite>https://www.ipgp.fr/fr/ovsg/observatoire-volcanologique-sismologique-de-guadeloupe</WebSite></Operator>' ||
						'<Operator><Agency>Institut de Physique du Globe de Paris (IPGP)</Agency><WebSite>https://www.ipgp.fr</WebSite></Operator>';
					
					WHEN (coperator.operator LIKE '%OVSG%IPGP') THEN
						result := '<Operator><Agency>Observatoire volcanologique et sismologique de Guadeloupe (OVSG)</Agency><WebSite>https://www.ipgp.fr/fr/ovsg/observatoire-volcanologique-sismologique-de-guadeloupe</WebSite></Operator>' ||
						'<Operator><Agency>Institut de Physique du Globe de Paris (IPGP)</Agency><WebSite>https://www.ipgp.fr</WebSite></Operator>';
				
				
					WHEN (coperator.operator LIKE '%OVSG%') THEN
						result := '<Operator><Agency>Observatoire volcanologique et sismologique de Guadeloupe (OVSG)</Agency><WebSite>https://www.ipgp.fr/fr/ovsg/observatoire-volcanologique-sismologique-de-guadeloupe</WebSite></Operator>' ||
						'<Operator><Agency>Institut de Physique du Globe de Paris (IPGP)</Agency><WebSite>https://www.ipgp.fr</WebSite></Operator>';
						
					WHEN (coperator.operator LIKE '%IRD%') THEN
						-- xml := '<Operator><Agency>Institut de Recherche pour le Développement (IRD)</Agency><WebSite>https://www.ird.fr/infos-pratiques/le-web-de-l-ird/(type)/52853f645b60e9c868f9df6f590fc9a3/(nom_type)/Observatoire,%20%C3%A9quipement%20scientifique</WebSite></Operator>';
						result := '<Operator><Agency>Institut de Recherche pour le Développement (IRD)</Agency></Operator>';
					
				   WHEN (coperator.operator LIKE '%(IPGP)%') THEN
						result := '<Operator><Agency>Institut de Physique du Globe de Paris (IPGP)</Agency><WebSite>https://www.ipgp.fr</WebSite></Operator>';
					
					WHEN (coperator.operator LIKE '%IRSN%') THEN
						result := '<Operator><Agency>Institut de Recherche pour la sureté nucléaire (IRSN)</Agency><WebSite>https://www.irsn.fr/fr/larecherche/organisation/equipes/environnement/berssin/Pages/Bureau-evaluation-risques-sismiques-surete-installations.aspx</WebSite></Operator>';
				
					WHEN (coperator.operator LIKE '%CEA%') THEN
						result := '<Operator><Agency>Commissariat à l''Energie Atomique et aux énergies alternatives (CEA/DASE,LDG)</Agency><WebSite>http://www-dase.cea.fr</WebSite></Operator>';
						
					WHEN (coperator.operator LIKE '%LDG%') THEN
						result := '<Operator><Agency>Commissariat à l''Energie Atomique et aux énergies alternatives (CEA/DASE,LDG)</Agency><WebSite>http://www-dase.cea.fr</WebSite></Operator>';
					
					WHEN (coperator.operator LIKE '%BRGM%') THEN
						result := '<Operator><Agency>Bureau de Recherches Géologiques et Minières (BRGM)</Agency><WebSite>https://www.brgm.eu/</WebSite></Operator>';
						
					WHEN (coperator.operator LIKE '%Cerema%') THEN
						result := '<Operator><Agency>Centre d''études et d''expertise sur les risques, l''environnement, la mobilité et l''aménagement (CEREMA)</Agency><WebSite>https://www.cerema.fr</WebSite></Operator>';
					
					WHEN (coperator.operator LIKE '%(OVPF%IPGP)%') THEN
						result := '<Operator><Agency>Observatoire volcanologique du Piton de la Fournaise (OVPF)</Agency><WebSite>https://www.ipgp.fr/fr/ovpf/observatoire-volcanologique-piton-de-fournaise</WebSite></Operator>' ||
						'<Operator><Agency>Institut de Physique du Globe de Paris (IPGP)</Agency><WebSite>https://www.ipgp.fr</WebSite></Operator>' ;
						
					WHEN (coperator.operator LIKE '%FOURNAISE%') THEN
						result := '<Operator><Agency>Observatoire volcanologique du Piton de la Fournaise (OVPF)</Agency><WebSite>https://www.ipgp.fr/fr/ovpf/observatoire-volcanologique-piton-de-fournaise</WebSite></Operator>' ||
						'<Operator><Agency>Institut de Physique du Globe de Paris (IPGP)</Agency><WebSite>https://www.ipgp.fr</WebSite></Operator>' ;
							
					WHEN (coperator.xml LIKE '%SRC%UWI%Trinidad%TR%') THEN	
						result := '<Operator><Agency>Seismic Research Centre, The University of the West Indies</Agency><WebSite>http://uwiseismic.com</WebSite></Operator>' ||
						'<Operator><Agency>West Indies French Seismic Network (West-Indies)</Agency><WebSite>http://volobsis.ipgp.fr/networks/detail/WI/</WebSite></Operator>';

					WHEN (coperator.xml LIKE '%West%Indies%') THEN
						result := '<Operator><Agency>West Indies French Seismic Network (West-Indies)</Agency><WebSite>http://volobsis.ipgp.fr/networks/detail/WI/</WebSite></Operator>' ||
						'<Operator><Agency>Institut de Physique du Globe de Paris (IPGP)</Agency><WebSite>https://www.ipgp.fr</WebSite></Operator>';
					
					
					WHEN (coperator.operator LIKE '%(ESG)%') THEN	
						result := '<Operator><Agency>ES Géothermie (ESG)</Agency><WebSite>https://geothermie.es.fr</WebSite></Operator>';
						
					WHEN (coperator.operator LIKE '%onroche%') THEN	
						result := '<Operator><Agency>Fonroche Géothermie (Fonroche Géothermie)</Agency><WebSite>https://www.fonroche-geothermie.com</WebSite></Operator>';
						
					WHEN (coperator.xml LIKE '%Electr%rance%') THEN	
						result := '<Operator><Agency>Electricité de France (EDF)</Agency><WebSite>https://www.edf.fr</WebSite></Operator>';
						
					WHEN (coperator.xml LIKE '%GEOMAR%OBS%facility%GEOMAR_OBS%') THEN
						result := '<Operator><Agency>GEOMAR Helmholtz Centre for Ocean Research Kiel (GEOMAR)</Agency><WebSite>https://www.geomar.de/en/research/fb4/fb4-gdy/infrastructure/obsobh-systems</WebSite></Operator>' ;
					
					WHEN (coperator.xml LIKE '%DEPAS%') THEN	
						result := '<Operator><Agency>The German Instrument Pool for Amphibian Seismology (DEPAS)</Agency></Operator>';
  -- 
  -- Les réseaux Sismob
  -- 
	-- Undervolc --
					WHEN (net = 'YA' and description LIKE '%UNDERVOLC%' ) THEN
						result := '<Operator><Agency>Parc national d''instruments sismologiques mobiles terrestres de l''INSU (RESIF-SISMOB)</Agency><Contact><Email>sismob@resif.fr</Email></Contact><WebSite>http://sismob.resif.fr</WebSite></Operator>' ||
						'<Operator><Agency>Observatoire volcanologique du Piton de la Fournaise (OVPF)</Agency><WebSite>https://www.ipgp.fr/fr/ovpf/observatoire-volcanologique-piton-de-fournaise</WebSite></Operator>' ||
						'<Operator><Agency>Institut de Physique du Globe de Paris (IPGP)</Agency><WebSite>https://www.ipgp.fr</WebSite></Operator>' ;
	-- Volcarray			
				  WHEN (coperator.operator LIKE '%VOLCARRAY%') THEN
						result := '<Operator><Agency>Institut des Sciences de la Terre (ISTERRE)</Agency><WebSite>https://www.isterre.fr</WebSite></Operator>' ||
						'<Operator><Agency>Observatoire volcanologique du Piton de la Fournaise (OVPF)</Agency><WebSite>https://www.ipgp.fr/fr/ovpf/observatoire-volcanologique-piton-de-fournaise/</WebSite></Operator>' ||
						'<Operator><Agency>Institut de Physique du Globe de Paris (IPGP)</Agency><WebSite>https://www.ipgp.fr</WebSite></Operator> ' ||
						'<Operator><Agency>Service National des Observations en Volcanologie (SNOV)</Agency></Operator>';
	-- CIFALPS			
					WHEN (coperator.xml LIKE '%IGGCAS%ISTERRE%INGV%') THEN	
						result := '<Operator><Agency>Institute of Geology and Geophysics, chinese Academy of Sciences (IGGCAS)</Agency><WebSite>http://english.igg.cas.cn/</WebSite></Operator>' ||
						'<Operator><Agency>Institut des Sciences de la Terre (ISTERRE)</Agency><WebSite>https://www.isterre.fr</WebSite></Operator>' ||
						'<Operator><Agency>Istituto Nazionale di Geofisica e Vulcanologia (INGV)</Agency><WebSite>http://www.ingv.it</WebSite></Operator>' ;
		
					WHEN (net = '7H' and description like '%GRAVITAIRE%') THEN
						result := '<Operator><Agency>Institut des Sciences de la Terre (ISTERRE)</Agency><WebSite>https://www.isterre.fr/french/recherche-observation/observation</WebSite></Operator>' ||
						'<Operator><Agency>Parc national d''instruments sismologiques mobiles terrestres de l''INSU (RESIF-SISMOB)</Agency><Contact><Email>sismob@resif.fr</Email></Contact><WebSite>http://sismob.resif.fr</WebSite></Operator>' ;
				
					WHEN (net = 'YX' and description LIKE '3F-Corinth%') THEN	
						result := '<Operator><Agency>Laboratoire Géoazur (GEOAZUR)</Agency></Operator>' ||
						'<Operator><Agency>Parc national d''instruments sismologiques mobiles terrestres de l''INSU (RESIF-SISMOB)</Agency><Contact><Email>sismob@resif.fr</Email></Contact><WebSite>http://sismob.resif.fr</WebSite></Operator>' ;
			-- Les nodes
					WHEN (net = '6J' and description like '%ARGOSTOLI%') THEN
						result := '<Operator><Agency>Parc national d''instruments sismologiques mobiles terrestres de l''INSU (RESIF-SISMOB Nodes)</Agency><Contact><Email>sismob@resif.fr</Email></Contact><WebSite>http://sismob.resif.fr</WebSite></Operator>' ;
					
					WHEN (net = 'ZO' and description like '%Argentière%') THEN
						result := '<Operator><Agency>Parc national d''instruments sismologiques mobiles terrestres de l''INSU (RESIF-SISMOB Nodes)</Agency><Contact><Email>sismob@resif.fr</Email></Contact><WebSite>http://sismob.resif.fr</WebSite></Operator>' ;
				
					WHEN (net = 'ZK' and description like '%Dense%nodal%') THEN
						result := '<Operator><Agency>Parc national d''instruments sismologiques mobiles terrestres de l''INSU (RESIF-SISMOB Nodes)</Agency><Contact><Email>sismob@resif.fr</Email></Contact><WebSite>http://sismob.resif.fr</WebSite></Operator>' ;
				
					WHEN (net = 'XG' and description like '%Vallunden%') THEN
						result := '<Operator><Agency>Parc national d''instruments sismologiques mobiles terrestres de l''INSU (RESIF-SISMOB Nodes)</Agency><Contact><Email>sismob@resif.fr</Email></Contact><WebSite>http://sismob.resif.fr</WebSite></Operator>' ;
					
					WHEN (net = 'Z7' and description like '%Rittershoffen%') THEN
						result := '<Operator><Agency>Parc national d''instruments sismologiques mobiles terrestres de l''INSU (RESIF-SISMOB Nodes)</Agency><Contact><Email>sismob@resif.fr</Email></Contact><WebSite>http://sismob.resif.fr</WebSite></Operator>' ;

					WHEN (net = 'ZE' and description like '%Quito%') THEN
						result := '<Operator><Agency>Parc national d''instruments sismologiques mobiles terrestres de l''INSU (RESIF-SISMOB Nodes)</Agency><Contact><Email>sismob@resif.fr</Email></Contact><WebSite>http://sismob.resif.fr</WebSite></Operator>' ;
			
					WHEN (net = '1F' and description like '%RESOLVE%') THEN
						result := '<Operator><Agency>Parc national d''instruments sismologiques mobiles terrestres de l''INSU (RESIF-SISMOB Nodes)</Agency><Contact><Email>sismob@resif.fr</Email></Contact><WebSite>http://sismob.resif.fr</WebSite></Operator>' ;
			
					WHEN (coperator.operator LIKE '%RESIF%SISMOB%Nodes%') THEN	
						result := '<Operator><Agency>Parc national d''instruments sismologiques mobiles terrestres de l''INSU (RESIF-SISMOB Nodes)</Agency><Contact><Email>sismob@resif.fr</Email></Contact><WebSite>http://sismob.resif.fr</WebSite></Operator>' ;
				-- le reste est du SISMOB !!!
					WHEN (net ~ '^[XYZ0-9]') THEN
						result := '<Operator><Agency>Parc national d''instruments sismologiques mobiles terrestres de l''INSU (RESIF-SISMOB)</Agency><Contact><Email>sismob@resif.fr</Email></Contact><WebSite>http://sismob.resif.fr</WebSite></Operator>' ;
					ELSE 
					result := result || coperator.xml ;
					END CASE;
				END LOOP;
		ELSE result := ''; 
		END CASE ;
		RETURN result;
END;
$$ LANGUAGE "plpgsql";


CREATE OR REPLACE FUNCTION public."network-operator-xml" (BIGINT, TEXT, TEXT, TEXT) RETURNS TEXT AS $$
DECLARE
        cid ALIAS FOR $1;               -- id for a network
        clevel ALIAS FOR $2; --'n'
        net ALIAS FOR $3; -- code network FDSN
        description ALIAS FOR $4;
        xml TEXT; -- RETURN
        coperator public.operator%ROWTYPE;
        BEGIN
                        xml:='';
                        FOR coperator IN SELECT distinct o.network_id, o.xml FROM public.operator o WHERE o.network_id  =  cid and  o.level = 'n'
                                -- LOOP xml := xml||coperator.xml ; END LOOP;
                                LOOP
                                CASE
                                        WHEN (net = 'PF') THEN
                                                xml := '<Operator><Agency>Observatoire volcanologique du Piton de la Fournaise (OVPF)</Agency><WebSite>https://www.ipgp.fr/fr/ovpf/observatoire-volcanologique-piton-de-fournaise</WebSite></Operator>' ||
                                                '<Operator><Agency>Institut de Physique du Globe de Paris (IPGP)</Agency><WebSite>https://www.ipgp.fr</WebSite></Operator>' ;
                                        WHEN (net = 'MQ') THEN
                                                xml := '<Operator><Agency>Observatoire Volcanologique et Sismologique de Martinique (OVSM)</Agency><WebSite>https://www.ipgp.fr/fr/ovsm/observatoire-volcanologique-sismologique-de-martinique</WebSite></Operator>' ||
                                                '<Operator><Agency>Institut de Physique du Globe de Paris (IPGP)</Agency><WebSite>https://www.ipgp.fr</WebSite></Operator>';
                                        WHEN (net = 'GL') THEN
                                                xml := '<Operator><Agency>Observatoire volcanologique et sismologique de Guadeloupe (OVSG)</Agency><WebSite>https://www.ipgp.fr/fr/ovsm/observatoire-volcanologique-sismologique-de-guadeloupe</WebSite></Operator>' ||
                                                '<Operator><Agency>Institut de Physique du Globe de Paris (IPGP)</Agency><WebSite>https://www.ipgp.fr</WebSite></Operator>';
                                        WHEN (net = 'WI') THEN
                                                xml := '<Operator><Agency>West Indies French Seismic Network (West-Indies)</Agency><WebSite>http://volobsis.ipgp.fr/networks/detail/WI/</WebSite></Operator>' ||
                                                '<Operator><Agency>Institut de Physique du Globe de Paris (IPGP)</Agency><WebSite>https://www.ipgp.fr</WebSite></Operator>';

                                        WHEN (net = 'G' ) THEN  xml := coperator.xml || xml;
                                        WHEN (net = 'RA') THEN
                                                xml := '<Operator><Agency>Réseau Accélérométrique Permanent (RESIF-RAP)</Agency><Contact><Email>rap@resif.fr</Email></Contact><WebSite>http://rap.resif.fr</WebSite></Operator>' ;
                                        -- A compléter si besoin ---
                                        ELSE
                                        xml := '';
                                END CASE;
                        END LOOP;
                RETURN xml;
END;
$$ LANGUAGE "plpgsql";

-- REFRESH MATERIALIZED VIEW CONCURRENTLY ws_common ;
-- REFRESH MATERIALIZED VIEW CONCURRENTLY ws_network_xml;
-- REFRESH MATERIALIZED VIEW CONCURRENTLY ws_station_xml;

-- quelles sont les réseaux star_year station que l'on ne retrouve PAS dans operator  sans opérateurs ?

-- select 'Liste des stations sans operator :';

-- select distinct n.network, n.start_year, s.station from networks n, station s, operator o where  n.network_id=s.network_id and s.station_id NOT IN (select distinct operator.station_id from operator where operator.level = 's' and operator.station_id != 0) GROUP BY n.network, n.start_year, s.station ORDER BY n.network, n.start_year, s.station;
