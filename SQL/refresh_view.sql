/*
UPDATE networks SET nb_station = sum_station(network_id) FROM temp_metadata_integration_sequence
where network_id = temp_metadata_integration_sequence.value and temp_metadata_integration_sequence.name = 'network_id' ;

UPDATE station SET nb_channel = sum_channel(station_id) FROM temp_metadata_integration_sequence
where station_id = temp_metadata_integration_sequence.value and temp_metadata_integration_sequence.name = 'station_id';
*/

update networks set nb_station = sum_station(network_id);
update station set nb_channel = sum_channel(station_id);


DROP INDEX if exists ix_ws_station_text CASCADE;
DROP INDEX if exists ix_ws_channel_text CASCADE;
DROP INDEX if exists ix_ws_network_text CASCADE;
DROP INDEX if exists ix_ws_network_xml CASCADE;
DROP INDEX if exists ix_ws_station_xml CASCADE;
DROP INDEX if exists ix_ws_channel_xml CASCADE;

CREATE UNIQUE INDEX ix_ws_station_text on ws_station_text(channel_id);
CREATE UNIQUE INDEX ix_ws_channel_text on ws_channel_text (channel_id);
CREATE UNIQUE INDEX ix_ws_network_text on  ws_network_text (channel_id);
CREATE UNIQUE INDEX ix_ws_network_xml on ws_network_xml (channel_id);
CREATE UNIQUE INDEX ix_ws_station_xml on ws_station_xml (channel_id);
CREATE UNIQUE INDEX ix_ws_channel_xml on ws_channel_xml(channel_id);

REFRESH MATERIALIZED VIEW CONCURRENTLY channel_light ;
-- REFRESH MATERIALIZED VIEW CONCURRENTLY channel_with_record; 
REFRESH MATERIALIZED VIEW CONCURRENTLY ws_common ;
REFRESH MATERIALIZED VIEW CONCURRENTLY ws_station_text;
REFRESH MATERIALIZED VIEW CONCURRENTLY ws_channel_text;
REFRESH MATERIALIZED VIEW CONCURRENTLY ws_network_text;
REFRESH MATERIALIZED VIEW CONCURRENTLY ws_network_xml;
REFRESH MATERIALIZED VIEW CONCURRENTLY ws_station_xml;
REFRESH MATERIALIZED VIEW CONCURRENTLY ws_channel_xml; 

select * from ws_network_text where nettext is null;
select * from ws_station_text where statext is null;
select * from ws_channel_text where chatext is null;

select ws_common.* from ws_common, ws_channel_text w where w.chatext is null and ws_common.channel_id = w.channel_id ;
select ws_common.* from ws_common, ws_station_text w where w.statext is null and ws_common.channel_id = w.channel_id ;
select ws_common.* from ws_common, ws_channel_xml w where w.xmlchan1 is null and ws_common.channel_id = w.channel_id ;
