-- resifinv-fonction.sql
-- fonctions plpgsql pour la base de données resifInv
-- [Péquegnat 2019] Création du fichier pour la V3 de la base de données resifInv (station XML 1.1)
-- [Péquegnat 2020] Vérification des commentaires 
-- [Péquegnat sept 2020] Fonctions appelées lors de l: l'intégration des métadonnées, l'intégration des données, l'extraction des données, l'extraction des métadonnées, et la construction/mise a jour des vues
-----------------------------------------
\set ON_ERROR_STOP 1
-- 
-- utilitaires
-- 
CREATE OR REPLACE FUNCTION public.isyear(text) RETURNS boolean AS '
SELECT $1 ~ ''^[0-9][0-9][0-9][0-9]$'' '
  LANGUAGE 'sql';

CREATE OR REPLACE FUNCTION public.isjulianday(text) RETURNS boolean AS '
SELECT $1 ~ ''^[0-9][0-9][0-9]$'' '
  LANGUAGE 'sql';

CREATE OR REPLACE function public."reverse"(TEXT) returns TEXT as $$
select case when length($1)>0
then substring($1, length($1), 1) || reverse(substring($1, 1, length($1)-1))
else '' end $$ language sql immutable strict;

CREATE OR REPLACE FUNCTION public."basename" (TEXT, VARCHAR(1))  RETURNS TEXT AS '
   declare
   fullname alias for $1;
   separator alias for $2;
   basename TEXT; -- resultat
   BEGIN
   select (SUBSTR(fullname, (length(fullname)) - position(separator in reverse(fullname)) + 2,length(fullname))) INTO basename;
   RETURN basename;
   END;'
LANGUAGE 'plpgsql';
-- génération d une chaine aleatoire
Create or replace function public.random_string(length integer) returns text as
$$
declare
  chars text[] := '{0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z}';
  result text := '';
  i integer := 0;
begin
  if length < 0 then
    raise exception 'Given length cannot be less than 0';
  end if;
  for i in 1..length loop
    result := result || chars[1+random()*(array_length(chars, 1)-1)];
  end loop;
  return result;
end;
$$ language plpgsql;

--===========================================================================
-- Calcul de la distance epicentrale en DEGRES
-- "distanceEpicentrale_deg"(<latitude 1>, <longitude 1>, <latitude 2>, <longitude 2>)
--  renvoie un float8
-- la terre n'est pas ronde, d'où les facteurs de correction....
--===========================================================================
CREATE OR REPLACE FUNCTION public."distance_epicentrale_deg"(DOUBLE PRECISION, DOUBLE PRECISION, DOUBLE PRECISION, DOUBLE PRECISION) RETURNS DOUBLE PRECISION AS '
   DECLARE
      p_lati ALIAS FOR $1;            -- Latitude du point
      p_long ALIAS FOR $2;            -- Longitude du point
      s_lati ALIAS FOR $3;            -- Latitude de la station
      s_long ALIAS FOR $4;            -- Longitude de la station
      ss_dist FLOAT8;                  -- Distance sur le grand cercle en degres, default 0
   BEGIN
      ss_dist := 0;
      ss_dist := (180/pi())*            -- Mise en Degre a partir des radians
            (acos(cos(
            atan(tan(radians(p_lati))*0.993305)   -- L1
            )*cos(
            atan(tan(radians(s_lati))*0.993305)   -- L2
            )*cos(
            radians(p_long) - radians(s_long)   -- D1
            )+sin(
            atan(tan(radians(p_lati))*0.993305)   -- L1
            )*sin(
            atan(tan(radians(s_lati))*0.993305)   -- L2
            )));
      RETURN ss_dist ;
   END;'
LANGUAGE 'plpgsql';

--===========================================================================
-- recuperation de l'id d'une transaction à partir de son code (unique)
-- 
CREATE OR REPLACE FUNCTION public.get_transaction_id (TEXT) RETURNS BIGINT AS $$
   DECLARE
   incode ALIAS FOR $1;      -- code identifiant de la transaction
   id BIGINT ;               -- id associé
   idt BIGINT;
   BEGIN
   idt := (select DISTINCT t.transaction_id
         FROM public.transaction_integration t
         WHERE t.code = incode);
   IF (idt IS NULL) THEN id:=0; ELSE id:=idt; END IF;
   RETURN id;
   END; $$
LANGUAGE 'plpgsql';
-------------------
-- recuperation de l'id d'une geology à partir de son code
-- 
CREATE OR REPLACE FUNCTION public.get_geology_id (TEXT) RETURNS INTEGER AS $$
   DECLARE
   geolcode ALIAS FOR $1;      -- code identifiant la geology
   id INTEGER ;               -- id associé
   idt INTEGER;
   BEGIN
   idt := (select DISTINCT g.geology
         FROM public.geology g
         WHERE g.geology = geolcode);
   IF (idt IS NULL) THEN id:=0; ELSE id:=idt; END IF;
   RETURN id;
   END; $$
LANGUAGE 'plpgsql';

-- recuperation de l'id d'une vault à partir de son code
-- 
CREATE OR REPLACE FUNCTION public.get_vault_id (TEXT) RETURNS INTEGER AS $$
   DECLARE
   vault ALIAS FOR $1;      -- code identifiant la geology
   id INTEGER ;               -- id associé
   idt INTEGER;
   BEGIN
   idt := (select DISTINCT v.vault
         FROM public.vault v
         WHERE v.vault = vault);
   IF (idt IS NULL) THEN id:=0; ELSE id:=idt; END IF;
   RETURN id;
   END; $$
LANGUAGE 'plpgsql';
---------------------------------------------------------------
-- recuperation de l'id d'une station à partir de son code et de l'id de son réseau
-- 
CREATE OR REPLACE FUNCTION public.get_station_id (TEXT,BIGINT) RETURNS BIGINT AS $$
   DECLARE
   sta ALIAS FOR $1;      -- code identifiant de la transaction
   nid ALIAS FOR $2;      -- id du reseau dans station
   id BIGINT ;               -- id associé
   idt BIGINT;
   BEGIN
   idt := (select DISTINCT s.station_id
         FROM public.station s
         WHERE s.code = sta and s.network_id = nid);
   IF (idt IS NULL) THEN id:=0; ELSE id:=idt; END IF;
   RETURN id;
   END; $$
LANGUAGE 'plpgsql';

---------------------------------------------------------------
-- recuperation de l'id d'une station à partir de son code et du code de son réseau
-- 
CREATE OR REPLACE FUNCTION public.get_station_id_text (TEXT,TEXT) RETURNS BIGINT AS $$
   DECLARE
   net ALIAS FOR $1;      -- code réseau
   stat ALIAS FOR $2;      -- code station
   id BIGINT ;               -- id associé
   idt BIGINT;
   BEGIN
   idt := (select DISTINCT s.station_id
            FROM public.networks n, public.station s
            WHERE n.network = net and s.network_id = n.network_id and s.station = stat);
   IF (idt IS NULL) THEN id:=0; ELSE id:=idt; END IF;
   RETURN id;
   END; $$
LANGUAGE 'plpgsql';

---------------------------------------------------------------
-- recuperation de l'id d'un réseau à partir de son code et de sa date de début
--  
CREATE OR REPLACE FUNCTION public.get_network_id (TEXT,TIMESTAMP WITHOUT TIME ZONE) RETURNS BIGINT AS $$ 
   DECLARE
   net ALIAS FOR $1;      -- code réseau
   start ALIAS FOR $2;      -- date de debut
   id BIGINT ;               -- id associé
   idt BIGINT;
   BEGIN
   idt := (select DISTINCT n.network_id
         FROM public.networks n
         WHERE n.network = net and
         start between n.starttime and n.endtime );
   IF (idt IS NULL) THEN id:=0; ELSE id:=idt; END IF;
   RETURN id;
   END; $$
LANGUAGE 'plpgsql';
---------------------------------------------------------------
-- renvoie le fullnet : NN[YYYY] d'un réseau à partir de son id.
-- 
CREATE OR REPLACE FUNCTION public."fullnet"(BIGINT) RETURNS TEXT AS $$
DECLARE
      netid ALIAS FOR $1;            --  id du reseau
      netcode TEXT;
   BEGIN
      SELECT n.network FROM public.networks n where n.network_id = netid INTO netcode;
      CASE
      -- [O-9XYZ] ==> reseau temporaire
         WHEN (netcode NOT in ('ND', 'FR', 'RA', 'FR', 'RD', 'CL', 'G', 'PF', 'WI', 'MQ', 'GL', 'MT'))
            THEN netcode := netcode || networks.start_year FROM public.networks where networks.network_id = netid ;
      ELSE
            netcode := netcode;
      END CASE;
      RETURN netcode ;
   END;
$$ LANGUAGE 'plpgsql';

-- select fullnet (45);
-- -- renvoie le fullnet d'un réseau à partir d'un code et d'une année (TEXT) (comprise entre les dates de début et de fin du réseau)
--
CREATE OR REPLACE FUNCTION public."fullnet_with_year"(TEXT, TEXT) RETURNS TEXT AS $$
DECLARE
      netcode ALIAS FOR $1;
      yeartext ALIAS FOR $2;
      fullnet TEXT; -- return value
   BEGIN
      CASE
      -- [O-9XYZ] ==> reseau temporaire
         WHEN (netcode ~ '^[A-W]') THEN fullnet := netcode;
         ELSE
            fullnet := netcode || networks.start_year::TEXT FROM public.networks where netcode = networks.network and yeartext::INTEGER BETWEEN networks.start_year and networks.end_year ;
      END CASE;
      RETURN fullnet ;
   END;
$$ LANGUAGE 'plpgsql';
--
---------------------------------------------------------------------
-- en base, les locations vides sont stockés sous la forme '--'. 
-- en output, ils sont replacés en ''
-- 
CREATE OR REPLACE FUNCTION public.format_location (TEXT)  RETURNS text AS $$
   DECLARE
   loc ALIAS FOR $1;      -- location
   locout TEXT;
   BEGIN
      IF (loc ='--') THEN locout:=''; ELSE locout:=loc; END IF;
   RETURN locout;
   END; $$
LANGUAGE 'plpgsql';

-- 
-- 
--===================================================================================================================================
-- longueur des listes d'elements (operator, comment, identifier, external_reference, station, channel, equipment, type_channel
-- #stations dans un réseau
--
CREATE OR REPLACE FUNCTION public.sum_station(BIGINT) RETURNS INTEGER AS $$
   DECLARE
      netid ALIAS FOR $1;            --  id du reseau
      nbstat INTEGER ;
   BEGIN
      nbstat := 0;
      SELECT COUNT(DISTINCT s.station_id)
            FROM public.station s
            WHERE s.network_id = netid
      INTO nbstat;
      RETURN nbstat ;
   END;
$$ LANGUAGE 'plpgsql';
--select sum_station (45);
--
-- #channels dans une station
--
CREATE OR REPLACE FUNCTION public.sum_channel(BIGINT) RETURNS INTEGER AS $$
   DECLARE
      staid ALIAS FOR $1;            --  id de la station
      nbchan INTEGER ;
   BEGIN
      nbchan := 0;
      SELECT COUNT(DISTINCT c.channel_id)
            FROM public.channel c
            WHERE c.station_id = staid
      INTO nbchan;
      RETURN nbchan ;
   END;
$$ LANGUAGE 'plpgsql';

---
-- report 20201022_migration_fix_output_nb_elements.sql
-- CP, 1/11/2020
--
-- fonction publique pour compter le nombre de station par réseau selon leur présence dans ws_common (stations 'complètes')
-- on prend en compte le champ station et non le champ station_id, ce qui signifie que on affiche le nombre de stations effectives,
-- toute 'epoch de station confindues'
-- déclaration reportée dans resifinv/SQL/resif-fonction.sql
--
CREATE OR REPLACE FUNCTION public.sum_station_ws_common(BIGINT) RETURNS INTEGER AS $$
   DECLARE
      netid ALIAS FOR $1;            --  id du reseau
      nbstat INTEGER ;
   BEGIN
      nbstat := 0;
      SELECT COUNT(DISTINCT w.station)
            FROM public.ws_common w
            WHERE w.network_id = netid
      INTO nbstat;
      RETURN nbstat ;
   END;
$$ LANGUAGE 'plpgsql';

--
-- fonction publique pour compter le nombre de channels par station selon leur présence dans ws_common (channels 'complets')
-- déclaration reportée dans resifinv/SQL/resif-fonction.sql

CREATE OR REPLACE FUNCTION public.sum_channel_ws_common(BIGINT) RETURNS INTEGER AS $$
   DECLARE
      staid ALIAS FOR $1;            --  id de la station
      nbchan INTEGER ;
   BEGIN
      nbchan := 0;
      SELECT COUNT(DISTINCT w.channel_id)
            FROM public.ws_common w
            WHERE w.station_id = staid
      INTO nbchan;
      RETURN nbchan ;
   END;
$$ LANGUAGE 'plpgsql';

--
-- select sum_channel (45);
--
-- #comments, selon level
--
CREATE OR REPLACE FUNCTION public.sum_comment(BIGINT, TEXT) RETURNS INTEGER AS $$
   DECLARE
   cid ALIAS FOR $1;
   clevel ALIAS FOR $2;
   sum INTEGER;
   BEGIN
      sum := 0;
      CASE
      WHEN (clevel = 'n')  THEN
           SELECT COUNT(DISTINCT c.comment_id) FROM public.comment c where c.level = 'n' and c.network_id = cid into sum;
       WHEN (clevel = 's')  THEN
           SELECT COUNT(DISTINCT c.comment_id) FROM public.comment c where c.level = 's' and c.station_id = cid into sum;
      ELSE
         SELECT COUNT(DISTINCT c.comment_id) FROM public.comment c where c.level = 'c' and c.channel_id = cid into sum;
      END CASE;
      RETURN sum ;
   END;
$$ LANGUAGE 'plpgsql';
--
-- select sum_comment (45,'c');
-- select sum_comment (45,'s');
--
-- #identifiers, selon level
--
CREATE OR REPLACE FUNCTION public.sum_identifier(BIGINT, TEXT) RETURNS INTEGER AS $$
   DECLARE
   cid ALIAS FOR $1;
   clevel ALIAS FOR $2;
   sum INTEGER;
   BEGIN
      sum := 0;
      CASE
         WHEN (clevel = 'n')  THEN
              SELECT COUNT(DISTINCT c.identifier_id) FROM public.identifier c where c.level = 'n' and c.network_id = cid into sum;
          WHEN (clevel = 's')  THEN
              SELECT COUNT(DISTINCT c.identifier_id) FROM public.identifier c where c.level = 's' and c.station_id = cid into sum;
           ELSE
            SELECT COUNT(DISTINCT c.identifier_id) FROM public.identifier c where c.level = 'c' and c.channel_id = cid into sum;
      END CASE;
      RETURN sum ;
   END;
$$ LANGUAGE 'plpgsql';
--select sum_identifier (29,'n');
--
-- #equipment, selon level
--
CREATE OR REPLACE FUNCTION public.sum_equipment(BIGINT, TEXT) RETURNS INTEGER AS $$
   DECLARE
   cid ALIAS FOR $1;
   clevel ALIAS FOR $2;
   sum INTEGER;
   BEGIN
      sum := 0;
      CASE
          WHEN (clevel = 's')  THEN
              SELECT COUNT(DISTINCT c.equipment_id)  FROM public.equipment c where c.level = 's' and c.station_id = cid into sum;
           ELSE
            SELECT COUNT(DISTINCT c.equipment_id)  FROM public.equipment c where c.level = 'c' and c.channel_id = cid into sum;
      END CASE;
      RETURN sum ;
   END;
$$ LANGUAGE 'plpgsql';
--
-- select sum_equipment (29,'n');
--
--
-- #operator, selon level
-- cette fonction doit etre revue, mais elle n'est pas utilisée pour le moment....
-- CP 11/1/2020
--
CREATE OR REPLACE FUNCTION public.sum_operator(BIGINT, TEXT) RETURNS INTEGER AS $$
   DECLARE
   cid ALIAS FOR $1;
   clevel ALIAS FOR $2;
   sum INTEGER;
   BEGIN
      sum := 0;
      CASE
         WHEN (clevel = 'n')  THEN
              SELECT COUNT(DISTINCT c.operator_id) FROM public.operator c where c.level = 'n' and c.network_id = cid into sum;
          WHEN (clevel = 's')  THEN
              SELECT COUNT(DISTINCT c.operator_id) FROM public.operator c where c.level = 's' and c.station_id = cid into sum;
      END CASE;
      RETURN sum ;
   END;
$$ LANGUAGE 'plpgsql';
-- select sum_operator (29,'s');
--
-- #external_references, selon level
--
CREATE OR REPLACE FUNCTION public.sum_reference(BIGINT, TEXT) RETURNS INTEGER AS $$
   DECLARE
   cid ALIAS FOR $1;
   clevel ALIAS FOR $2;
   sum INTEGER;
   BEGIN
      sum := 0;
      CASE
         WHEN (clevel = 'n')  THEN
              SELECT COUNT(DISTINCT c.external_reference_id) FROM public.external_reference c where c.level = 'n' and c.network_id = cid into sum;
          WHEN (clevel = 's')  THEN
              SELECT COUNT(DISTINCT c.external_reference_id) FROM public.external_reference c where c.level = 's' and c.station_id = cid into sum;
           ELSE
              SELECT COUNT(DISTINCT c.external_reference_id) FROM public.external_reference c where c.level = 'c' and c.channel_id = cid into sum;
      END CASE;
      RETURN sum ;
   END;
$$ LANGUAGE 'plpgsql';
-- select sum_reference (35,'n');
--
-- #type_channel, selon level
--
CREATE OR REPLACE FUNCTION public.sum_type_channel(BIGINT) RETURNS INTEGER AS $$
   DECLARE
   cid ALIAS FOR $1;
   sum INTEGER;
   BEGIN
      sum := 0;
      SELECT COUNT(DISTINCT t.type_channel_at_channel_id) FROM public.type_channel_at_channel t where t.channel_id = cid into sum;
      RETURN sum ;
   END;
$$ LANGUAGE 'plpgsql';
-- select sum_type_channel (35);
-- 
-- =====================================================================================================================================
-- formatage xml des informations
-- 
CREATE OR REPLACE FUNCTION public."latitude-xml" (BIGINT, TEXT) RETURNS TEXT AS $$
   DECLARE
   cid ALIAS FOR $1;
   clevel ALIAS FOR $2;
   xml TEXT;
   BEGIN
   xml := '';
   case WHEN (clevel = 's') THEN
         xml := l.xml FROM public.latitude l where l.latitude_id = cid and l.level = 's'  ;
      ELSE
         xml :=  l.xml FROM public.latitude l where l.latitude_id = cid and l.level = 'c';
   END CASE;
   RETURN xml;
   END; $$
LANGUAGE 'plpgsql';

-- select "latitude-xml"(100,'s');

CREATE OR REPLACE FUNCTION public."distance-xml" (BIGINT, TEXT, TEXT) RETURNS TEXT AS $$
   DECLARE
   cid ALIAS FOR $1;      -- id for a network, a station or a channel
   clevel ALIAS FOR $2; --'n', 's', 'c'
   ctype ALIAS for $3;
   xml TEXT; -- RETURN
   cdistance public."distance"%ROWTYPE;
   BEGIN
   xml:='';
   CASE
      WHEN (clevel = 's' and ctype = 'e' and cid = 0) THEN
              xml := '<Elevation>0</Elevation> ';
       WHEN (clevel = 'c' and ctype = 'e' and cid = 0) THEN
              xml := '<Elevation>0</Elevation> ';
       WHEN (clevel = 'c' and ctype = 'd' and cid = 0) THEN
              xml := '<Depth>0</Depth> ';
      WHEN (clevel = 's' and ctype = 'e' and cid != 0) THEN
              xml := d.xml FROM public.distance d where level = 's' and distance_id = cid and ctype = 'e';
      WHEN (clevel = 'c' and ctype = 'e' and cid != 0) THEN
              xml := d.xml FROM public.distance d where level = 'c' and distance_id = cid and ctype = 'e';
      WHEN (clevel = 'c' and ctype = 'd' and cid != 0) THEN
            xml := d.xml FROM public.distance d where level = 'c' and distance_id = cid and ctype = 'd';
      ELSE xml := '';

      END CASE;
      RETURN xml;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION public."longitude-xml" (BIGINT, TEXT) RETURNS TEXT AS $$
   DECLARE
   cid ALIAS FOR $1;
   clevel ALIAS FOR $2;
   xml TEXT;
   BEGIN
   case WHEN (clevel = 's') THEN
      xml := l.xml FROM public.longitude l where l.level = 's'  and l.longitude_id = cid ;
      ELSE xml := l.xml FROM public.longitude l where l.level = 'c'  and l.longitude_id = cid ;
      END CASE;
   RETURN xml;
   END; $$
LANGUAGE 'plpgsql';

-- select "longitude-xml"(100,'s');

CREATE OR REPLACE FUNCTION public."elevation-xml" (BIGINT, TEXT) RETURNS TEXT AS $$
   DECLARE
   cid ALIAS FOR $1;
   clevel ALIAS FOR $2;
   xml TEXT;
   BEGIN
   xml := '<Elevation>0</Elevation>';
   case WHEN (clevel = 's' and cid != 0) THEN
         xml := d.xml FROM public.distance d where d.distance_id = cid and d.level = 's' and d.type = 'e';
        WHEN (clevel = 'c' and cid != 0) THEN
        xml := d.xml FROM public.distance d where d.distance_id = cid and d.level = 'c'  and d.type = 'e';
        WHEN (clevel = '#' ) THEN xml := '<Elevation>0</Elevation>';
        ELSE xml := '<Elevation>0</Elevation>';
      END CASE;
   RETURN xml;
   END; $$
LANGUAGE 'plpgsql';


--select "elevation-xml"(100,'s');
--select "elevation-xml"(3000,'c');

CREATE OR REPLACE FUNCTION public."depth-xml" (BIGINT, TEXT) RETURNS TEXT AS $$
   DECLARE
   cid ALIAS FOR $1;
   clevel ALIAS FOR $2;
   xml TEXT;
   BEGIN
   xml := '<Depth>0</Depth>';
   case WHEN (clevel = 'c' and cid != 0) THEN
      xml := d.xml FROM public.distance d where d.distance_id = cid and d.level = 'c'  and d.type = 'd';
   WHEN (cid = 0) THEN
      xml := '<Depth>0</Depth>';
   ELSE xml := '<Depth>0</Depth>';
      END CASE;
   RETURN xml;
   -- RETURN xml;
   END; $$
LANGUAGE 'plpgsql';

--select "depth-xml"(100,'s');
--select "depth-xml"(3000,'c');

CREATE OR REPLACE FUNCTION public."geology-xml" (INTEGER) RETURNS TEXT AS $$
   DECLARE
   cid ALIAS FOR $1;   -- id dans la table geology
   xml TEXT;
   BEGIN
   xml := '';
   case 
      WHEN (cid != 0 )
         THEN xml := '<Geology>' || g.geology ||  '</Geology>' FROM public.geology g where g.geology_id != 0 and g.geology_id = cid;
   ELSE xml := '';
   END CASE;
   RETURN xml;
   END; $$
LANGUAGE 'plpgsql';

-- select "geology-xml"(3000);

CREATE OR REPLACE FUNCTION public."vault-xml" (INTEGER) RETURNS TEXT AS $$
   DECLARE
   cid ALIAS FOR $1;
   xml TEXT;
   BEGIN
   xml := '';
   case WHEN (cid != 0 ) THEN xml := '<Vault>' || v.vault ||  '</Vault>' FROM public.vault v where v.vault_id != 0 and vault_id = cid;
   ELSE xml := '';
      END CASE;
   RETURN xml;
   END; $$
LANGUAGE 'plpgsql';

-- select "vault-xml"(0);
-- 2020-05-12 : La baise Site est obligatoire, on renvoie un Unknown si il est non référencé.
-- 
CREATE OR REPLACE FUNCTION public."site-xml" (BIGINT) RETURNS TEXT AS $$
   DECLARE
   cid ALIAS FOR $1;
   xml TEXT;
   BEGIN
   xml:= '';
   case WHEN (cid != 0 ) THEN xml := site.xml FROM public.site where site.site_id = cid;
   ELSE xml := '';
      END CASE;
   RETURN xml;
   END; $$
LANGUAGE 'plpgsql';

/*
CREATE OR REPLACE FUNCTION public."comment-xml" (BIGINT, TEXT) RETURNS TEXT AS $$
   DECLARE
   cid ALIAS FOR $1;      -- id for a network, a station or a channel
   clevel ALIAS FOR $2; --'n', 's', 'c'
   xml TEXT; -- RETURN value
   ccomment public."comment"%ROWTYPE;
   BEGIN
   xml:='';
   CASE
      WHEN (clevel = 'n')  THEN
           FOR ccomment IN SELECT * FROM public.comment WHERE comment.network_id =  cid and comment.level = clevel
            LOOP
               xml := xml||ccomment.xml;
            END LOOP;
      WHEN (clevel = 's')  THEN
           FOR ccomment IN SELECT * FROM public.comment WHERE comment.station_id =  cid and comment.level = clevel
            LOOP
               xml := xml||ccomment.xml;
            END LOOP;
      ELSE
           FOR ccomment IN SELECT * FROM public.comment WHERE comment.channel_id =  cid and comment.level = clevel
            LOOP
               xml := xml||ccomment.xml;
            END LOOP ;
      END CASE;
      RETURN xml;
END;
$$ LANGUAGE plpgsql;
*/
-- modification CPequegnat 22/10/2020, pour éviter la répétion des commentaires au niveau network.
-- public.network_comment est une vue logique, qui ne contient que les commentaires de réseaux (pas de champ level donc)
CREATE OR REPLACE FUNCTION public."comment-xml" (BIGINT, TEXT) RETURNS TEXT AS $$
   DECLARE
   cid ALIAS FOR $1;      -- id for a network, a station or a channel
   clevel ALIAS FOR $2; --'n', 's', 'c'
   xml TEXT; -- RETURN value
   ccomment public."comment"%ROWTYPE;
   ncomment public."network_comment"%ROWTYPE;
   BEGIN
   xml:='';
   CASE
      WHEN (clevel = 'n')  THEN
           FOR ncomment IN SELECT distinct * FROM public.network_comment n WHERE n.network_id =  cid
            LOOP
               xml := xml||ncomment.xml;
            END LOOP;
      WHEN (clevel = 's')  THEN
           FOR ccomment IN SELECT distinct * FROM public.comment c WHERE c.station_id =  cid and c.level = clevel
            LOOP
               xml := xml||ccomment.xml;
            END LOOP;
      ELSE
           FOR ccomment IN SELECT distinct * FROM public.comment s WHERE s.channel_id =  cid and s.level = clevel
            LOOP
               xml := xml||ccomment.xml;
            END LOOP ;
      END CASE;
      RETURN xml;
END;
$$ LANGUAGE plpgsql;

--select s.station, "comment-xml"(s.station_id,'s')
--FROM public.station s WHERE s.station = 'AGD';

CREATE OR REPLACE FUNCTION public."identifier-xml" (BIGINT, TEXT) RETURNS TEXT AS $$
   DECLARE
   cid ALIAS FOR $1;      -- id for a network, a station or a channel
   clevel ALIAS FOR $2; --'n', 's', 'c'
   xml TEXT; -- RETURN
   cidentifier public."identifier"%ROWTYPE;
   BEGIN
   xml:='';
   CASE
      WHEN (clevel = 'n') THEN
            FOR cidentifier IN SELECT * FROM public.identifier WHERE identifier.network_id =  cid and identifier.level = 'n'
            LOOP
               xml := xml||cidentifier.xml;
            END LOOP;
      WHEN (clevel = 's') THEN
           FOR cidentifier IN SELECT * FROM public.identifier WHERE identifier.station_id = cid and identifier.level = 's'
            LOOP
               xml := xml||cidentifier.xml;
            END LOOP;
      ELSE
           FOR cidentifier IN SELECT * FROM public.identifier WHERE identifier.channel_id =  cid and identifier.level = 'c'
            LOOP
               xml := xml||cidentifier.xml;
            END LOOP;
      END CASE;
      RETURN xml;
END;
$$ LANGUAGE plpgsql;
-------------------------------------------------------------------------
-- select n.network, "identifier-xml"(n.network_id,'n')
-- FROM public.networks n WHERE n.network = 'FR';
-- 2020-09-08
-- 
-- 
CREATE OR REPLACE FUNCTION public."operator-xml" (BIGINT, TEXT) RETURNS TEXT AS $$
   DECLARE
   cid ALIAS FOR $1;      -- id for a network;
   clevel ALIAS FOR $2; --'n', 's', 'c'
   xml TEXT; -- RETURN
   coperator TEXT;
   BEGIN
   xml:='';
   CASE
      WHEN (clevel = 'n') THEN
           FOR coperator IN SELECT distinct o.xml FROM public.operator o WHERE o.network_id =  cid and o.level = 'n'
            LOOP
               xml := xml||coperator;
            END LOOP;
      WHEN (clevel = 's') THEN
           FOR coperator IN SELECT distinct o.xml FROM public.operator o WHERE o.station_id =  cid and o.level = 's'
            LOOP
               xml := xml||coperator;
            END LOOP;
      ELSE xml := '';
      END CASE ;
      RETURN xml;
END;
$$ LANGUAGE plpgsql;

-- select s.station, "operator-xml"(s.station_id,'s')
-- FROM public.station s WHERE s.station = 'OGAG';


CREATE OR REPLACE FUNCTION public."reference-xml" (BIGINT, TEXT) RETURNS TEXT AS $$
   DECLARE
   cid ALIAS FOR $1;      -- id for a network, a station
   clevel ALIAS FOR $2; --'n', 's', 'c'
   xml TEXT; -- RETURN
   creference public.external_reference%ROWTYPE;
   BEGIN
   xml:='';
   CASE
      WHEN (clevel = 'n')  THEN
           FOR creference IN SELECT * FROM public.external_reference WHERE external_reference.network_id =  cid and external_reference.level = 'n'
            LOOP
               xml := xml ||  '<ExternalReference>' || creference.xml || '</ExternalReference>';
            END LOOP;
      WHEN (clevel = 's')  THEN
           FOR creference IN SELECT * FROM public.external_reference WHERE external_reference.station_id =  cid and external_reference.level = 's'
            LOOP
               xml := xml || '<ExternalReference>' || creference.xml || '</ExternalReference>';
            END LOOP;
      ELSE -- (clevel = 'c')
           FOR creference IN SELECT * FROM public.external_reference WHERE external_reference.channel_id =  cid and external_reference.level = 'c'
            LOOP
               xml := xml || '<ExternalReference>' || creference.xml || '</ExternalReference>';
            END LOOP ;
      END CASE;
      RETURN xml;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION public."type_channel-xml" (BIGINT) RETURNS TEXT AS $$
   DECLARE
   cid ALIAS FOR $1;      -- id for channel_id
   xml TEXT; -- RETURN
   stringtype text;
   ctypechannel public.type_channel_at_channel%ROWTYPE;
   BEGIN
   xml:='';
   FOR ctypechannel IN SELECT distinct * FROM public.type_channel_at_channel t WHERE t.channel_id =  cid
            LOOP
            select distinct tt.type_channel FROM public. type_channel tt where tt.type_channel_id =  ctypechannel.type_channel_id into stringtype;
               xml := xml ||'<Type>' || stringtype || '</Type>' ;
            END LOOP;
   RETURN xml;
   END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION public."azimuth-xml" (BIGINT) RETURNS TEXT AS $$
   DECLARE
   cid ALIAS FOR $1;      -- id for azimuth_id
   xml TEXT; -- RETURN
   BEGIN
   CASE WHEN (cid != 0) THEN
      xml := a.xml FROM public.azimuth a where a.azimuth_id = cid;
      ELSE xml := '' ;
      END CASE;
   RETURN xml;
   END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION public."dip-xml" (BIGINT) RETURNS TEXT AS $$
   DECLARE
   cid ALIAS FOR $1;      -- id for dip_id
   xml TEXT; -- RETURN
   BEGIN
   CASE WHEN (cid != 0) THEN
      xml := d.xml FROM public.dip d where d.dip_id = cid;
      ELSE xml := '' ;
   END CASE;
   RETURN xml;
   END;
$$ LANGUAGE plpgsql;

-- DROP FUNCTION IF exists "samplerate-xml" (BIGINT)  CASCADE;
-- DROP FUNCTION IF exists "samplerate-xml" (INTEGER)  CASCADE;

CREATE OR REPLACE FUNCTION "samplerate-xml" (BIGINT) RETURNS TEXT AS $$
   DECLARE
   cid ALIAS FOR $1;      -- id for samplerate_id
   xml TEXT; -- RETURN
   BEGIN
   xml := s.xml FROM public.samplerate s where s.samplerate_id = cid;
   RETURN xml;
   END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION public."clockdrift-xml" (BIGINT) RETURNS TEXT AS $$
   DECLARE
   cid ALIAS FOR $1;      -- id for clockdrift_id
   xml TEXT; -- RETURN
   BEGIN
   xml := c.xml FROM public.clockdrift c where c.clockdrift_id = cid;
   RETURN xml;
   END;
$$ LANGUAGE plpgsql;
--
-- TODO : ajouter les preamplier et les Ocean bottom ...
-- Broadband Ocean Bottom Seismometer
-- SENSOR
--  PREAMPLIFIER
--  DATALOGGER

CREATE OR REPLACE FUNCTION public."equipment-xml" (BIGINT, TEXT) RETURNS TEXT AS $$ 
   DECLARE
   cid ALIAS FOR $1;      -- id for a station or a channel
   clevel ALIAS FOR $2; -- 's', 'c'
   xml TEXT; -- RETURN
   -- equip TEXT;
   cequipment public."equipment"%ROWTYPE;
   BEGIN
   xml:='';
   CASE
      WHEN (clevel = 's' and cid != 0) THEN
           FOR cequipment IN SELECT * FROM public.equipment e WHERE e.station_id =  cid and e.level = 's'
            LOOP
               xml := xml || cequipment.xml ;
            END LOOP;
      WHEN (clevel = 'c' and cid != 0) THEN
           FOR cequipment IN SELECT * FROM public.equipment e WHERE e.channel_id =  cid and e.level = 'c' and e.type = 'SENSOR'
            LOOP
               xml := xml || cequipment.xml ;
            END LOOP;
           FOR cequipment IN SELECT * FROM public.equipment e WHERE e.channel_id =  cid and e.level = 'c' and e.type = 'DATALOGGER'
            LOOP
               xml := xml || cequipment.xml ;
            END LOOP;
      ELSE xml := '';
      END CASE;
      RETURN xml;
END; 
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION public."sensitivity-xml" (BIGINT) RETURNS TEXT AS $$
   DECLARE
   cid ALIAS FOR $1;      -- id a channel
   xml TEXT; -- RETURN
   BEGIN
   select distinct '<Response>' || s.xml || '</Response>'
      FROM public.sensitivity s where s.channel_id = cid into xml;
      RETURN xml;
END; 
$$ LANGUAGE plpgsql;
----------------------------
CREATE OR REPLACE FUNCTION public."waterlevel-xml" (BIGINT, TEXT) RETURNS TEXT AS $$
   DECLARE
   cid ALIAS FOR $1;   --
   clevel ALIAS FOR $2;
   xml TEXT;
   BEGIN
   xml := '';
   CASE
      WHEN (clevel = 's' and cid != 0)  THEN xml := w.xml FROM public.waterlevel w where w.level = 's' and w.waterlevel_id = cid and w.waterlevel is not null ;
      WHEN (clevel = 'c' and cid != 0)  THEN xml := w.xml FROM public.waterlevel w where w.level = 'c' and w.waterlevel_id = cid and w.waterlevel is not null ;
      ELSE xml := '';
   END CASE;
   RETURN xml;
   END; $$
LANGUAGE 'plpgsql';

-----------------------------------------------------------

CREATE OR REPLACE FUNCTION public."unit-calib-xml" (INTEGER) RETURNS TEXT AS $$
   DECLARE
   unid ALIAS FOR $1;   --
   xml TEXT;
   BEGIN
   xml := '';
   CASE
      WHEN (unid != 0) THEN
         xml := '<CalibrationUnits><Name>' || u.unit || '</Name><Description>' || u.description || '</Description></CalibrationUnits>'
         FROM public.unit u where u.unit_id = unid ;
        ELSE xml := '';
   END CASE;
   RETURN xml;
   END; $$
LANGUAGE 'plpgsql';
--
--============================================================================================================================================
/*
-- #Network|Description|StartTime|EndTime|TotalStations
--
CREATE OR REPLACE FUNCTION public."network-txt" (BIGINT) RETURNS TEXT AS $$
   DECLARE
   netid ALIAS FOR $1;      -- code reseau FDSN
   txt TEXT;                -- RETURN
   BEGIN
   txt :=   n.network ||'|'|| n.description ||'|'||
      TO_CHAR(n.starttime,'YYYY-MM-DD') || 'T'|| TO_CHAR(n.starttime,'HH24:MI:SS') ||'|'||
      -- TODO : à remplacer lorsque l'ancien portail sera hors de service
      -- CASE WHEN n.endtime = 'infinity' THEN ''
         -- ELSE TO_CHAR(n.endtime,'YYYY-MM-DD') || 'T'|| TO_CHAR(n.endtime,'HH24:MI:SS') END
      CASE WHEN n."endtime" = 'infinity' THEN '2500-12-31T23:59:59'
         ELSE TO_CHAR(n."endtime",'YYYY-MM-DD') || 'T'|| TO_CHAR(n."endtime",'HH24:MI:SS') END
      ||'|'|| n.nb_station
   FROM public.networks n
   WHERE n.network_id = netid;
   RETURN txt;
   END;
$$ LANGUAGE 'plpgsql';
*/
--
-- Fonction pour le formatage textuel d'un réseau, avec le nombre de ses stations qui sont dans ws_common
--
-- #Network|Description|StartTime|EndTime|TotalStations
-- report de 20201022_migration_fix_output_nb_elements.sql
--
CREATE OR REPLACE FUNCTION public."network-txt" (BIGINT) RETURNS TEXT AS $$
   DECLARE
   netid ALIAS FOR $1;      -- code reseau FDSN
   txt TEXT;                -- RETURN
   BEGIN
   txt :=   n.network ||'|'|| n.description ||'|'||
      TO_CHAR(n.starttime,'YYYY-MM-DD') || 'T'|| TO_CHAR(n.starttime,'HH24:MI:SS') ||'|'||
      -- TODO : à remplacer lorsque l'ancien portail sera hors de service
      -- CASE WHEN n.endtime = 'infinity' THEN ''
         -- ELSE TO_CHAR(n.endtime,'YYYY-MM-DD') || 'T'|| TO_CHAR(n.endtime,'HH24:MI:SS') END
      CASE WHEN n."endtime" = 'infinity' THEN '2500-12-31T23:59:59'
         ELSE TO_CHAR(n."endtime",'YYYY-MM-DD') || 'T'|| TO_CHAR(n."endtime",'HH24:MI:SS') END
      ||'|'|| public.sum_station_ws_common(netid)
   FROM public.networks n
   WHERE n.network_id = netid;
   RETURN txt;
   END;
$$ LANGUAGE 'plpgsql';

--
-- Fonction pour le formatage textuel d'un réseau, avec le nombre de ses stations qui sont dans ws_common
-- #Network|Station|Latitude|Longitude|Elevation|SiteName|StartTime|EndTime

CREATE OR REPLACE FUNCTION public."station-txt" (BIGINT) RETURNS TEXT AS $$
   DECLARE
   staid ALIAS FOR $1;
   txt TEXT;
   BEGIN
   txt :=   n.network || '|' || s.station || '|'  || la.latitude || '|' || lo.longitude ||'|'|| d.distance||'|'|| si.site || '|'||
      TO_CHAR(s.starttime,'YYYY-MM-DD') || 'T'|| TO_CHAR(s.starttime,'HH24:MI:SS') ||'|'||
      CASE WHEN s."endtime" = 'infinity' THEN '2500-12-31T23:59:59'
      ELSE TO_CHAR(s."endtime",'YYYY-MM-DD') || 'T'|| TO_CHAR(s."endtime",'HH24:MI:SS') END
   FROM public.networks n
     INNER JOIN public.station s ON n.network_id = s.network_id
     INNER JOIN public.distance d ON s.elevation_id = d.distance_id
     INNER JOIN public.latitude la ON la.latitude_id = s.latitude_id
     INNER JOIN public.longitude lo ON lo.longitude_id = s.longitude_id
     INNER JOIN public.site si ON si.site_id = s.site_id
   WHERE
      s.station_id = staid
      ;
   RETURN txt;
   END; $$
LANGUAGE 'plpgsql';

--
--Network|Station|Location|Channel|Latitude|Longitude|Elevation|Depth|Azimuth|Dip|SensorDescription|Scale|ScaleFrequency|ScaleUnits|SampleRate|StartTime|EndTime
--

CREATE OR REPLACE FUNCTION public."channel-txt" (BIGINT) RETURNS TEXT AS $$
DECLARE 
   chaid ALIAS FOR $1;
   txt TEXT;
   BEGIN
   SELECT
         distinct n.network || '|' || s.station || '|' || public.format_location(c.location) || '|' ||
         c.channel || '|' || la.latitude || '|' || lo.longitude ||'|' ||
         di1.distance||'|'|| di2.distance || '|' ||
         CASE WHEN az.azimuth is null then '' ELSE az.azimuth::text END || '|' ||
         CASE WHEN dp.dip is null THEN '' ELSE dp.dip::text END || '|' ||
         -- CASE WHEN eq.model is null THEN '' ELSE eq.model END || '|'||
         CASE WHEN eq.description is null THEN '' ELSE eq.description END || '|'||
         CASE WHEN (se.sensitivity IS NOT NULL) THEN se.sensitivity::text ELSE ''  END || '|' ||
         CASE WHEN (se.frequency IS NOT NULL)  THEN se.frequency::text ELSE ''   END || '|' ||
         CASE WHEN (se.unit IS NOT NULL ) THEN se.unit::text ELSE '' END || '|'||
         sa.samplerate || '|'||
         TO_CHAR(c.starttime,'YYYY-MM-DD') || 'T'|| TO_CHAR(c.starttime,'HH24:MI:SS') || '|'||
         -- TODO : à remplacer lorsque l'ancien portail sera hors de service
         -- CASE WHEN c.endtime = 'infinity' THEN '' ELSE TO_CHAR(c.endtime,'YYYY-MM-DD') || 'T'|| TO_CHAR(c.endtime,'HH24:MI:SS') END
         CASE WHEN c."endtime" = 'infinity' THEN '2500-12-31T23:59:59'
               ELSE TO_CHAR(c."endtime",'YYYY-MM-DD') || 'T'|| TO_CHAR(c."endtime",'HH24:MI:SS') END
   FROM
      public.channel c,
      INNER JOIN public.station s ON s.station_id = c.station_id
      INNER JOIN public.networks n ON s.network_id = n.network_id
      INNER JOIN public.latitude la ON la.latitude_id = c.latitude_id
      INNER JOIN public.longitude lo ON lo.longitude_id = c.longitude_id
      INNER JOIN public.distance di1 ON di1.distance_id = c.elevation_id
      INNER JOIN public.distance di2 ON di2.distance_id = c.depth_id
      INNER JOIN public.sensitivity se ON se.channel_id = c.channel_id
      INNER JOIN public.samplerate sa ON c.samplerate_id = sa.samplerate_id
      INNER JOIN public.azimuth az ON c.azimuth_id = az.azimuth_id
      INNER JOIN public.dip dp ON c.dip_id = dp.dip_id
      INNER JOIN public.equipment eq ON eq.channel_id = c.channel_id
   WHERE
         c.channel_id = chaid and
         eq.type = 'SENSOR'  and
   INTO txt;
   RETURN txt;
   END;
$$ LANGUAGE 'plpgsql';
-----------------------------------------------------------------------------------
-- XML pour network_id
-- report de 20201022_migration_fix_output_nb_elements.sql
-- CP 1/11/2020
/*

CREATE OR REPLACE FUNCTION public."network-xml" (BIGINT) RETURNS TEXT AS $$
   DECLARE
   netid ALIAS FOR $1;      -- code reseau FDSN
   xml TEXT; -- RETURN
   BEGIN
   xml :='<Network code="' || n.network || '"' ||
      CASE WHEN (n.altcode is not null ) THEN ' alternateCode="' || n.altcode ||  '"' ELSE '' END ||
      CASE WHEN (n.histcode is not null ) THEN ' historicalCode="' || n.histcode ||  '"' ELSE '' END ||
      ' startDate="' || TO_CHAR(n.starttime,'YYYY-MM-DD') || 'T'|| TO_CHAR(n.starttime,'HH24:MI:SS') || '"' ||
      -- CASE WHEN (n.endtime = 'infinity'::TIMESTAMP WITHOUT TIME ZONE)  THEN ''
      -- ELSE
      ' endDate="' ||
        CASE WHEN (n."endtime" = 'infinity'::TIMESTAMP WITHOUT TIME ZONE)  THEN '2500-12-31T23:59:59"'
            ELSE TO_CHAR(n."endtime",'YYYY-MM-DD') || 'T'|| TO_CHAR(n."endtime",'HH24:MI:SS') || '"'  END ||
      ' restrictedStatus="' || n.policy || '">'  ||
      CASE WHEN (n.description is not null ) THEN '<Description>' || n.description || '</Description>' ELSE '' END ||
      public."identifier-xml" (n.network_id, 'n') ||
      public."comment-xml" (n.network_id, 'n') ||
      -- public."operator-xml" (n.network_id, 'n') ||
      public."operator-xml" (n.network_id, 'n', n.network,n.description) ||
   '<TotalNumberStations>' || n.nb_station || '</TotalNumberStations>'
-- la fin de la structure network ne pas être construite en statique, la mise a jour se fait dynamiquement
   FROM networks n WHERE n.network_id = netid;
   RETURN xml;
   END;
$$ LANGUAGE 'plpgsql';
*/
--
-- Fonctions pour le formatage xml d'un réseau, avec le nombre de ses stations qui sont sont dans ws_common
--
--
CREATE OR REPLACE FUNCTION public."network-xml" (BIGINT) RETURNS TEXT AS $$
   DECLARE
   netid ALIAS FOR $1;      -- code reseau FDSN
   xml TEXT; -- RETURN
   BEGIN
   xml :='<Network code="' || n.network || '"' ||
      CASE WHEN (n.altcode is not null ) THEN ' alternateCode="' || n.altcode ||  '"' ELSE '' END ||
      CASE WHEN (n.histcode is not null ) THEN ' historicalCode="' || n.histcode ||  '"' ELSE '' END ||
      ' startDate="' || TO_CHAR(n.starttime,'YYYY-MM-DD') || 'T'|| TO_CHAR(n.starttime,'HH24:MI:SS') || '"' ||
      -- CASE WHEN (n.endtime = 'infinity'::TIMESTAMP WITHOUT TIME ZONE)  THEN ''
      -- ELSE
      ' endDate="' ||
        CASE WHEN (n."endtime" = 'infinity'::TIMESTAMP WITHOUT TIME ZONE)  THEN '2500-12-31T23:59:59"'
            ELSE TO_CHAR(n."endtime",'YYYY-MM-DD') || 'T'|| TO_CHAR(n."endtime",'HH24:MI:SS') || '"'  END ||
      ' restrictedStatus="' || n.policy || '">'  ||
      CASE WHEN (n.description is not null ) THEN '<Description>' || n.description || '</Description>' ELSE '' END ||
      public."identifier-xml" (n.network_id, 'n') ||
      public."comment-xml" (n.network_id, 'n') ||
      public."operator-xml" (n.network_id, 'n') ||
   '<TotalNumberStations>' || public.sum_station_ws_common (netid) || '</TotalNumberStations>'
-- la fin de la structure network ne peut être construite en statique, la mise a jour se fait dynamiquement
   FROM public.networks n WHERE n.network_id = netid;
   RETURN xml;
   END;
$$ LANGUAGE 'plpgsql';


--
-- Fonctions pour le formatage xml d'une station, avec le nombre de ses stations qui sont sont dans ws_common
-- report de 20201022_migration_fix_output_nb_elements.sql
-- 01/11/2020, CP

-- part 1 level station, extenddedattribute = true

/*CREATE OR REPLACE FUNCTION public."station-xml1" (BIGINT) RETURNS TEXT AS $$
   DECLARE
   staid ALIAS FOR $1;
   xml TEXT;
   BEGIN
   xml := '<Station code="' || s.station || '"' ||
   CASE WHEN (s.altcode is not null ) THEN ' alternateCode="' || s.altcode ||  '"' ELSE '' END ||
   CASE WHEN (s.histcode is not null ) THEN ' historicalCode="' || s.histcode ||  '"' ELSE '' END ||
   ' startDate="' || TO_CHAR(s.starttime,'YYYY-MM-DD') || 'T'|| TO_CHAR(s.starttime,'HH24:MI:SS') || '"' ||
   -- CASE WHEN (s.endtime = 'infinity'::TIMESTAMP WITHOUT TIME ZONE)  THEN ''
       --    ELSE
    -- ' endDate="' || TO_CHAR(s.endtime,'YYYY-MM-DD') || 'T'|| TO_CHAR(s.endtime,'HH24:MI:SS.US') || '"'  END ||
    ' endDate="' ||
   CASE WHEN s."endtime" = 'infinity' THEN '2500-12-31T23:59:59"'
      ELSE TO_CHAR(s."endtime",'YYYY-MM-DD') || 'T'|| TO_CHAR(s."endtime",'HH24:MI:SS') || '"' END ||
    ' restrictedStatus="' || n.policy ||  '"' ||
    ' resif:alternateNetworkCodes="' || public.fullnet(n.network_id) || '">' ||
   CASE WHEN (s.description is not null ) THEN '<Description>' || s."description" || '</Description>' ELSE '' END ||
   public."comment-xml" (s.station_id, 's')  ||
   public."latitude-xml" (s.latitude_id, 's')  ||
   public."longitude-xml" (s.longitude_id, 's')  ||
   public."distance-xml" (s.elevation_id, 's', 'e') ||
   -- public."waterlevel-xml" (s.waterlevel_id, 's')   ||
   CASE WHEN s.site_id = 0 THEN '<Site><Name>' || s.station || '</Name></Site>' ELSE public."site-xml" (s.site_id) END ||
   -- public."vault-xml" (s.vault_id) ||
   public."geology-xml" (s.geology_id)  ||
   -- Pas de réécriture de l operateur
   -- public."operator-xml" (s.station_id, 's') ||
   -- réécriture de l'operateur
   public."operator-xml" (s.station_id, 's', n.network, n.description) ||
   -- public."equipment-xml" (s.station_id, 's')  ||
   CASE WHEN (s.creationdate is not null)
      THEN '<CreationDate>' || TO_CHAR(s."creationdate",'YYYY-MM-DD') || 'T'|| TO_CHAR(s."creationdate",'HH24:MI:SS') || '</CreationDate>' ELSE
      '<CreationDate>' || TO_CHAR(s."starttime",'YYYY-MM-DD') || 'T'|| TO_CHAR(s."starttime",'HH24:MI:SS') || '</CreationDate>' END ||
   --- CASE WHEN (s.terminationdate is not null)
 -- THEN '<TerminationDate>' || TO_CHAR(s."terminationdate",'YYYY-MM-DD') || 'T'|| TO_CHAR(s."terminationdate",'HH24:MI:SS') || 'Z</TerminationDate>' ELSE '' END ||
   --
   '<TotalNumberChannels>' || s.nb_channel || '</TotalNumberChannels>'
   FROM networks n,
   station s
   WHERE s.station_id = staid
   and n.network_id = s.network_id
   ;
   RETURN xml;
   END;
$$ LANGUAGE 'plpgsql';
*/
--
-- -- part 1 level station, extenddedattribute = false
--
/* CREATE OR REPLACE FUNCTION public."station-xml11" (BIGINT) RETURNS TEXT AS $$
   DECLARE
   staid ALIAS FOR $1;
   xml TEXT;
   BEGIN
   xml := '<Station code="' || s.station || '"' ||
   CASE WHEN (s.altcode is not null ) THEN ' alternateCode="' || s.altcode ||  '"' ELSE '' END ||
   CASE WHEN (s.histcode is not null ) THEN ' historicalCode="' || s.histcode ||  '"' ELSE '' END ||
   ' startDate="' || TO_CHAR(s.starttime,'YYYY-MM-DD') || 'T'|| TO_CHAR(s.starttime,'HH24:MI:SS') || '"' ||
   -- CASE WHEN (s.endtime = 'infinity'::TIMESTAMP WITHOUT TIME ZONE)  THEN ''
          -- ELSE
          -- ' endDate="' || TO_CHAR(s.endtime,'YYYY-MM-DD') || 'T'|| TO_CHAR(s.endtime,'HH24:MI:SS.US') || '"'  END ||
    ' endDate="' ||
   CASE WHEN s."endtime" = 'infinity' THEN '2500-12-31T23:59:59"'
      ELSE TO_CHAR(s."endtime",'YYYY-MM-DD') || 'T'|| TO_CHAR(s."endtime",'HH24:MI:SS') || '"' END ||
   ' restrictedStatus="' || n.policy ||  '">' ||
   CASE WHEN (s.description is not null ) THEN '<Description>' || s."description" || '</Description>' ELSE '' END ||
   public."comment-xml" (s.station_id, 's')  ||
   public."latitude-xml" (s.latitude_id, 's')  ||
   public."longitude-xml" (s.longitude_id, 's')  ||
   public."distance-xml" (s.elevation_id, 's', 'e') ||
   -- public."waterlevel-xml" (s.waterlevel_id, 's')   ||
   CASE WHEN s.site_id = 0 THEN '<Site><Name>' || s.station || '</Name></Site>' ELSE public."site-xml" (s.site_id) END ||
   -- public."vault-xml" (s.vault_id) ||
   public."geology-xml" (s.geology_id)  ||
   -- Pas de réécriture de l operateur
   -- public."operator-xml" (s.station_id, 's') ||
   -- réécriture de l'operateur
   public."operator-xml" (s.station_id, 's',n.network,n.description) ||
   --public."equipment-xml" (s.station_id, 's')  ||
   CASE WHEN (s.creationdate is not null)
      THEN '<CreationDate>' || TO_CHAR(s."creationdate",'YYYY-MM-DD') || 'T'|| TO_CHAR(s."creationdate",'HH24:MI:SS') || '</CreationDate>' ELSE
      '<CreationDate>' || TO_CHAR(s."starttime",'YYYY-MM-DD') || 'T'|| TO_CHAR(s."starttime",'HH24:MI:SS') || '</CreationDate>' END ||
      --
   -- CASE WHEN (s.terminationdate is not null and s.endtime !=  'infinity'::TIMESTAMP WITHOUT TIME ZONE)
         -- THEN '<TerminationDate>' || TO_CHAR(s."terminationdate",'YYYY-MM-DD') || 'T'|| TO_CHAR(s."terminationdate",'HH24:MI:SS') || 'Z</TerminationDate>' ELSE '' END ||

   '<TotalNumberChannels>' || s.nb_channel || '</TotalNumberChannels>' 
FROM networks n, station s
   WHERE s.station_id = staid AND n.network_id = s.network_id ;
   RETURN xml;
   END;
$$ LANGUAGE 'plpgsql';
*/

-- part 1 level station, extenddedattribute = true

CREATE OR REPLACE FUNCTION public."station-xml1" (BIGINT) RETURNS TEXT AS $$
   DECLARE
   staid ALIAS FOR $1;
   xml TEXT;
   BEGIN
   xml := '<Station code="' || s.station || '"' ||
   CASE WHEN (s.altcode is not null ) THEN ' alternateCode="' || s.altcode ||  '"' ELSE '' END ||
   CASE WHEN (s.histcode is not null ) THEN ' historicalCode="' || s.histcode ||  '"' ELSE '' END ||
   ' startDate="' || TO_CHAR(s.starttime,'YYYY-MM-DD') || 'T'|| TO_CHAR(s.starttime,'HH24:MI:SS') || '"' ||
   -- CASE WHEN (s.endtime = 'infinity'::TIMESTAMP WITHOUT TIME ZONE)  THEN ''
       --    ELSE
    -- ' endDate="' || TO_CHAR(s.endtime,'YYYY-MM-DD') || 'T'|| TO_CHAR(s.endtime,'HH24:MI:SS.US') || '"'  END ||
    ' endDate="' ||
   CASE WHEN s."endtime" = 'infinity' THEN '2500-12-31T23:59:59"'
      ELSE TO_CHAR(s."endtime",'YYYY-MM-DD') || 'T'|| TO_CHAR(s."endtime",'HH24:MI:SS') || '"' END ||
    ' restrictedStatus="' || n.policy ||  '"' ||
    ' resif:alternateNetworkCodes="' || public.fullnet(n.network_id) || '">' ||
   CASE WHEN (s.description is not null ) THEN '<Description>' || s."description" || '</Description>' ELSE '' END ||
   public."comment-xml" (s.station_id, 's')  ||
   public."latitude-xml" (s.latitude_id, 's')  ||
   public."longitude-xml" (s.longitude_id, 's')  ||
   public."distance-xml" (s.elevation_id, 's', 'e') ||
   -- public."waterlevel-xml" (s.waterlevel_id, 's')   ||
   CASE WHEN s.site_id = 0 THEN '<Site><Name>' || s.station || '</Name></Site>' ELSE public."site-xml" (s.site_id) END ||
   -- public."vault-xml" (s.vault_id) ||
   public."geology-xml" (s.geology_id)  ||
   public."operator-xml" (s.station_id, 's') ||
   -- public."equipment-xml" (s.station_id, 's')  ||
   CASE WHEN (s.creationdate is not null)
      THEN '<CreationDate>' || TO_CHAR(s."creationdate",'YYYY-MM-DD') || 'T'|| TO_CHAR(s."creationdate",'HH24:MI:SS') || '</CreationDate>' ELSE
      '<CreationDate>' || TO_CHAR(s."starttime",'YYYY-MM-DD') || 'T'|| TO_CHAR(s."starttime",'HH24:MI:SS') || '</CreationDate>' END ||
   -- CASE WHEN (s.terminationdate is not null)
   -- THEN '<TerminationDate>' || TO_CHAR(s."terminationdate",'YYYY-MM-DD') || 'T'|| TO_CHAR(s."terminationdate",'HH24:MI:SS') || 'Z</TerminationDate>' ELSE '' END ||
   --
   '<TotalNumberChannels>' || public.sum_channel_ws_common(staid) || '</TotalNumberChannels>'
   FROM public.networks n, public.station s
   WHERE s.station_id = staid
   and n.network_id = s.network_id
   ;
   RETURN xml;
   END;
$$ LANGUAGE 'plpgsql';
--
-- -- part 1 level station, extenddedattribute = false
--
CREATE OR REPLACE FUNCTION public."station-xml11" (BIGINT) RETURNS TEXT AS $$
   DECLARE
   staid ALIAS FOR $1;
   xml TEXT;
   BEGIN
   xml := '<Station code="' || s.station || '"' ||
   CASE WHEN (s.altcode is not null ) THEN ' alternateCode="' || s.altcode ||  '"' ELSE '' END ||
   CASE WHEN (s.histcode is not null ) THEN ' historicalCode="' || s.histcode ||  '"' ELSE '' END ||
   ' startDate="' || TO_CHAR(s.starttime,'YYYY-MM-DD') || 'T'|| TO_CHAR(s.starttime,'HH24:MI:SS') || '"' ||
   -- CASE WHEN (s.endtime = 'infinity'::TIMESTAMP WITHOUT TIME ZONE)  THEN ''
          -- ELSE
          -- ' endDate="' || TO_CHAR(s.endtime,'YYYY-MM-DD') || 'T'|| TO_CHAR(s.endtime,'HH24:MI:SS.US') || '"'  END ||
    ' endDate="' ||
   CASE WHEN s."endtime" = 'infinity' THEN '2500-12-31T23:59:59"'
      ELSE TO_CHAR(s."endtime",'YYYY-MM-DD') || 'T'|| TO_CHAR(s."endtime",'HH24:MI:SS') || '"' END ||
   ' restrictedStatus="' || n.policy ||  '">' ||
   CASE WHEN (s.description is not null ) THEN '<Description>' || s."description" || '</Description>' ELSE '' END ||
   public."comment-xml" (s.station_id, 's')  ||
   public."latitude-xml" (s.latitude_id, 's')  ||
   public."longitude-xml" (s.longitude_id, 's')  ||
   public."distance-xml" (s.elevation_id, 's', 'e') ||
   -- public."waterlevel-xml" (s.waterlevel_id, 's')   ||
   CASE WHEN s.site_id = 0 THEN '<Site><Name>' || s.station || '</Name></Site>' ELSE public."site-xml" (s.site_id) END ||
   -- public."vault-xml" (s.vault_id) ||
   public."geology-xml" (s.geology_id)  ||
   public."operator-xml" (s.station_id, 's') ||
   --public."equipment-xml" (s.station_id, 's')  ||
   CASE WHEN (s.creationdate is not null)
      THEN '<CreationDate>' || TO_CHAR(s."creationdate",'YYYY-MM-DD') || 'T'|| TO_CHAR(s."creationdate",'HH24:MI:SS') || '</CreationDate>' ELSE
      '<CreationDate>' || TO_CHAR(s."starttime",'YYYY-MM-DD') || 'T'|| TO_CHAR(s."starttime",'HH24:MI:SS') || '</CreationDate>' END ||
     --
   -- CASE WHEN (s.terminationdate is not null and s.endtime !=  'infinity'::TIMESTAMP WITHOUT TIME ZONE)
      --    THEN '<TerminationDate>' || TO_CHAR(s."terminationdate",'YYYY-MM-DD') || 'T'|| TO_CHAR(s."terminationdate",'HH24:MI:SS') || 'Z</TerminationDate>' ELSE '' END ||
   --
   '<TotalNumberChannels>' || public.sum_channel_ws_common(staid)  || '</TotalNumberChannels>'
FROM public.networks n, public.station s
   WHERE s.station_id = staid AND n.network_id = s.network_id ;
   RETURN xml;
   END;
$$ LANGUAGE 'plpgsql';
--- 
-- station level station, part 2 (external-reference)
CREATE OR REPLACE FUNCTION public."station-xml22" (BIGINT) RETURNS TEXT AS $$
DECLARE 
staid ALIAS FOR $1;
xml TEXT; 
BEGIN
xml :=  public."reference-xml" (staid, 's') ;
RETURN xml;
END; 
$$ LANGUAGE 'plpgsql';
-----------------------------------------------------------------
--
-- XML part 1 pour channel _id
--
CREATE OR REPLACE FUNCTION public."channel-xml1" (BIGINT) RETURNS TEXT AS $$
   DECLARE
   chaid ALIAS FOR $1;
   xml TEXT;
   BEGIN
   SELECT DISTINCT
   '<Channel' ||  ' locationCode="' || public.format_location("c"."location") || '" code="' || "c"."channel" || '" ' ||
   CASE WHEN (c.altcode is not null ) THEN ' alternateCode="' || c.altcode ||  '"' ELSE '' END ||
   CASE WHEN (c.histcode is not null ) THEN ' historicalCode="' || c.histcode ||  '"' ELSE '' END ||
   ' startDate="' || TO_CHAR(c.starttime,'YYYY-MM-DD') || 'T'|| TO_CHAR(c.starttime,'HH24:MI:SS') || '"' ||
      -- TODO : à remplacer lorsque l'ancien portail sera hors de service
   -- CASE WHEN (c.endtime = 'infinity'::TIMESTAMP WITHOUT TIME ZONE)  THEN '' ELSE ' endDate="' || TO_CHAR(c.endtime,'YYYY-MM-DD') || 'T'|| TO_CHAR(c.endtime,'HH24:MI:SS.US') || '"'  END ||
   ' endDate="' ||
   CASE WHEN c."endtime" = 'infinity' THEN '2500-12-31T23:59:59"'
      ELSE TO_CHAR(c."endtime",'YYYY-MM-DD') || 'T'|| TO_CHAR(c."endtime",'HH24:MI:SS') || '"' END ||
   ' restrictedStatus="' || n.policy ||  '">' ||
   CASE WHEN (c.description is not null ) THEN '<Description>' || c."description" || '</Description>' ELSE '' END ||
   public."comment-xml" (c.channel_id, 'c')  ||
--   public."identifier-xml" (c.channel_id, 'c')  ||
   public."latitude-xml" (c.latitude_id, 'c')  ||
   public."longitude-xml" (c.longitude_id, 'c')  ||
   public."elevation-xml" (c.elevation_id, 'c')  ||
   public."depth-xml" (c.depth_id, 'c')   ||
--   public."waterlevel-xml" (s.waterlevel_id, 's') ||
   public."azimuth-xml" (c.azimuth_id) ||
   public."dip-xml" (c.dip_id)  ||
   public."type_channel-xml" (c.channel_id)  ||
   public."samplerate-xml" (c.samplerate_id) ||
   public. "clockdrift-xml" (c.clockdrift_id) ||
--   public."unit-calib-xml" (c.unit_id) ||
   public."equipment-xml" (c.channel_id,'c')
   FROM public.channel c, public.networks n, public.station s
   WHERE
         c.channel_id != 0 and s.station_id != 0 and n.network_id != 0 and
         c.channel_id = chaid and
         s.network_id = n.network_id and
         c.station_id = s.station_id
   INTO xml;
   RETURN xml;
   END; $$
LANGUAGE 'plpgsql';

--
-- Obsolete
--

CREATE OR REPLACE FUNCTION public."matchtimeseries_channel" (BIGINT) RETURNS BOOLEAN AS $$
DECLARE 
   chaid ALIAS FOR $1;
   has BOOLEAN;
   nb INTEGER;
   BEGIN
   has := FALSE;
   nb := 0;
   -- channel_with_record
   select count(*) FROM public.channel_with_record c where c.channel_id = chaid limit 1 INTO nb;
   IF ( nb > 0 ) THEN has := TRUE;  END IF;
   return has;
END;$$
LANGUAGE 'plpgsql';


CREATE OR REPLACE FUNCTION public."mtc_channel" (BIGINT) RETURNS BOOLEAN AS $$
DECLARE 
   chaid ALIAS FOR $1;
   has BOOLEAN;
   nba INTEGER;
   nbb INTEGER;
   BEGIN
   has := FALSE;
   nba := 0;
   nbb := 0;
   select channel_id FROM public.rall r where r.channel_id >0 and r.channel_id = chaid limit 1 INTO nba;
   select channel_id FROM public.rbud r where r.channel_id >0 and r.channel_id = chaid limit 1 INTO nbb;
   IF ( nba > 0 or nbb > 0 ) THEN has := TRUE;  END IF;
   return has;
END;$$
LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION public."mtc_station" (BIGINT) RETURNS BOOLEAN AS $$
DECLARE 
   staid ALIAS FOR $1;
   has BOOLEAN;
   nba INTEGER;
   nbb INTEGER;
   BEGIN
   has := FALSE;
   nba := 0;
   nbb := 0;
   select station_id FROM public.rall r where r.station_id >0 and r.station_id = staid limit 1 INTO nba ;
   IF ( nba = 0 ) THEN select station_id FROM public.rbud r where r.station_id >0 and r.station_id = staid limit 1 INTO nbb; END IF;
   IF ( nba > 0 or nbb > 0 ) THEN has := TRUE;  END IF;
   return has;
END;$$
LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION public."mtc_network" (BIGINT) RETURNS BOOLEAN AS $$
DECLARE 
   netid ALIAS FOR $1;
   has BOOLEAN;
   nba INTEGER;
   nbb INTEGER;
   BEGIN
   has := FALSE;
   nba := 0;
   nbb := 0;
   select network_id FROM public.rall r where r.network_id = netid limit 1 INTO nba;
   IF ( nba = 0 ) THEN select network_id FROM public.rbud r where r.network_id > 0 and r.network_id = netid limit 1 INTO nbb; END IF;
   IF ( nba > 0 or nbb > 0 ) THEN has := TRUE;  END IF;
   return has;
END;$$
LANGUAGE 'plpgsql';
--
-- au moins une occurence de station dans rall ou rbud ?
-- 

CREATE OR REPLACE FUNCTION public."matchtimeseries_station" (BIGINT) RETURNS BOOLEAN AS $$
DECLARE 
   staid ALIAS FOR $1;
   has BOOLEAN;
   nba INTEGER;
   nbr INTEGER;
   BEGIN
   has := FALSE;
   nba := 0;
   nbr := 0;
   select r.rall_id FROM public.rall r, public.station s where r.station =  s.station and s.station_id = staid limit 1 into nba;
   select r.rbud_id FROM public.rbud r, public.station s where r.station =  s.station and s.station_id = staid limit 1 into nbr;
   IF ( nba > 0 or nbr > 0 ) THEN has := TRUE;  END IF;
   return has;
END;$$
LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION public."matchtimeseries_network" (BIGINT) RETURNS BOOLEAN AS $$
DECLARE 
   neid ALIAS FOR $1;
   has BOOLEAN;
   nba INTEGER;
   nbr INTEGER;
   BEGIN
   has := FALSE;
   nba := 0;
   nbr := 0;
   select r.network FROM public.rall r, public.networks n where r.network =  n.network and n.network_id = neid limit 1 into nba;
   select r.network FROM public.rbud r, public.networkd n where r.network =  n.network and n.network_id = neid limit 1 into nbr;
   IF ( nba > 0 or nbr > 0 ) THEN has := TRUE;  END IF;
   return has;
END;$$
LANGUAGE 'plpgsql';

-- CREATE OR REPLACE FUNCTION set_channel_id_in_file_table (TEXT, TEXT, TEXT, TEXT, TIMESTAMP WITHOUT TIME ZONE, TIMESTAMP WITHOUT TIME ZONE) RETURNS BIGINT AS $$
-- un fichier peut être associés à 0, .. channels.

/*
CREATE OR REPLACE FUNCTION public.set_channel_id_in_rbud (BIGINT) RETURNS BIGINT AS $$
-- Dans ce qui suit, on considère qu'un fichier n'est pas 'orphelins' si il est associé à au moins un channel
   DECLARE
   rid ALIAS FOR $1;   -- id du fichier dans la table
   id BIGINT ;            -- id du channel auquel il appratient
   idt BIGINT;
   BEGIN
   idt := ((
         select DISTINCT c.channel_id
         FROM public.channel c, public.station s, public.networks n, public.rbud r
         WHERE
         (r.rbud_id = rid and r.network = n.network and r.station = s.station and r.channel = c.channel and r.location = c.location and
         r.starttime BETWEEN c.starttime and c.endtime and
         n.network_id = s.station_id and c.station_id = s.station_id )
         OR
         (r.rbud_id = rid and r.network = n.network and r.station = s.station and r.channel = c.channel and r.location = c.location and
         r.endtime BETWEEN c.starttime and c.endtime and
         n.network_id = s.station_id and c.station_id = s.station_id )
         )
         ORDER BY channel_id DESC
         LIMIT 1);
   IF (idt IS NULL) THEN id:=0; ELSE id:=idt; END IF;
   RETURN id;
   END; $$
LANGUAGE 'plpgsql';; 

*/
------------------------------
CREATE OR REPLACE function public.station_clause() RETURNS TEXT AS $$
DECLARE
clause TEXT;
staall integer;    -- # of different station code in the request
stastar integer;    -- # of '*' for station in the request
stalike integer;
BEGIN
clause := '  AND w.station LIKE t.station '; -- the most general clause
staall :=  (select count (distinct t.station) FROM temp_req t);
stastar := (select count (distinct t.station) FROM temp_req t where t.station = '*');
stalike := 
   (select count (distinct t.station) from temp_req t where t.station like '%*%') +
   (select count (distinct t.station) from temp_req t where t.station like '*%') +
   (select count (distinct t.station) from temp_req t where t.station like '%*') +
   (select count (distinct t.station) from temp_req t where t.station like '%?%') +
   (select count (distinct t.station) from temp_req t where t.station like '%?') +
   (select count (distinct t.station) from temp_req t where t.station like '?%');
CASE
   -- il n'y a qu'un seul code station et il est égale = 1
   WHEN ((staall = 1) and (stastar = 1)) THEN
      clause := '';
   -- il y a plusieurs codes station et aucun d'entre eux ne contient de 'µ'
   WHEN (stalike = 0)  THEN
      clause := '  AND w.station = t.station ';
   ELSE
      clause := '  AND w.station LIKE t.station ';
END CASE;
return clause;
END; $$ 
LANGUAGE 'plpgsql';

---
--- construction de la clause where pour le paramètre network=
--- [Pequegnat 2021-03-26]
--- Attention a l'dre des argment dans une clause LIKE. Corrige le bugdes caractères joket non expansé dans le cas 
--- du reseau
--- construction de la clause where pour le paramètre network=
--( )
CREATE OR REPLACE function public.network_clause() RETURNS TEXT AS $$
DECLARE
clause TEXT;
netall integer;    -- # of different network code in the request
netstar integer;    -- # of '*' for network in the request
netlike integer;
BEGIN
clause := ' AND w.network LIKE t.network '; 
netall :=  (select count (distinct t.network) from temp_req t);
netstar := (select count (distinct t.network) from temp_req t where t.network = '*');
netlike := 
   (select count (distinct t.network) from temp_req t where t.network like '%*%') +
   (select count (distinct t.network) from temp_req t where t.network like '*%') +
   (select count (distinct t.network) from temp_req t where t.network like '%*') +
   (select count (distinct t.network) from temp_req t where t.network like '%?%') +
   (select count (distinct t.network) from temp_req t where t.network like '%?') +
   (select count (distinct t.network) from temp_req t where t.network like '?%');
CASE
   -- il n'y a qu'un seul code network et il est égal = '*'
   WHEN ((netall = 1) and (netstar = 1)) THEN
      clause := '';
   -- il y a plusieurs codes network et aucun d'entre eux ne contient de joker
   WHEN (netlike = 0)  THEN
--      clause := ' AND t.network = w.network ';
     clause := ' AND w.network = t.network '; 
   ELSE
--      clause := ' AND t.network LIKE w.network ';
	clause := ' AND w.network LIKE t.network '; 
END CASE;
return clause;
END; $$ 
LANGUAGE 'plpgsql';

CREATE OR REPLACE function public.location_clause() RETURNS TEXT AS $$
DECLARE
clause TEXT;
locall integer;    -- # of different station code in the request
locstar integer;    -- # of '*' for station in the request
loclike integer;
BEGIN
clause := '  AND w.location LIKE t.location '; -- the most general clause
locall :=  (select count (distinct t.location) from temp_req t);
locstar := (select count (distinct t.location) from temp_req t where t.location = '*');
loclike := 
   (select count (distinct t.location) from temp_req t where t.location like '%*%') +
   (select count (distinct t.location) from temp_req t where t.location like '*%') +
   (select count (distinct t.location) from temp_req t where t.location like '%*') +
   (select count (distinct t.location) from temp_req t where t.location like '%?%') +
   (select count (distinct t.location) from temp_req t where t.location like '%?') +
   (select count (distinct t.location) from temp_req t where t.location like '?%');
CASE
   -- il n'y a qu'un seul code location et il est égale = 1
   WHEN ((locall = 1) and (locstar = 1)) THEN
      clause := '';
   -- il y a plusieurs codes station et aucun d'entre eux ne contient de 'µ'
   WHEN (loclike = 0)  THEN
      clause := ' AND w.location = t.location ';
   ELSE
      clause := ' AND w.location LIKE t.location ';
END CASE;
return clause;
END; $$ 
LANGUAGE 'plpgsql';


CREATE OR REPLACE function public.channel_clause() RETURNS TEXT AS $$
DECLARE
clause TEXT;
chaall integer;    -- # of different station code in the request
chastar integer;    -- # of '*' for station in the request
chalike integer;
BEGIN
clause := '  AND w.channel LIKE t.channel '; -- the most general clause
chaall :=  (select count (distinct t.channel) from temp_req t);
chastar := (select count (distinct t.channel) from temp_req t where t.channel = '*');
chalike := 
   (select count (distinct t.channel) from temp_req t where t.channel like '%*%') +
   (select count (distinct t.channel) from temp_req t where t.channel like '*%') +
   (select count (distinct t.channel) from temp_req t where t.channel like '%*') +
   (select count (distinct t.channel) from temp_req t where t.channel like '%?%') +
   (select count (distinct t.channel) from temp_req t where t.channel like '%?') +
   (select count (distinct t.channel) from temp_req t where t.channel like '?%');
CASE
   -- il n'y a qu'un seul code station et il est égale = 1
   WHEN ((chaall = 1) and (chastar = 1)) THEN
      clause := '';
   -- il y a plusieurs codes station et aucun d'entre eux ne contient de 'µ'
   WHEN (chalike = 0)  THEN
      clause := '  AND w.channel = t.channel';
   ELSE
      clause := '  AND w.channel LIKE t.channel ';
END CASE;
return clause;
END; $$ 
LANGUAGE 'plpgsql';

CREATE OR REPLACE function public.mtc_clause() RETURNS TEXT AS $$
-- used to format and optimize a ws station-request 
DECLARE
clause TEXT;
BEGIN
clause := '';
SELECT '  AND w.mtc = TRUE ' 
from temp_req 
where temp_req.matchtimeseries = TRUE and temp_req.level IN ('channel', 'response') INTO clause; 
SELECT '  AND w.mts = TRUE ' 
from temp_req 
where temp_req.matchtimeseries = TRUE and temp_req.level IN ('network', 'station') INTO clause; 
return clause;
END; $$ 
LANGUAGE 'plpgsql';

CREATE OR REPLACE function public.updateafter_clause() RETURNS TEXT AS $$
-- used to format and optimize a ws station-request 
DECLARE
clause TEXT;
BEGIN
clause := '';
SELECT ' AND w.updated >  t.updatedafter '
from temp_req 
where temp_req.textdate7 != '*'  INTO clause;
return clause;
END; $$ 
LANGUAGE 'plpgsql';

CREATE OR REPLACE function public.include_restricted_clause() RETURNS TEXT AS $$
DECLARE
clause TEXT;
BEGIN
clause := '';
-- SELECT '  AND w.open =  TRUE '  
SELECT '  AND w.policy =  ''open'' '  
from temp_req 
where temp_req.includerestricted = FALSE INTO clause;
return clause;
END; $$ 
LANGUAGE 'plpgsql';

-- date de la dernière mise à jour d'un channel
CREATE OR REPLACE function public.last_update_metadata(BIGINT) RETURNS TIMESTAMP WITHOUT TIME ZONE AS $$
-- used to format and optimize a ws station-request 
DECLARE
chaid alias for $1;
mydate TIMESTAMP WITHOUT TIME ZONE; -- return
BEGIN
select max (t.update) FROM public.transaction_integration t, public.channel c where t.source_file = c.source_file and c.channel_id = chaid into mydate;
case when mydate is null THEN mydate := '-infinity';  ELSE mydate := mydate ;
end case;
return mydate;
END; 
$$  LANGUAGE 'plpgsql';
--
/*
CREATE OR REPLACE FUNCTION public.set_channel_id_in_file_table(bigint, bigint, text, text, timestamp without time zone, timestamp without time zone)  RETURNS bigint AS $$
-- un fichier peut être associés à 0, .. channels.
-- Dans ce qui suit, on considère qu'un fichier n'est pas 'orphelin' si il est associé à au moins un channel
   DECLARE
   netid ALIAS FOR $1;      -- code reseau FDSN de la station
   statid ALIAS FOR $2;      -- code station
   loc ALIAS FOR $3;      -- location
   chan ALIAS FOR $4;      -- channel
--    qual ALIAS FOR $5;      -- indice de qualite
   cstart ALIAS FOR $5;      -- date de mise en fonctionnement du channel
   cend ALIAS FOR $6;      -- date d'arret du channel
   id BIGINT ;               -- station_id pour ce channel
   idt BIGINT ;
   BEGIN
   idt := (
         select DISTINCT c.channel_id
         FROM public.channel c, public.station s
         WHERE
         (c.station_id = s.station_id and
         s.network_id = netid and
         s.station_id = statid and
         c.channel = chan and
         c.location = loc
         and cstart BETWEEN c.starttime AND c.endtime )
         OR
         ( c.station_id = s.station_id and
         s.network_id = netid and
         s.station_id = statid and
         c.channel = chan and
         c.location = loc
         AND cend BETWEEN c.starttime AND c.endtime)
         ORDER BY channel_id DESC
         LIMIT 1);
   IF (idt IS NULL) THEN id:=0; ELSE id:=idt; END IF;
   RETURN id;
   END;
$$  LANGUAGE 'plpgsql';
*/
---
-- network,station,location,channel,starttime, endtime

CREATE OR REPLACE FUNCTION public.set_channel_id_in_rbud (TEXT, TEXT, text, text, timestamp without time zone, timestamp without time zone)  RETURNS bigint AS $$
-- un fichier peut être associés à 0, .. channels.
-- Dans ce qui suit, on considère qu'un fichier n'est pas 'orphelin' si il est associé à au moins un channel
   DECLARE
   net ALIAS FOR $1;      -- code reseau FDSN de la station
   sta ALIAS FOR $2;      -- code station
   loc ALIAS FOR $3;      -- location
   chan ALIAS FOR $4;      -- channel
   cstart ALIAS FOR $5;      -- date de mise en fonctionnement du channel
   cend ALIAS FOR $6;      -- date d'arret du channel
   id BIGINT ;               -- station_id pour ce channel
   idt BIGINT ;
   BEGIN
   idt := (
         select DISTINCT c.channel_id
         FROM public.channel c, public.station s, public.network n
         WHERE
         (c.station_id = s.station_id and
         s.network_id = n.network_id and
         s.station = sta and
         n.network = net and
         c.channel = chan and
         c.location = loc
         and cstart BETWEEN c.starttime AND c.endtime )
         OR
         (c.station_id = s.station_id and
         s.network_id = n.network_id and
         s.station = sta and
         n.network = net and
         c.channel = chan and
         c.location = loc
         AND cend BETWEEN c.starttime AND c.endtime)
         ORDER BY channel_id DESC
         LIMIT 1);
   IF (idt IS NULL) THEN id:=0; ELSE id:=idt; END IF;
   RETURN id;
   END;
$$  LANGUAGE 'plpgsql';

/*
CREATE OR REPLACE FUNCTION public.set_station_id_for_data (TEXT, TEXT, timestamp without time zone, timestamp without time zone)  RETURNS bigint AS $$
-- un fichier peut être associés à 0, .. channels.
-- Dans ce qui suit, on considère qu'un fichier n'est pas 'orphelin' si il est associé à au moins un channel
   DECLARE
   net ALIAS FOR $1;      -- code reseau FDSN de la station
   sta ALIAS FOR $2;      -- code station
   cstart ALIAS FOR $3;      -- date de mise en fonctionnement du channel
   cend ALIAS FOR $4;      -- date d'arret du channel
   id BIGINT ;               -- station_id pour ce channel
   idt BIGINT ;
   BEGIN
   idt := (
         select DISTINCT s.station_id
         FROM public.station s, networks n
         WHERE
         (
         s.network_id = n.network_id and
         s.station = sta and
         n.network = net and
         cstart BETWEEN s.starttime AND s.endtime )
         OR
         (s.network_id = n.network_id and
         s.station = sta and
         n.network = net and
          cend BETWEEN s.starttime AND s.endtime)
         ORDER BY station_id DESC
         LIMIT 1);
   IF (idt IS NULL) THEN id:=0; ELSE id:=idt; END IF;
   RETURN id;
   END;
$$  LANGUAGE 'plpgsql';
*/
/*
CREATE OR REPLACE FUNCTION public.set_network_id_for_data (TEXT, TEXT, timestamp without time zone, timestamp without time zone)  RETURNS bigint AS $$
-- un fichier peut être associés à 0, .. channels.
-- Dans ce qui suit, on considère qu'un fichier n'est pas 'orphelin' si il est associé à au moins un channel
   DECLARE
   net ALIAS FOR $1;      -- code reseau FDSN de la station
   cstart ALIAS FOR $3;      -- date de mise en fonctionnement du channel
   cend ALIAS FOR $4;      -- date d'arret du channel
   id BIGINT ;               -- station_id pour ce channel
   idt BIGINT ;
   BEGIN
   idt := (
         select DISTINCT n.network_id
         FROM public.network n
         WHERE
         (
         n.network = net
         and cstart BETWEEN n.starttime AND n.endtime )
         OR
         (n.network = net
         and cend BETWEEN n.starttime AND n.endtime )
         ORDER BY station_id DESC
         LIMIT 1);
   IF (idt IS NULL) THEN id:=0; ELSE id:=idt; END IF;
   RETURN id;
   END;
$$  LANGUAGE 'plpgsql';
*/

CREATE OR REPLACE FUNCTION public.set_network_id_bis (TEXT, integer, integer)  RETURNS bigint AS $$
-- renvoie l'id d'une réseau à partir de son code et des années de débit et de fin.
-- Utilise dans admin/update-user-access.sql
   DECLARE
   net ALIAS FOR $1;      -- code reseau FDSN de la station
   syear ALIAS FOR $2;      -- annee de debut
   eyear ALIAS FOR $3;      -- annee de fin
   idt BIGINT ;            -- return
   id BIGINT ;            -- return
   BEGIN
   idt := (
         select DISTINCT n.network_id
         FROM public.networks n
         WHERE
         n.network = net
         and syear BETWEEN n.start_year AND n.end_year
         AND
         eyear BETWEEN n.start_year AND n.end_year );

   IF (idt IS NULL) THEN idt=0 ; END IF;
   RETURN idt;
   END;
$$  LANGUAGE 'plpgsql';


CREATE OR REPLACE FUNCTION public.set_channel_id(text, text, text, text, timestamp without time zone, timestamp without time zone)  RETURNS bigint AS $$
-- un fichier peut être associés à 0, .. channels.
-- Dans ce qui suit, on considère qu'un fichier n'est pas 'orphelin' si il est associé à au moins un channel
   DECLARE
   net ALIAS FOR $1;      -- code reseau FDSN de la station
   sta ALIAS FOR $2;      -- code station
   loc ALIAS FOR $3;      -- location
   chan ALIAS FOR $4;      -- channel
   cstart ALIAS FOR $5;      -- une date
   cend ALIAS FOR $6;      -- date d'arret du channel
   id BIGINT ;               -- station_id pour ce channel
   idt BIGINT ;
   BEGIN
   idt := (
         select DISTINCT c.channel_id
         FROM public.channel c, public.station s, public.networks n
         WHERE
         (
         s.station = sta and
         n.network = net and
         c.channel = chan and
         c.location = loc and
         c.station_id = s.station_id and
         s.network_id = n.network_id and
         cstart BETWEEN c.starttime AND c.endtime)
         OR
         (
         s.station = sta and
         n.network = net and
         c.channel = chan and
         c.location = loc and
         c.station_id = s.station_id and
         s.network_id = n.network_id and
         cend BETWEEN c.starttime AND c.endtime)
         ORDER BY c.channel_id DESC
         LIMIT 1);
   IF (idt IS NULL) THEN id:=0; ELSE id:=idt; END IF;
   RETURN id;
   END;
$$  LANGUAGE 'plpgsql';
/*

CREATE OR REPLACE FUNCTION public.set_channel_id_in_rbud(text, text, text, text, timestamp without time zone, timestamp without time zone)  RETURNS bigint AS $$
-- un fichier peut être associés à 0, .. channels.
-- Dans ce qui suit, on considère qu'un fichier n'est pas 'orphelin' si il est associé à au moins un channel
   DECLARE
   net ALIAS FOR $1;      -- code reseau FDSN de la station
   sta ALIAS FOR $2;      -- code station
   loc ALIAS FOR $3;      -- location
   chan ALIAS FOR $4;      -- channel
   cstart ALIAS FOR $5;      -- une date
   cend ALIAS FOR $6;      -- date d'arret du channel
   id BIGINT ;               -- station_id pour ce channel
   idt BIGINT ;
   BEGIN
   idt := (
         select DISTINCT c.channel_id
         FROM public.channel c, public.station s, public.networks n
         WHERE
         (c.channel = chan and
         c.location = loc and
         c.station_id = s.station_id and
         s.station = sta and
         n.network = net and
         c.station_id = s.station_id and
         s.network_id = n.network_id and
         cstart BETWEEN c.starttime AND c.endtime)
         OR
         (c.channel = chan and
         c.location = loc and
         c.station_id = s.station_id and
         s.station = sta and
         n.network = net and
         c.station_id = s.station_id and
         s.network_id = n.network_id and
         cend BETWEEN c.starttime AND c.endtime)
         ORDER BY channel_id DESC
         LIMIT 1);
   IF (idt IS NULL) THEN id:=0; ELSE id:=idt; END IF;
   RETURN id;
   END;
$$  LANGUAGE 'plpgsql';
*/

-- ce qui suit est source de confusion et devrait porbabelemnt revu 
-- Les deux fonctions set_network_id ont des signatures différentes
-- dans la première on appel avec une date (n'importe laquelle entre les dates de début et de fin du réseau) et dans la seconde
-- on appelle avec les deux dates de débuts et de fin, supposément egale à celle du réseau
 CREATE OR REPLACE FUNCTION public.set_network_id(text, timestamp without time zone, timestamp without time zone)  RETURNS bigint AS $$
   DECLARE
   net ALIAS FOR $1;      -- code reseau FDSN de la station
   dstart ALIAS FOR $2;      -- une date
   dend ALIAS FOR $3;      -- une date
   idt BIGINT ;
   BEGIN
   idt := (
         select DISTINCT n.network_id
         FROM public.networks n
         WHERE
         n.network = net
         and dstart BETWEEN n.starttime AND n.endtime
         and dend BETWEEN n.starttime AND n.endtime);
   IF (idt IS NULL) THEN idt=0 ; END IF;
   RETURN idt;
   END;
$$  LANGUAGE 'plpgsql';


CREATE OR REPLACE FUNCTION public.set_network_id(text, timestamp without time zone)  RETURNS bigint AS $$
   DECLARE
   net ALIAS FOR $1;      -- code reseau FDSN de la station
   start ALIAS FOR $2;      -- une date
   idt BIGINT ;
   id BIGINT ;
   BEGIN
   idt := (
         select DISTINCT n.network_id
         FROM public.networks n
         WHERE
         n.network = net and start BETWEEN n.starttime AND n.endtime);
   IF (idt IS NULL) THEN id:=0; ELSE id:=idt; END IF;
   RETURN id;
   END;
$$  LANGUAGE 'plpgsql';

/*
CREATE OR REPLACE FUNCTION public.set_network_id_in_rbud(text, timestamp without time zone)  RETURNS bigint AS $$
   DECLARE
   net ALIAS FOR $1;      -- code reseau FDSN de la station
   start ALIAS FOR $2;      -- une date
   idt BIGINT ;
   id BIGINT ;
   BEGIN
   idt := (
         select DISTINCT n.network_id
         FROM public.networks n
         WHERE
         n.network = net and start BETWEEN n.starttime AND n.endtime);
   IF (idt IS NULL) THEN id:=0; ELSE id:=idt; END IF;
   RETURN id;
   END;
$$  LANGUAGE 'plpgsql';
*/

CREATE OR REPLACE FUNCTION public.set_station_id (BIGINT, text, timestamp without time zone)  RETURNS bigint AS $$
   DECLARE
   netid ALIAS FOR $1;      -- id du reseau
   stat ALIAS FOR $2;       -- code station
   start ALIAS FOR $3;      -- un date
   idt BIGINT ;
   id BIGINT ;
   BEGIN
   idt := (
         select DISTINCT s.station_id
         FROM public.station s, public.networks n
         WHERE
         s.station = stat and s.network_id = netid and start BETWEEN s.starttime AND s.endtime);
   IF (idt IS NULL) THEN id:=0; ELSE id:=idt; END IF;
   RETURN id;
   END;
$$  LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION public.set_data_availability(text, INTEGER)  RETURNS boolean AS $$
   DECLARE
   net ALIAS FOR $1;      -- code reseau FDSN de la station
   year ALIAS FOR $2;
   avail boolean;
   open text;
   BEGIN
   avail := false;
   select n.policy from networks n where n.network = net and year BETWEEN start_year and end_year INTO open;
   case
      WHEN (open = 'open' ) then
         avail = true;
      else
         avail = false;
   end case;
   return avail;
   END;
$$  LANGUAGE 'plpgsql';


CREATE OR REPLACE FUNCTION public.set_station_id_in_rbud (text, text, timestamp without time zone, timestamp without time zone)  RETURNS bigint AS $$
   DECLARE
   net ALIAS FOR $1;      -- id du reseau
   stat ALIAS FOR $2;       -- code station
   startd ALIAS FOR $3;      -- date début fichier
   endd ALIAS FOR $4;    -- date fin fichier
   idt BIGINT ;
   id BIGINT ;
   BEGIN
   idt := (
         select DISTINCT s.station_id
         FROM public.station s, public.networks n
         WHERE
         s.station = stat and n.network = net and s.network_id = n.network_id and (startd BETWEEN s.starttime AND s.starttime OR endd BETWEEN s.starttime and s.endtime));
   IF (idt IS NULL) THEN id:=0; ELSE id:=idt; END IF;
   RETURN id;
   END;
$$  LANGUAGE 'plpgsql';


