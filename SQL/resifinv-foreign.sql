-- 
-- pour foreign
-- 
DROP   materialized VIEW if exists public."channel_with_record";
CREATE materialized VIEW public."channel_with_record" AS SELECT DISTINCT
   "n"."network",
	"n"."start_year",
	"n"."end_year",
	"s"."station",
	"c"."location",
	"c"."channel",
	c.starttime,
	c.endtime,
	n.policy
FROM public."channel" "c", public."station" "s", public."networks" "n", public.ws_common w
WHERE  	
	n.network_id != 0 AND  s.station_id != 0 AND c.channel_id != 0 
	AND 	s.station_id = c.station_id
	AND  s.network_id = n.network_id 
	AND w.channel_id = c.channel_id
	AND w.mtc = TRUE
			;
			
create unique index testidx on channel_with_record (network, station, location, channel, starttime, endtime);	

ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT ON TABLES TO seedtree5;
ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT ON TABLES TO seedtree5ro;
ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT ON TABLES TO wsavailability_ro;
ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT ON TABLES TO wsavailability_refresh;
ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT ON TABLES TO resifinv_ro;
GRANT SELECT ON channel_with_record to  wsavailability_refresh;
GRANT SELECT ON channel_with_record to  seedtree5ro;

GRANT SELECT ON TABLE public.rall TO ws_ro;
GRANT SELECT ON TABLE public.rbud TO ws_ro;
GRANT SELECT ON TABLE public.assembleddata TO wsprod;
GRANT SELECT ON TABLE public.assembleddata TO ws_ro;
