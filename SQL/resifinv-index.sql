G--
--  Liste des clefs primaires 
-- 
/*
agency_agency_key
agency_pkey
azimuth_pkey
azimuth_xml_key
comment_pkey
dip_pkey
dip_xml_key
distance_pkey
external_reference_pkey
geology_geology_key
geology_pkey
identifier_pkey
equipment_pkey
channel_pkey1
clockdrift_pkey
clockdrift_xml_key
latitude_pkey
longitude_pkey
networks_network_start_year_key1
networks_pkey1
operator_pkey
aut_user_pkey
uniq_aut_user
response_pkey
samplerate_pkey
samplerate_xml_key
sensitivity_pkey
site_pkey
station_pkey1
transaction_integration_pkey
type_channel_pkey
type_channel_type_channel_key
type_channel_at_channel_pkey
unit_pkey
unit_unit_description_key
vault_pkey
vault_vault_key
waterlevel_pkey

*/
DROP INDEX IF EXISTS ix_azimuth_1 cascade;
CREATE INDEX ix_azimuth_1 on azimuth(azimuth);


DROP INDEX IF EXISTS ix_channel_1 cascade;
DROP INDEX IF EXISTS ix_channel_10 cascade;
DROP INDEX IF EXISTS ix_channel_102 cascade;
DROP INDEX IF EXISTS ix_channel_103 cascade;
DROP INDEX IF EXISTS ix_channel_104 cascade;
DROP INDEX IF EXISTS ix_channel_105 cascade;
DROP INDEX IF EXISTS ix_channel_11 cascade;
DROP INDEX IF EXISTS ix_channel_12 cascade;
DROP INDEX IF EXISTS ix_channel_13 cascade;
DROP INDEX IF EXISTS ix_channel_14 cascade;
DROP INDEX IF EXISTS ix_channel_15 cascade;
DROP INDEX IF EXISTS ix_channel_2 cascade;
DROP INDEX IF EXISTS ix_channel_3 cascade;
DROP INDEX IF EXISTS ix_channel_4 cascade;
DROP INDEX IF EXISTS ix_channel_5 cascade;
DROP INDEX IF EXISTS ix_channel_6 cascade;
DROP INDEX IF EXISTS ix_channel_7 cascade;
DROP INDEX IF EXISTS ix_channel_8 cascade;
DROP INDEX IF EXISTS ix_channel_9 cascade;
DROP INDEX IF EXISTS tmp_ix_channel3 cascade;
DROP INDEX IF EXISTS tmp_ix_channel4 cascade;
DROP INDEX IF EXISTS tmp_ix_channel5 cascade;
DROP INDEX IF EXISTS tmp_ix_channel6 cascade;
DROP INDEX IF EXISTS ix_channel_light_100 cascade;
DROP INDEX IF EXISTS ix_channel_light_106 cascade;


DROP INDEX IF EXISTS ix_channel_001 cascade;
DROP INDEX IF EXISTS ix_channel_002 cascade;
DROP INDEX IF EXISTS ix_channel_003 cascade;
DROP INDEX IF EXISTS ix_channel_004 cascade;
DROP INDEX IF EXISTS ix_channel_005 cascade;
DROP INDEX IF EXISTS ix_channel_006 cascade;
DROP INDEX IF EXISTS ix_channel_007 cascade;
DROP INDEX IF EXISTS ix_channel_008 cascade;
DROP INDEX IF EXISTS ix_channel_009 cascade;
DROP INDEX IF EXISTS ix_channel_010 cascade;
DROP INDEX IF EXISTS ix_channel_011 cascade;
DROP INDEX IF EXISTS ix_channel_012 cascade;
DROP INDEX IF EXISTS ix_channel_013 cascade;
DROP INDEX IF EXISTS ix_channel_014 cascade;
DROP INDEX IF EXISTS ix_channel_015 cascade;
DROP INDEX IF EXISTS ix_channel_016 cascade;
DROP INDEX IF EXISTS ix_channel_017 cascade;
DROP INDEX IF EXISTS ix_channel_018 cascade;
DROP INDEX IF EXISTS ix_channel_019 cascade;
DROP INDEX IF EXISTS ix_channel_020 cascade;
DROP INDEX IF EXISTS ix_channel_021 cascade;
DROP INDEX IF EXISTS ix_channel_022 cascade;
DROP INDEX IF EXISTS ix_channel_023 cascade;
DROP INDEX IF EXISTS ix_channel_024 cascade;
DROP INDEX IF EXISTS ix_channel_025 cascade;
DROP INDEX IF EXISTS ix_channel_026 cascade;
DROP INDEX IF EXISTS ix_channel_027 cascade;
DROP INDEX IF EXISTS ix_channel_028 cascade;
DROP INDEX IF EXISTS ix_channel_029 cascade;
DROP INDEX IF EXISTS trgm_ix_channel_001 cascade;
DROP INDEX IF EXISTS trgm_ix_channel_002 cascade;

CREATE INDEX ix_channel_001 ON public.channel USING btree (channel varchar_pattern_ops); 
CREATE INDEX trgm_ix_channel_001 ON public.channel USING gin (channel gin_trgm_ops);
CREATE INDEX ix_channel_002 ON public.channel USING btree (location varchar_pattern_ops); 
CREATE INDEX trgm_ix_channel_002 ON public.channel USING gin (location gin_trgm_ops);

CREATE INDEX ix_channel_003 ON public.channel USING btree (starttime);
CREATE INDEX ix_channel_004 ON public.channel USING btree (endtime);
CREATE INDEX ix_channel_005 ON public.channel USING btree (azimuth_id);
CREATE INDEX ix_channel_006 ON public.channel USING btree (clockdrift_id);
CREATE INDEX ix_channel_007 ON public.channel USING btree (waterlevel_id);
CREATE INDEX ix_channel_008 ON public.channel USING btree (source_file);
CREATE INDEX ix_channel_009 ON public.channel USING btree (dip_id);
CREATE INDEX ix_channel_010 ON public.channel USING btree (station_id);
CREATE INDEX ix_channel_011 ON public.channel USING btree (latitude_id);
CREATE INDEX ix_channel_012 ON public.channel USING btree (longitude_id);
CREATE INDEX ix_channel_013 ON public.channel USING btree (elevation_id);
CREATE INDEX ix_channel_014 ON public.channel USING btree (depth_id);
CREATE INDEX ix_channel_015 ON public.channel USING btree (samplerate_id);
CREATE INDEX ix_channel_016 ON public.channel USING btree (channel_id,channel);
/*
CREATE INDEX ix_channel_017 ON public.channel USING btree (channel_id,location);
CREATE INDEX ix_channel_018 ON public.channel USING btree (channel_id,starttime,endtime);
CREATE INDEX ix_channel_019 ON public.channel USING btree (channel_id,azimuth_id);
CREATE INDEX ix_channel_020 ON public.channel USING btree (channel_id,clockdrift_id);
CREATE INDEX ix_channel_021 ON public.channel USING btree (channel_id,source_file);
CREATE INDEX ix_channel_022 ON public.channel USING btree (channel_id,station_id);
CREATE INDEX ix_channel_023 ON public.channel USING btree (channel_id,latitude_id);
CREATE INDEX ix_channel_024 ON public.channel USING btree (channel_id,longitude_id);
CREATE INDEX ix_channel_025 ON public.channel USING btree (channel_id,elevation_id);
CREATE INDEX ix_channel_027 ON public.channel USING btree (channel_id,depth_id);
CREATE INDEX ix_channel_028 ON public.channel USING btree (channel_id,samplerate_id);
CREATE INDEX ix_channel_029 ON public.channel USING btree (channel_id,dip_id);
*/

DROP INDEX IF EXISTS ix_clockdrift_1 cascade;
DROP INDEX IF EXISTS ix_clockdrift_001 cascade;
CREATE INDEX ix_clockdrift_001 ON public.clockdrift USING btree (clockdrift);

DROP INDEX IF EXISTS ix_comment_1 cascade;
DROP INDEX IF EXISTS ix_comment_2 cascade;
DROP INDEX IF EXISTS ix_comment_3 cascade;
DROP INDEX IF EXISTS ix_comment_4 cascade;
DROP INDEX IF EXISTS ix_comment_5 cascade;
DROP INDEX IF EXISTS ix_comment_6 cascade;
DROP INDEX IF EXISTS ix_comment_7 cascade;
DROP INDEX IF EXISTS ix_comment_8 cascade;

DROP INDEX IF EXISTS ix_equipment_6 cascade;
DROP INDEX IF EXISTS ix_equipment_7 cascade;


DROP INDEX IF EXISTS ix_comment_001 cascade;
DROP INDEX IF EXISTS ix_comment_002 cascade;
DROP INDEX IF EXISTS ix_comment_003 cascade;
DROP INDEX IF EXISTS ix_comment_004 cascade;
DROP INDEX IF EXISTS ix_comment_005 cascade;
DROP INDEX IF EXISTS ix_comment_006 cascade;
DROP INDEX IF EXISTS ix_comment_007 cascade;
DROP INDEX IF EXISTS ix_comment_008 cascade;
DROP INDEX IF EXISTS ix_comment_009 cascade;

CREATE INDEX ix_comment_001 ON public.comment USING btree (network_id);
CREATE INDEX ix_comment_002 ON public.comment USING btree (station_id);
CREATE INDEX ix_comment_003 ON public.comment USING btree (channel_id);
CREATE INDEX ix_comment_004 ON public.comment USING btree (level);
CREATE INDEX ix_comment_005 ON public.comment USING btree (network_id,level);
CREATE INDEX ix_comment_006 ON public.comment USING btree (station_id,level);
CREATE INDEX ix_comment_007 ON public.comment USING btree (channel_id,level);
CREATE INDEX ix_comment_008 ON public.comment USING btree (network_id,level);
CREATE INDEX ix_comment_009 ON public.comment USING btree (source_file);

DROP INDEX IF EXISTS ix_dip_1 cascade;
DROP INDEX IF EXISTS ix_dip_001 cascade;

CREATE INDEX ix_dip_001 ON public.dip USING btree (dip);

DROP INDEX IF EXISTS ix_distance_1 cascade;
DROP INDEX IF EXISTS ix_distance_2 cascade;
DROP INDEX IF EXISTS ix_distance_3 cascade;
DROP INDEX IF EXISTS ix_distance_4 cascade;
DROP INDEX IF EXISTS temp_ix_distance1 cascade;
DROP INDEX IF EXISTS temp_ix_distance11 cascade;
DROP INDEX IF EXISTS temp_ix_distance2 cascade;
DROP INDEX IF EXISTS ix_distance_4 cascade;
DROP INDEX IF EXISTS ix_distance_001 cascade;
DROP INDEX IF EXISTS ix_distance_002 cascade;
DROP INDEX IF EXISTS ix_distance_003  cascade;
DROP INDEX IF EXISTS ix_distance_004  cascade;
DROP INDEX IF EXISTS ix_distance_005  cascade;
DROP INDEX IF EXISTS ix_distance_006 cascade;


CREATE INDEX ix_distance_001 ON public.distance USING btree (distance);
CREATE INDEX ix_distance_002 ON public.distance USING btree (level);
CREATE INDEX ix_distance_003 ON public.distance USING btree (type);
CREATE INDEX ix_distance_004 ON public.distance USING btree (source_file);
CREATE INDEX ix_distance_005 ON public.distance USING btree (distance_id,distance,level,type);
CREATE INDEX ix_distance_006 ON public.distance USING btree (distance,level,type);


DROP INDEX IF EXISTS ix_equipment_2 cascade;
DROP INDEX IF EXISTS ix_equipment_3 cascade;
DROP INDEX IF EXISTS ix_equipment_4 cascade;
DROP INDEX IF EXISTS ix_equipment_5 cascade;
DROP INDEX IF EXISTS ix_equipment_8 cascade;
DROP INDEX IF EXISTS ix_equipment_9 cascade;
DROP INDEX IF EXISTS ix_equipment_001 cascade;
DROP INDEX IF EXISTS ix_equipment_002 cascade;
DROP INDEX IF EXISTS ix_equipment_003 cascade;
DROP INDEX IF EXISTS ix_equipment_004 cascade;
DROP INDEX IF EXISTS ix_equipment_005 cascade;
DROP INDEX IF EXISTS ix_equipment_006 cascade;
DROP INDEX IF EXISTS ix_equipment_007 cascade;


CREATE INDEX ix_equipment_001 ON public.equipment USING btree (station_id);
CREATE INDEX ix_equipment_002 ON public.equipment USING btree (channel_id);
CREATE INDEX ix_equipment_003 ON public.equipment USING btree (level);
CREATE INDEX ix_equipment_004 ON public.equipment USING btree (type);
CREATE INDEX ix_equipment_005 ON public.equipment USING btree (station_id, level);
CREATE INDEX ix_equipment_006 ON public.equipment USING btree (channel_id, level);
-- CREATE INDEX ix_equipment_007 ON public.equipment USING btree (soure_file);


DROP INDEX IF EXISTS ix_external_reference_1 cascade;
DROP INDEX IF EXISTS ix_external_reference_2 cascade;
DROP INDEX IF EXISTS ix_external_reference_3 cascade;
DROP INDEX IF EXISTS ix_external_reference_4 cascade;
DROP INDEX IF EXISTS ix_external_reference_7 cascade;
DROP INDEX IF EXISTS ix_external_reference_8 cascade;
DROP INDEX IF EXISTS ix_external_reference_9 cascade;

DROP INDEX IF EXISTS ix_external_reference_001 cascade;
DROP INDEX IF EXISTS ix_external_reference_002 cascade;
DROP INDEX IF EXISTS ix_external_reference_003 cascade;
DROP INDEX IF EXISTS ix_external_reference_004 cascade;
DROP INDEX IF EXISTS ix_external_reference_005 cascade;
DROP INDEX IF EXISTS ix_external_reference_006 cascade;
DROP INDEX IF EXISTS ix_external_reference_007 cascade;
DROP INDEX IF EXISTS ix_external_reference_008 cascade;
DROP INDEX IF EXISTS ix_external_reference_009 cascade;

CREATE INDEX ix_external_reference_001 ON public.external_reference USING btree (network_id);
CREATE INDEX ix_external_reference_002 ON public.external_reference USING btree (station_id);
CREATE INDEX ix_external_reference_003 ON public.external_reference USING btree (channel_id);
CREATE INDEX ix_external_reference_004 ON public.external_reference USING btree (level);
CREATE INDEX ix_external_reference_005 ON public.external_reference USING btree (network_id,level);
CREATE INDEX ix_external_reference_006 ON public.external_reference USING btree (station_id,level);
CREATE INDEX ix_external_reference_007 ON public.external_reference USING btree (channel_id,level);
CREATE INDEX ix_external_reference_008 ON public.external_reference USING btree (source_file);


DROP INDEX IF EXISTS ix_identifier_1 cascade;
DROP INDEX IF EXISTS ix_identifier_2 cascade;
DROP INDEX IF EXISTS ix_identifier_3 cascade;
DROP INDEX IF EXISTS ix_identifier_4 cascade;
DROP INDEX IF EXISTS ix_identifier_5 cascade;
DROP INDEX IF EXISTS ix_identifier_7 cascade;
DROP INDEX IF EXISTS ix_identifier_8 cascade;
DROP INDEX IF EXISTS ix_identifier_9 cascade;

DROP INDEX IF EXISTS ix_identifier_001 cascade;
DROP INDEX IF EXISTS ix_identifier_002 cascade;
DROP INDEX IF EXISTS ix_identifier_003 cascade;
DROP INDEX IF EXISTS ix_identifier_004 cascade;
DROP INDEX IF EXISTS ix_identifier_005 cascade;
DROP INDEX IF EXISTS ix_identifier_006 cascade;
DROP INDEX IF EXISTS ix_identifier_007 cascade;
DROP INDEX IF EXISTS ix_identifier_008 cascade;
DROP INDEX IF EXISTS ix_identifier_009 cascade;

CREATE INDEX ix_identifier_001 ON public.identifier USING btree (network_id);
CREATE INDEX ix_identifier_002 ON public.identifier USING btree (station_id);
CREATE INDEX ix_identifier_003 ON public.identifier USING btree (channel_id);
CREATE INDEX ix_identifier_004 ON public.identifier USING btree (network_id,level);
CREATE INDEX ix_identifier_005 ON public.identifier USING btree (station_id, level);
CREATE INDEX ix_identifier_006 ON public.identifier USING btree (channel_id,level);
CREATE INDEX ix_identifier_007 ON public.identifier USING btree (source_file varchar_pattern_ops);


DROP INDEX IF EXISTS ix_latitude_1 cascade;
DROP INDEX IF EXISTS ix_latitude_2 cascade;
DROP INDEX IF EXISTS ix_latitude_3 cascade;
DROP INDEX IF EXISTS ix_latitude_001 cascade;
DROP INDEX IF EXISTS ix_latitude_002 cascade;
DROP INDEX IF EXISTS ix_latitude_003 cascade;
DROP INDEX IF EXISTS ix_latitude_004 cascade;

CREATE INDEX ix_latitude_001 ON public.latitude USING btree (latitude);
CREATE INDEX ix_latitude_002 ON public.latitude USING btree (latitude_id,level);
CREATE INDEX ix_latitude_003 ON public.latitude USING btree (latitude_id,latitude,level);
CREATE INDEX ix_latitude_004 ON public.latitude USING btree (source_file varchar_pattern_ops);
 

DROP INDEX IF EXISTS ix_longitude_1 cascade;
DROP INDEX IF EXISTS ix_longitude_2 cascade;
DROP INDEX IF EXISTS ix_longitude_3 cascade;

DROP INDEX IF EXISTS ix_longitude_001 cascade;
DROP INDEX IF EXISTS ix_longitude_002 cascade;
DROP INDEX IF EXISTS ix_longitude_003 cascade;
DROP INDEX IF EXISTS ix_longitude_004 cascade;

CREATE INDEX ix_longitude_001 ON public.longitude USING btree (longitude);
CREATE INDEX ix_longitude_002 ON public.longitude USING btree (longitude_id,level);
CREATE INDEX ix_longitude_003 ON public.longitude USING btree (longitude_id,longitude,level);
CREATE INDEX ix_longitude_004 ON public.longitude USING btree (source_file varchar_pattern_ops);

DROP INDEX IF EXISTS ix_networks_1 cascade;
DROP INDEX IF EXISTS ix_networks_10 cascade;
DROP INDEX IF EXISTS ix_networks_2 cascade;
DROP INDEX IF EXISTS ix_networks_3 cascade;
DROP INDEX IF EXISTS ix_networks_4 cascade;
DROP INDEX IF EXISTS ix_networks_5 cascade;
DROP INDEX IF EXISTS ix_networks_6 cascade;
DROP INDEX IF EXISTS ix_networks_7 cascade;
DROP INDEX IF EXISTS ix_networks_8 cascade;
DROP INDEX IF EXISTS ix_networks_9 cascade;

DROP INDEX IF EXISTS ix_networks_001 cascade;
DROP INDEX IF EXISTS trgm_ix_network_001 cascade;
DROP INDEX IF EXISTS ix_networks_002 cascade;
DROP INDEX IF EXISTS ix_networks_003 cascade;
DROP INDEX IF EXISTS ix_networks_004 cascade;
DROP INDEX IF EXISTS ix_networks_005 cascade;
DROP INDEX IF EXISTS ix_networks_006 cascade;
DROP INDEX IF EXISTS ix_networks_007 cascade;
DROP INDEX IF EXISTS ix_networks_008 cascade;
DROP INDEX IF EXISTS ix_networks_009 cascade;
DROP INDEX IF EXISTS ix_networks_010 cascade;
DROP INDEX IF EXISTS ix_networks_011 cascade;

DROP INDEX ix_networks_001 CASCADE;
CREATE INDEX ix_networks_001 ON public.networks USING btree (network varchar_pattern_ops);
CREATE INDEX trgm_ix_network_001 ON public.networks USING gin (network gin_trgm_ops);
CREATE INDEX ix_networks_002 ON public.networks USING btree (network, starttime, endtime);
CREATE INDEX ix_networks_003 ON public.networks USING btree (network, start_year);
CREATE INDEX ix_networks_004 ON public.networks USING btree (network, start_year, end_year);
CREATE INDEX ix_networks_005 ON public.networks USING btree (network, starttime);
CREATE INDEX ix_networks_006 ON public.networks USING btree (network, endtime);


DROP INDEX IF EXISTS ix_operator_1 cascade;
DROP INDEX IF EXISTS ix_operator_2 cascade;
DROP INDEX IF EXISTS ix_operator_3 cascade;
DROP INDEX IF EXISTS ix_operator_4 cascade;
DROP INDEX IF EXISTS ix_operator_7 cascade;
DROP INDEX IF EXISTS ix_operator_8 cascade;

DROP INDEX IF EXISTS ix_operator_001 cascade;
DROP INDEX IF EXISTS ix_operator_002 cascade;
DROP INDEX IF EXISTS ix_operator_003 cascade;
DROP INDEX IF EXISTS ix_operator_004 cascade;
DROP INDEX IF EXISTS ix_operator_005 cascade;
DROP INDEX IF EXISTS ix_operator_006 cascade;

CREATE INDEX ix_operator_001 ON public.operator USING btree (network_id);
CREATE INDEX ix_operator_002 ON public.operator USING btree (station_id);
CREATE INDEX ix_operator_003 ON public.operator USING btree (source_file);
CREATE INDEX ix_operator_004 ON public.operator USING btree (level);
CREATE INDEX ix_operator_005 ON public.operator USING btree (network_id,level);
CREATE INDEX ix_operator_006 ON public.operator USING btree (station_id,level);

DROP INDEX IF EXISTS ix_rall_11 cascade;
DROP INDEX IF EXISTS ix_rall_0 cascade;
DROP INDEX IF EXISTS ix_rall_1 cascade;
DROP INDEX IF EXISTS ix_rall_10 cascade;
DROP INDEX IF EXISTS ix_rall_2 cascade;
DROP INDEX IF EXISTS ix_rall_3 cascade;
DROP INDEX IF EXISTS ix_rall_4 cascade;
DROP INDEX IF EXISTS ix_rall_5 cascade;
DROP INDEX IF EXISTS ix_rall_6 cascade;
DROP INDEX IF EXISTS ix_rall_7 cascade;
DROP INDEX IF EXISTS ix_rall_n_100 cascade;
DROP INDEX IF EXISTS ix_rall_n_4 cascade;


DROP INDEX IF EXISTS ix_rall_001 cascade;
DROP INDEX IF EXISTS ix_rall_002 cascade;
DROP INDEX IF EXISTS ix_rall_003 cascade;
DROP INDEX IF EXISTS ix_rall_004 cascade;
DROP INDEX IF EXISTS ix_rall_005 cascade;
DROP INDEX IF EXISTS ix_rall_006 cascade;
DROP INDEX IF EXISTS ix_rall_007 cascade;
DROP INDEX IF EXISTS ix_rall_008 cascade;
DROP INDEX IF EXISTS ix_rall_009 cascade;
DROP INDEX IF EXISTS ix_rall_010 cascade;
DROP INDEX IF EXISTS ix_rall_011 cascade;
DROP INDEX IF EXISTS ix_rall_012 cascade;
DROP INDEX IF EXISTS ix_rall_013 cascade;
DROP INDEX IF EXISTS ix_rall_014 cascade;
DROP INDEX IF EXISTS ix_rall_015 cascade;
DROP INDEX IF EXISTS ix_rall_016 cascade;
DROP INDEX IF EXISTS ix_rall_017 cascade;
DROP INDEX IF EXISTS ix_rall_018 cascade;


DROP INDEX IF EXISTS trgm_ix_rall_006 cascade;
DROP INDEX IF EXISTS trgm_ix_rall_007 cascade;
DROP INDEX IF EXISTS trgm_ix_rall_008 cascade;
DROP INDEX IF EXISTS trgm_ix_rall_009 cascade;

DROP INDEX IF EXISTS trgm_ix_rall_002 cascade;
DROP INDEX IF EXISTS trgm_ix_rall_003 cascade;
DROP INDEX IF EXISTS trgm_ix_rall_004 cascade;
DROP INDEX IF EXISTS trgm_ix_rall_005 cascade;

CREATE INDEX ix_rall_001 ON public.rall USING btree (channel_id);
CREATE INDEX ix_rall_002 ON public.rall USING btree (station_id);
CREATE INDEX ix_rall_003 ON public.rall USING btree (network_id);
CREATE INDEX ix_rall_004 ON public.rall USING btree (network_id,station_id,channel_id);
CREATE INDEX ix_rall_005 ON public.rall USING btree (network,station,location,channel);
CREATE INDEX ix_rall_006 ON public.rall USING btree (network);
CREATE INDEX ix_rall_007 ON public.rall USING btree (station);
CREATE INDEX ix_rall_008 ON public.rall USING btree (location);
CREATE INDEX ix_rall_009 ON public.rall USING btree (channel);
CREATE INDEX trgm_ix_rall_006 ON public.rall USING gin (network gin_trgm_ops);
CREATE INDEX trgm_ix_rall_007 ON public.rall USING gin (station gin_trgm_ops);
CREATE INDEX trgm_ix_rall_008 ON public.rall USING gin (channel gin_trgm_ops);
CREATE INDEX trgm_ix_rall_009 ON public.rall USING gin (location gin_trgm_ops);
CREATE INDEX ix_rall_010 ON public.rall USING btree (starttime, endtime);
CREATE INDEX ix_rall_011 ON public.rall USING btree (source_file);
CREATE INDEX ix_rall_012 ON public.rall USING btree (year);
CREATE INDEX ix_rall_013 ON public.rall USING btree (starttime);
CREATE INDEX ix_rall_014 ON public.rall USING btree (endtime);
CREATE INDEX ix_rall_015 ON public.rall USING btree (quality);
CREATE INDEX ix_rall_016 ON public.rall USING btree (availability);
CREATE INDEX ix_rall_017 ON public.rall USING btree (channel_id, starttime,endtime);
CREATE INDEX ix_rall_018 ON public.rall USING btree (channel_id, starttime, endtime,quality);



--- ????CREATE INDEX ix_rall_013 ON public.rall USING btree (channel_id, network, station, location, channel,starttime,endtime);

DROP INDEX IF EXISTS ix_rbud_11 cascade;
DROP INDEX IF EXISTS ix_rbud_n_100 cascade;
DROP INDEX IF EXISTS ix_rbud_n_4 cascade;

DROP INDEX IF EXISTS ix_rbud_001 cascade;
DROP INDEX IF EXISTS ix_rbud_002 cascade;
DROP INDEX IF EXISTS ix_rbud_003 cascade;
DROP INDEX IF EXISTS ix_rbud_004 cascade;
DROP INDEX IF EXISTS ix_rbud_005 cascade;
DROP INDEX IF EXISTS ix_rbud_006 cascade;
DROP INDEX IF EXISTS ix_rbud_007 cascade;
DROP INDEX IF EXISTS ix_rbud_008 cascade;
DROP INDEX IF EXISTS ix_rbud_009 cascade;
DROP INDEX IF EXISTS ix_rbud_010 cascade;
DROP INDEX IF EXISTS ix_rbud_011 cascade;
DROP INDEX IF EXISTS ix_rbud_012 cascade;
DROP INDEX IF EXISTS ix_rbud_013 cascade;
DROP INDEX IF EXISTS ix_rbud_014 cascade;
DROP INDEX IF EXISTS ix_rbud_015 cascade;
DROP INDEX IF EXISTS ix_rbud_016 cascade;
DROP INDEX IF EXISTS ix_rbud_017 cascade;
DROP INDEX IF EXISTS ix_rbud_018 cascade;


DROP INDEX IF EXISTS trgm_ix_rbud_006 cascade;
DROP INDEX IF EXISTS trgm_ix_rbud_007 cascade;
DROP INDEX IF EXISTS trgm_ix_rbud_008 cascade;
DROP INDEX IF EXISTS trgm_ix_rbud_009 cascade;

DROP INDEX IF EXISTS trgm_ix_rbud_002 cascade;
DROP INDEX IF EXISTS trgm_ix_rbud_003 cascade;
DROP INDEX IF EXISTS trgm_ix_rbud_004 cascade;
DROP INDEX IF EXISTS trgm_ix_rbud_005 cascade;

CREATE INDEX ix_rbud_001 ON public.rbud USING btree (channel_id);
CREATE INDEX ix_rbud_002 ON public.rbud USING btree (station_id);
CREATE INDEX ix_rbud_003 ON public.rbud USING btree (network_id);
CREATE INDEX ix_rbud_004 ON public.rbud USING btree (network_id,station_id,channel_id);
CREATE INDEX ix_rbud_005 ON public.rbud USING btree (network,station,location,channel);
CREATE INDEX ix_rbud_006 ON public.rbud USING btree (network);
CREATE INDEX ix_rbud_007 ON public.rbud USING btree (station);
CREATE INDEX ix_rbud_008 ON public.rbud USING btree (location);
CREATE INDEX ix_rbud_009 ON public.rbud USING btree (channel);
CREATE INDEX trgm_ix_rbud_006 ON public.rbud USING gin (network gin_trgm_ops);
CREATE INDEX trgm_ix_rbud_007 ON public.rbud USING gin (station gin_trgm_ops);
CREATE INDEX trgm_ix_rbud_008 ON public.rbud USING gin (channel gin_trgm_ops);
CREATE INDEX trgm_ix_rbud_009 ON public.rbud USING gin (location gin_trgm_ops);

CREATE INDEX ix_rbud_010 ON public.rbud USING btree (starttime, endtime);
CREATE INDEX ix_rbud_011 ON public.rbud USING btree (source_file);
CREATE INDEX ix_rbud_012 ON public.rbud USING btree (year);
CREATE INDEX ix_rbud_013 ON public.rbud USING btree (starttime);
CREATE INDEX ix_rbud_014 ON public.rbud USING btree (endtime);
CREATE INDEX ix_rbud_015 ON public.rbud USING btree (quality);
CREATE INDEX ix_rbud_016 ON public.rbud USING btree (availability);
CREATE INDEX ix_rbud_017 ON public.rbud USING btree (channel_id, starttime,endtime);
CREATE INDEX ix_rbud_018 ON public.rbud USING btree (channel_id, starttime, endtime,quality);

DROP INDEX IF EXISTS ix_response_0 cascade;
DROP INDEX IF EXISTS ix_response_1 cascade;

DROP INDEX IF EXISTS ix_response_001 cascade;
DROP INDEX IF EXISTS ix_response_002 cascade;

CREATE INDEX ix_response_001 ON public.response USING btree (source_file);
CREATE INDEX ix_response_002 ON public.response USING btree (channel_id);

DROP INDEX IF EXISTS ix_samplerate_1 cascade;
DROP INDEX IF EXISTS ix_samplerate_001 cascade;

CREATE INDEX ix_samplerate_001 ON public.samplerate USING btree (samplerate);

DROP INDEX IF EXISTS ix_sensitivity_1 cascade;
DROP INDEX IF EXISTS ix_sensitivity_2 cascade;
DROP INDEX IF EXISTS ix_sensitivity_3 cascade;
DROP INDEX IF EXISTS ix_sensitivity_4 cascade;
DROP INDEX IF EXISTS ix_sensitivity_5 cascade;

DROP INDEX IF EXISTS ix_sensitivity_001 cascade;
DROP INDEX IF EXISTS ix_sensitivity_002 cascade;
DROP INDEX IF EXISTS ix_sensitivity_003 cascade;
DROP INDEX IF EXISTS ix_sensitivity_004 cascade;
DROP INDEX IF EXISTS ix_sensitivity_005 cascade;

CREATE INDEX ix_sensitivity_001 ON public.sensitivity USING btree (frequency);
CREATE INDEX ix_sensitivity_002 ON public.sensitivity USING btree (sensitivity);
CREATE INDEX ix_sensitivity_003 ON public.sensitivity USING btree (channel_id);
CREATE INDEX ix_sensitivity_004 ON public.sensitivity USING btree (unit_id);
CREATE INDEX ix_sensitivity_005 ON public.sensitivity USING btree (source_file);

DROP INDEX IF EXISTS ix_site_1 cascade;
DROP INDEX IF EXISTS ix_site_2 cascade;

DROP INDEX IF EXISTS ix_site_001 cascade;
DROP INDEX IF EXISTS ix_site_002 cascade;

CREATE INDEX ix_site_001 ON public.site USING btree (site);
CREATE INDEX ix_site_002 ON public.site USING btree (source_file);

------------------ station------------------------------

DROP INDEX IF EXISTS ix_station_1 cascade;
DROP INDEX IF EXISTS ix_station_10 cascade;
DROP INDEX IF EXISTS ix_station_11 cascade;
DROP INDEX IF EXISTS ix_station_12 cascade;
DROP INDEX IF EXISTS ix_station_13 cascade;
DROP INDEX IF EXISTS ix_station_14 cascade;
DROP INDEX IF EXISTS ix_station_2 cascade;
DROP INDEX IF EXISTS ix_station_3 cascade;
DROP INDEX IF EXISTS ix_station_4 cascade;
DROP INDEX IF EXISTS ix_station_6 cascade;
DROP INDEX IF EXISTS ix_station_7 cascade;
DROP INDEX IF EXISTS ix_station_8 cascade;
DROP INDEX IF EXISTS ix_station_9 cascade;

DROP INDEX IF EXISTS ix_station_001 cascade;
DROP INDEX IF EXISTS ix_station_002 cascade;
DROP INDEX IF EXISTS ix_station_003 cascade;
DROP INDEX IF EXISTS ix_station_004 cascade;
DROP INDEX IF EXISTS ix_station_005 cascade;
DROP INDEX IF EXISTS ix_station_006 cascade;
DROP INDEX IF EXISTS ix_station_007 cascade;
DROP INDEX IF EXISTS ix_station_008 cascade;
DROP INDEX IF EXISTS ix_station_009 cascade;
DROP INDEX IF EXISTS ix_station_010 cascade;
DROP INDEX IF EXISTS ix_station_011 cascade;
DROP INDEX IF EXISTS ix_station_012 cascade;
DROP INDEX IF EXISTS ix_station_013 cascade;
DROP INDEX IF EXISTS tgrm_ix_station_001 cascade;

DROP INDEX ix_station_001 cascade;
CREATE INDEX ix_station_001 ON public.station USING btree (station varchar_pattern_ops);

CREATE INDEX tgrm_ix_station_001 ON public.station USING gin (station gin_trgm_ops);
CREATE INDEX ix_station_002 ON public.station USING btree (site_id);
CREATE INDEX ix_station_003 ON public.station USING btree (geology_id);
CREATE INDEX ix_station_004 ON public.station USING btree (vault_id);
CREATE INDEX ix_station_005 ON public.station USING btree (waterlevel_id);
CREATE INDEX ix_station_006 ON public.station USING btree (source_file varchar_pattern_ops);
CREATE INDEX ix_station_007 ON public.station USING btree (network_id);
CREATE INDEX ix_station_008 ON public.station USING btree (starttime);
CREATE INDEX ix_station_009 ON public.station USING btree (endtime, endtime);
CREATE INDEX ix_station_010 ON public.station USING btree (endtime);
CREATE INDEX ix_station_011 ON public.station USING btree (latitude_id);
CREATE INDEX ix_station_012 ON public.station USING btree (longitude_id);
CREATE INDEX ix_station_013 ON public.station USING btree (elevation_id);
 

DROP INDEX IF EXISTS ix_transaction_code cascade;
DROP INDEX IF EXISTS ix_transaction_source_file cascade;
DROP INDEX IF EXISTS ix_transaction_update cascade;

DROP INDEX IF EXISTS ix_transaction_001 cascade;
DROP INDEX IF EXISTS ix_transaction_002 cascade;
DROP INDEX IF EXISTS ix_transaction_003 cascade;

CREATE INDEX ix_transaction_001 ON public.transaction_integration USING btree (code);
CREATE INDEX ix_transaction_002 ON public.transaction_integration USING btree (source_file);
CREATE INDEX ix_transaction_003 ON public.transaction_integration USING btree (update);

DROP INDEX IF EXISTS ix_waterlevel_1 cascade;
DROP INDEX IF EXISTS ix_waterlevel_001 cascade;

