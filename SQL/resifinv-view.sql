--
-- resifinv-view.sql
-- Création de vues matérialisées utilisée dans les requêtes de métadonnées
-- \set ON_ERROR_STOP 1
--
-- schemas :
-- stationrequest : pour l'exécution du ws station
-- admin : pour différentes table d'administration
-- dataselect : pour  l'exécution du ws dataselect
-- 
DROP SCHEMA IF EXISTS admin CASCADE; CREATE SCHEMA admin OWNER resifinvprod;
DROP SCHEMA IF EXISTS stationrequest CASCADE; CREATE SCHEMA stationrequest;
DROP SCHEMA IF EXISTS dataselect CASCADE; CREATE SCHEMA dataselect;
ALTER SCHEMA stationrequest OWNER TO wsstation;
ALTER SCHEMA dataselect OWNER TO wsdataselect;
--
-- implementation coherence avec seedtree, pour le le ws availability.
-- elle doit être rafraichie à l'issue de chaque transaction d'intégration
-- TODO : travailler sur des index, à voir avec JT
-- 
DROP VIEW if exists public.resif_nslc CASCADE;
CREATE VIEW public.resif_nslc AS 
SELECT DISTINCT 
n.network,
s.station,
c.location,
c.channel,
c.source_file
FROM public.networks n, public.station s, public.channel c
where
n.network_id = s.network_id and
s.station_id = c.station_id
UNION
-- cette requête UNION  permet de tenir compte des volumes XML qui ne comport(ai)ent pas de défintion de channel(s)
SELECT DISTINCT 
n.network,
s.station,
'#','#',
s.source_file
FROM public.networks n, public.station s, public.channel c
where 
n.network_id = s.network_id and s.source_file NOT IN 
(select distinct c.source_file 
FROM networks n, station s, channel c
where
n.network_id = s.network_id and
s.station_id = c.station_id)
; 
--
-- pour distinguer les operateurs de réseaux et les operateurs de station
--
DROP VIEW if exists public.network_operator CASCADE;
CREATE VIEW public.network_operator AS
SELECT DISTINCT 
o.network_id, 
string_agg(o.xml, '')
FROM operator o 
where o.xml != '' and o.network_id != 0
GROUP BY 1;

--
-- pour distinguer les commentaires de réseaux des commentaires de station et de channel
-- vue logique introduite dans la migration 20201022_migration_fix_comments_network_level.sql
-- 
--
DROP VIEW if exists public.network_comment CASCADE;
CREATE VIEW public.network_comment AS
SELECT DISTINCT 
c.network_id, 
c.xml
FROM public.comment c
where c.xml != '' and c.network_id != 0 and c.level = 'n';

-- droit d'accès pour les données restreintes
-- 
DROP VIEW if exists public.aut_user cascade;
CREATE VIEW public.aut_user AS
SELECT 
	 a.network_id,
    a.network,
    a.start_year,
    a.end_year,
    a.name
   FROM public.resif_users a
UNION
 SELECT b.network_id,
    b.network,
    b.start_year,
    b.end_year,
    b.name
    FROM public.eida_temp_users b;
--
-- vues matérialisées et index associés
DROP MATERIALIZED VIEW IF exists public.channel_light CASCADE;
-- EXPLAIN ANALYZE
CREATE MATERIALIZED VIEW  public.channel_light  AS SELECT DISTINCT
c.channel_id,
n.network,
s.station,
c.location,
c.channel,
c.starttime,
c.endtime,
s.station_id,
n.network_id
from public.networks n, public.station s, public.channel c 
where n.network_id = s.network_id and c.station_id = s.station_id;

CREATE UNIQUE INDEX index_channel_light ON channel_light(channel_id); 


DROP MATERIALIZED VIEW IF exists public.ws_common CASCADE;
-- DROP TABLE IF EXISTS  ws_common CASCADE;
-- EXPLAIN ANALYZE
CREATE MATERIALIZED VIEW  public.ws_common AS SELECT DISTINCT
	-- nextval('ws_common_id_seq') as ws_common_id,
	c.channel_id,
	n.network_id,
	s.station_id,
	n.network,
	s.station,
	c.location,
	c.channel,
	n.start_year,
	n.end_year,
	n.policy AS policy,
	las.latitude AS latitude,
	los.longitude AS longitude,
	n.starttime AS nstarttime,
	n.endtime AS nendtime,
	s.starttime AS sstarttime,
	s.endtime AS sendtime,
	c.starttime AS cstarttime, 					
	c.endtime  AS cendtime,
	"mtc_channel"(c.channel_id) as mtc,
	"mtc_station"(c.station_id) as mts,
	last_update_metadata(c.channel_id) AS updated
FROM 		public.networks n,
			public.channel c, 
			public.station s, 
			public.latitude las, 
			public.longitude los
WHERE   	n.network_id != 0 and s.station_id != 0 and c.channel_id != 0 and
			las.latitude_id != 0 and los.longitude_id != 0  
			AND 	s.network_id = n.network_id
			AND 	s.station_id = c.station_id
			and 	s.latitude_id = las.latitude_id
			AND 	s.longitude_id = los.longitude_id
			;
CREATE INDEX ix_ws_1 ON ws_common(network_id);
CREATE INDEX ix_ws_2 ON ws_common(station_id);
CREATE UNIQUE INDEX ix_ws_3 ON ws_common(channel_id);

CREATE INDEX ix_ws_4 ON ws_common(channel);
CREATE INDEX trgm_ix_ws_4 ON ws_common USING gin (channel gin_trgm_ops);
CREATE INDEX ix_ws_5 ON ws_common(location);
CREATE INDEX trgm_ix_ws_5 ON ws_common USING gin (location gin_trgm_ops);
CREATE INDEX ix_ws_6 ON ws_common(network);
CREATE INDEX trgm_ix_ws_6 ON ws_common USING gin (network gin_trgm_ops);
CREATE INDEX ix_ws_7 ON ws_common(station);
CREATE INDEX trgm_ix_ws_7 ON ws_common USING gin (station gin_trgm_ops);

CREATE INDEX ix_ws_001 ON ws_common(nstarttime);
CREATE INDEX ix_ws_002 ON ws_common(nendtime);
CREATE INDEX ix_ws_003 ON ws_common(nstarttime, nendtime);
CREATE INDEX ix_ws_004 ON ws_common(sstarttime);
CREATE INDEX ix_ws_005 ON ws_common(sendtime);
CREATE INDEX ix_ws_006 ON ws_common(sstarttime, sendtime);
CREATE INDEX ix_ws_007 ON ws_common(cstarttime);
CREATE INDEX ix_ws_008 ON ws_common(cendtime);
CREATE INDEX ix_ws_009 ON ws_common(cstarttime, cendtime);
CREATE INDEX ix_ws_010 ON ws_common(latitude);
CREATE INDEX ix_ws_011 ON ws_common(longitude);
CREATE INDEX ix_ws_012 ON ws_common(longitude,latitude);
CREATE INDEX ix_ws_013 ON ws_common(start_year);
CREATE INDEX ix_ws_014 ON ws_common(end_year);
CREATE INDEX ix_ws_015 ON ws_common(network,start_year, end_year);
CREATE INDEX ix_ws_016 ON ws_common(mtc);
CREATE INDEX ix_ws_019 ON ws_common(mts);
CREATE INDEX ix_ws_017 ON ws_common(policy);
CREATE INDEX ix_ws_018 ON ws_common(updated);

-- EXPLAIN ANALYZE
CREATE MATERIALIZED VIEW  public.ws_network_text AS SELECT DISTINCT
w.channel_id,
"network-txt" (w.network_id)::TEXT AS nettext
FROM public.ws_common w;

CREATE UNIQUE INDEX ix_ws_network_text_1 ON ws_network_text(channel_id);

-- EXPLAIN ANALYZE
CREATE MATERIALIZED VIEW  public.ws_station_text AS SELECT DISTINCT
w.channel_id,
"station-txt" (w.station_id)::TEXT AS statext
FROM public.ws_common w;
                        
CREATE UNIQUE INDEX ix_ws_station_text_1 ON ws_station_text(channel_id);

-- EXPLAIN ANALYZE
CREATE MATERIALIZED VIEW  public.ws_channel_text AS SELECT DISTINCT
w.channel_id,
"channel-txt" (w.channel_id)::TEXT AS chatext
FROM public.ws_common w
WHERE 
"channel-txt" (w.channel_id)::TEXT is not null and
"channel-txt" (w.channel_id)::TEXT != '';

CREATE UNIQUE INDEX ix_ws_channel_text_1 ON ws_channel_text(channel_id);
              
-- EXPLAIN ANALYZE
CREATE MATERIALIZED VIEW  public.ws_network_xml  AS SELECT DISTINCT
w.channel_id,
"network-xml" (w.network_id)::TEXT AS xmlnet1
FROM public.ws_common w;

CREATE  UNIQUE INDEX ix_ws_network_xml_1 ON ws_network_xml(channel_id);

-- EXPLAIN ANALYZE

CREATE MATERIALIZED VIEW  ws_station_xml  AS SELECT DISTINCT
w.channel_id,
"station-xml1"(w.station_id)::TEXT AS xmlstat1,
"station-xml11"(w.station_id)::TEXT AS xmlstat11,
"station-xml22"(w.station_id)::TEXT AS xmlstat22
FROM public.ws_common w
WHERE "station-xml1"(w.station_id)::TEXT != '';

CREATE UNIQUE INDEX ix_ws_station_xml_1 ON ws_station_xml(channel_id);

-- EXPLAIN ANALYZE
CREATE MATERIALIZED VIEW  public.ws_channel_xml  AS SELECT DISTINCT
w.channel_id,
"channel-xml1" (w.channel_id)::TEXT AS xmlchan1,
"sensitivity-xml"(w.channel_id)::TEXT AS xmlchan2
FROM public.ws_common w;

CREATE UNIQUE  INDEX ix_ws_channel_xml_1 ON ws_channel_xml(channel_id);


select count(*) from public.ws_network_text;
select count(*) from public.ws_station_text;
select count(*) from public.ws_channel_text;
select count(*) from public.ws_network_xml;
select count(*) from public.ws_station_xml;
select count(*) from public.ws_channel_xml;
