
CREATE OR REPLACE FUNCTION public.set_network_id(text, timestamp without time zone, timestamp without time zone)  RETURNS bigint AS $$
	DECLARE 
	net		ALIAS FOR $1;		-- code reseau FDSN de la station
	dstart		ALIAS FOR $2;		-- une date
	dend		ALIAS FOR $3;		-- une date
	idt		BIGINT ;
	BEGIN
	idt  := (
			select DISTINCT n.network_id 
			FROM public.networks n
			WHERE 
			n.network = net 
			and dstart BETWEEN n.starttime AND n.endtime
			and dend BETWEEN n.starttime AND n.endtime);
	IF (idt IS NULL) THEN idt=0 ; END IF;
	RETURN idt;
	END; 
$$  LANGUAGE 'plpgsql';

\q
CREATE OR REPLACE FUNCTION public.set_network_id_bis (TEXT, integer, integer)  RETURNS bigint AS $$
-- renvoie l'id d'une réseau à partir de son code et des années de débit et de fin.
-- Utilise dans admin/update-user-access.sql
	DECLARE  
	net		ALIAS FOR $1;		-- code reseau FDSN de la station
	syear		ALIAS FOR $2;		-- annee de debut
	eyear		ALIAS FOR $3;		-- annee de fin
	idt		BIGINT ;				-- return
	id			BIGINT ;				-- return
	BEGIN
	idt  := (
			select DISTINCT n.network_id 
			FROM public.networks n
			WHERE 
			n.network = net
			and syear BETWEEN n.start_year AND n.end_year 
			AND
			eyear BETWEEN n.start_year AND n.end_year );
			
	IF (idt IS NULL) THEN idt=0 ; END IF;
	RETURN idt;
	END;
$$  LANGUAGE 'plpgsql';
\q


select distinct  n.network, s.station, o.operator, public."operator-xml" (s.station_id, 's', n.network, n.description) from station s, networks n, operator o where s.network_id = n.network_id and o.station_id = s.station_id and s.station_id != 0 and o.level = 's'  order by  n.network,s.station;

\q

select distinct  public."operator-xml" (s.station_id, 's', n.network, n.description) from 
station s, networks n, operator o where s.network_id = n.network_id and o.station_id = s.station_id and s.station_id != 0 and o.level = 's'  order by  n.network,s.station;
