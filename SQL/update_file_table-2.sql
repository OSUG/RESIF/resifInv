--
-- cette mise a jour est destinée à rensienger le statut d'un fichier afin de ne pas exposer via timeseris, timeseriesplot des données sous embargo.
--

alter table rall drop column availability cascade;
alter table rbud drop column availability cascade;

alter table rall add column availability BOOLEAN default TRUE;
alter table rbud add column availability BOOLEAN default TRUE;



CREATE OR REPLACE FUNCTION set_data_availability (TEXT, INTEGER) RETURNS BOOLEAN AS $$
-- un fichier herite de la policy du network auquel il appartient
	DECLARE 
	net		   ALIAS FOR $1;		-- code reseau FDSN de la station
	annee			ALIAS FOR $2;				-- annnée de la date de début de fichier
	avl  			BOOLEAN ;				-- true if open data
	status		TEXT;
	BEGIN
	avl='false';
		
	select distinct n.policy 
					from networks n
					where
					n.network = net and 
					annee between n.start_year and n.end_year
	INTO status;
	case WHEN (status = 'open') THEN 
			avl := 'true'; 
	ELSE avl:='false';
	END CASE ;
	return avl;
	END; $$
LANGUAGE 'plpgsql';


update rall set availability = set_data_availability (network, year) where network like 'FR';
update rbud set availability = set_data_availability (network, year );

