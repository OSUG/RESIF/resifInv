

CREATE OR REPLACE FUNCTION set_channel_id_in_file_table (TEXT, TEXT, TEXT, TEXT, TIMESTAMP WITHOUT TIME ZONE, TIMESTAMP WITHOUT TIME ZONE) RETURNS BIGINT AS $$
-- un fichier peut être associés à 0, .. channels.
-- Dans ce qui suit, on considère qu'un fichier n'est pas 'orphelins' si il est associé à au moins un channel
	DECLARE 
	net		ALIAS FOR $1;		-- code reseau FDSN de la station
	stat		ALIAS FOR $2;		-- code station
	loc		ALIAS FOR $3;		-- location
	chan		ALIAS FOR $4;		-- channel
	cstart	ALIAS FOR $5;		-- date de mise en fonctionnement du channel
	cend		ALIAS FOR $6;		-- date d'arret du channel
	id  		BIGINT ;					-- channel_id pour ce channel
	idt		BIGINT ;
	BEGIN
	idt:=0;
	idt  := (select DISTINCT w.channel_id 
			FROM ws_common w
			WHERE 
			(w.network = net AND w.station = stat AND w.location = loc AND w.channel = chan AND cstart BETWEEN w.cstarttime AND w.cendtime ) 
			OR
			(w.network = net AND w.station = stat AND w.location = loc AND w.channel = chan AND cend BETWEEN w.cstarttime AND w.cendtime)
			ORDER BY channel_id DESC
			LIMIT 1);
	-- IF (idt IS NULL) THEN id:=0; ELSE id:=idt; END IF;
	RETURN idt;
	END; $$
LANGUAGE 'plpgsql';

/*
explain analyze update rall set channel_id = set_channel_id_in_file_table(network,station,location,channel,starttime,endtime) ;

explain analyze
update rbud set channel_id = set_channel_id_in_file_table(network,station,location,channel,starttime,endtime) ;

reindex table rall;
reindex table rbud;

*/

CREATE OR REPLACE FUNCTION set_station_id_in_file_table (TEXT, TEXT, TIMESTAMP WITHOUT TIME ZONE, TIMESTAMP WITHOUT TIME ZONE) RETURNS BIGINT AS $$
-- un fichier peut être associés à 0, .. channels.
-- Dans ce qui suit, on considère qu'un fichier n'est pas 'orphelins' si il est associé à au moins un channel
	DECLARE 
	net		ALIAS FOR $1;		-- code reseau FDSN de la station
	stat		ALIAS FOR $2;		-- code station
	sstart	ALIAS FOR $3;		-- date de mise en fonctionnement du channel
	ssend		ALIAS FOR $4;		-- date d'arret du channel
	id  		BIGINT ;					-- channel_id pour ce channel
	idt		BIGINT ;
	BEGIN
	idt:=0;
	idt  := (select DISTINCT w.station_id 
			FROM ws_common w
			WHERE 
			(w.network = net AND w.station = stat and sstart BETWEEN w.sstarttime AND w.sendtime ) 
			OR
			(w.network = net AND w.station = stat AND ssend BETWEEN w.sstarttime AND w.sendtime)
			ORDER BY station_id DESC
			LIMIT 1);
	-- IF (idt IS NULL) THEN id:=0; ELSE id:=idt; END IF;
	RETURN idt;
	END; $$
LANGUAGE 'plpgsql';

