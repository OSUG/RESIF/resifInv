CREATE OR REPLACE FUNCTION public.set_channel_id(text, text, text, text, timestamp without time zone, timestamp without time zone)  RETURNS bigint AS $$
	DECLARE 
	net		ALIAS FOR $1;		-- code reseau FDSN de la station
	sta	ALIAS FOR $2;		-- code station
	loc		ALIAS FOR $3;		-- location
	chan		ALIAS FOR $4;		-- channel
	cstart	ALIAS FOR $5;		-- une date
	cend		ALIAS FOR $6;		-- date d'arret du channel
	id  		BIGINT ;					-- station_id pour ce channel
	idt		BIGINT ;
	BEGIN
	idt  := (
			select DISTINCT c.channel_id 
			FROM channel c, station s, networks n
			WHERE 
			(c.channel = chan and
			c.location = loc and 
			s.station = sta and
			n.network = net and 
			n.network_id = s.network_id and
			s.station_id = c.station_id and 
			cstart BETWEEN c.starttime AND c.endtime)
			OR
			(c.channel = chan and
			c.location = loc and 
			s.station = sta and
			n.network = net and 
			n.network_id = s.network_id and
			s.station_id = c.station_id and 
			cend BETWEEN c.starttime AND c.endtime
			)
			ORDER BY c.channel_id DESC
			LIMIT 1);
	IF (idt IS NULL) THEN id:=0; ELSE id:=idt; END IF;
	RETURN id;
	END; 
$$  LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION public.set_station_id(text, text, timestamp without time zone, timestamp without time zone)  RETURNS bigint AS $$
	DECLARE 
	net		ALIAS FOR $1;		-- code reseau FDSN de la station
	sta	ALIAS FOR $2;		-- code station
	cstart	ALIAS FOR $3;		-- une date
	cend		ALIAS FOR $4;		-- date d'arret du channel
	id  		BIGINT ;					-- station_id pour ce channel
	idt		BIGINT ;
	BEGIN
	idt  := (
			select DISTINCT s.station_id 
			FROM station s, networks n
			WHERE 
			(
			s.station = sta and
			n.network = net and 
			n.network_id = s.network_id and
			cstart BETWEEN s.starttime AND s.endtime)
			OR
			(
			s.station = sta and
			n.network = net and 
			n.network_id = s.network_id and
			cend BETWEEN s.starttime AND s.endtime
			)
			ORDER BY s.station_id DESC
			LIMIT 1);
	IF (idt IS NULL) THEN id:=0; ELSE id:=idt; END IF;
	RETURN id;
	END; 
$$  LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION public.set_network_id(text, timestamp without time zone, timestamp without time zone)  RETURNS bigint AS $$
	DECLARE 
	net		ALIAS FOR $1;		-- code reseau FDSN de la station
	cstart	ALIAS FOR $2;		-- une date
	cend		ALIAS FOR $3;		-- date d'arret du channel
	id  		BIGINT ;					-- station_id pour ce channel
	idt		BIGINT ;
	BEGIN
	idt  := (
			select DISTINCT n.network_id 
			FROM networks n
			WHERE 
			(n.network = net and 
			cstart BETWEEN n.starttime AND n.endtime)
			OR
			(n.network = net and 
			cstart BETWEEN n.starttime AND n.endtime)
			ORDER BY n.network_id DESC
			LIMIT 1);
	IF (idt IS NULL) THEN id:=0; ELSE id:=idt; END IF;
	RETURN id;
	END; 
$$  LANGUAGE 'plpgsql';
---
update rbud set channel_id = set_channel_id (network, station, location, channel, starttime, endtime) where channel_id = 0;
update rbud set station_id = set_station_id (network, station, starttime, endtime) where station_id = 0;
update rbud set network_id = set_network_id (network, starttime, endtime) where network_id = 0;

update rbud set station_id = 0 where station_id is null;
update rbud set network_id = 0 where network_id is null;
update rbud set channel_id = 0 where network_id is null;

ALTER TABLE rbud ADD CONSTRAINT  "rbud_pkey1" PRIMARY KEY btree (rbud_id);

ALTER TABLE rbud ALTER COLUMN network_id SET DEFAULT 0;
ALTER TABLE rbud ALTER COLUMN station_id SET DEFAULT 0;
ALTER TABLE rbud ALTER COLUMN channel_id SET DEFAULT 0;


alter table rbud ADD CONSTRAINT rbud_network_id_key FOREIGN KEY (network_id) REFERENCES networks(network_id) ON DELETE SET DEFAULT;
alter table rbud ADD CONSTRAINT rbud_station_id_key FOREIGN KEY (station_id) REFERENCES station(station_id) ON DELETE SET DEFAULT;
alter table rbud ADD CONSTRAINT rbud_channel_id_key FOREIGN KEY (channel_id) REFERENCES channel(channel_id) ON DELETE SET DEFAULT;

---
update rall set channel_id = set_channel_id (network, station, location, channel, starttime, endtime);
update rall set station_id = set_station_id (network, station, starttime, endtime);
update rall set network_id = set_network_id (network, starttime, endtime);

update rall set station_id = 0 where station_id is null;
update rall set network_id = 0 where network_id is null;
update rall set channel_id = 0 where network_id is null;

ALTER TABLE rall ALTER COLUMN network_id SET DEFAULT 0;
ALTER TABLE rall ALTER COLUMN station_id SET DEFAULT 0;
ALTER TABLE rall ALTER COLUMN channel_id SET DEFAULT 0;

ALTER TABLE rall ADD CONSTRAINT  "rall_pkey1" PRIMARY KEY (rall_id);
alter table rall ADD CONSTRAINT rall_network_id_key FOREIGN KEY (network_id) REFERENCES networks(network_id) ON DELETE SET DEFAULT;
alter table rall ADD CONSTRAINT rall_station_id_key FOREIGN KEY (station_id) REFERENCES station(station_id) ON DELETE SET DEFAULT;
alter table rall ADD CONSTRAINT rall_channel_id_key FOREIGN KEY (channel_id) REFERENCES channel(channel_id) ON DELETE SET DEFAULT;


CREATE INDEX  "ix_rall_017" on rall (network, station, location, channel, starttime, endtime);
CREATE INDEX  "ix_rall_019" on rall (network, station, starttime, endtime);
CREATE INDEX  "ix_rall_020" on rall (network, station);
CREATE INDEX  "ix_rall_021" on rall (network, starttime, endtime);
CREATE INDEX  "ix_rall_022" on rall (network);

CREATE INDEX  "ix_rbud_017" on rbud (network, station, location, channel, starttime, endtime);
CREATE INDEX  "ix_rbud_019" on rbud (network, station, starttime, endtime);
CREATE INDEX  "ix_rbud_020" on rbud (network, station);
CREATE INDEX  "ix_rbud_021" on rbud (network, starttime, endtime);
CREATE INDEX  "ix_rbud_022" on rbud (network);



