
CREATE OR REPLACE FUNCTION public.set_station_id(text, text, timestamp without time zone, timestamp without time zone)  RETURNS bigint AS $$
	DECLARE 
	net		ALIAS FOR $1;		-- code reseau FDSN de la station
	sta	ALIAS FOR $2;		-- code station
	cstart	ALIAS FOR $3;		-- une date
	cend		ALIAS FOR $4;		-- date d'arret du channel
	id  		BIGINT ;					-- station_id pour ce channel
	idt		BIGINT ;
	BEGIN
	idt  := (
			select DISTINCT s.station_id 
			FROM station s, networks n
			WHERE 
			(
			s.station = sta and
			n.network = net and 
			n.network_id = s.network_id and
			cstart BETWEEN s.starttime AND s.endtime)
			OR
			(
			s.station = sta and
			n.network = net and 
			n.network_id = s.network_id and
			cend BETWEEN s.starttime AND s.endtime
			)
			ORDER BY s.station_id DESC
			LIMIT 1);
	IF (idt IS NULL) THEN id:=0; ELSE id:=idt; END IF;
	RETURN id;
	END; 
$$  LANGUAGE 'plpgsql';
update rbud set station_id = set_station_id (network, station, starttime, endtime);
update rbud set station_id = 0 where station_id is null;
update rall set station_id = set_station_id (network, station, starttime, endtime);
update rall set station_id = 0 where station_id is null;
