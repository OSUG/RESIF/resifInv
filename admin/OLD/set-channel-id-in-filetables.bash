#!/bin/bash
# [Pequegnat, juillet 2019] creation du script, permet de réassigner la valeur channel_id dans les table rall et rbud
set -u

# set -x
# check RUNMODE exists
trap "echo RUNMODE environnement variable must be set to 'test' or 'production'" EXIT
echo $RUNMODE > /dev/null
trap - EXIT
export station=null
export network=null
while getopts "s:n:" flag; do
    case $flag in
        s) export station=$OPTARG ;;
        # n) export network=$OPTARG ;;
    esac
done
shift $((OPTIND-1))

case $RUNMODE in
	"production") 
	export PGDATABASE="resifInv-Prod"
	export PGUSER="resifinvprod"
	export PGHOST="postgres-geodata.ujf-grenoble.fr"
	;;
	"test")
	export PGDATABASE="resifInv-Test"
	export PGUSER="resifinvdev"
	export PGHOST="resif10.u-ga.fr"
	;;
esac	

case $station in
	null) exit;;
	*)
echo -n  "Reassign channel_id in rall and rbud tables, for station $station in $PGHOST:$PGDATABASE ? (Y/N)"
read rep
[[ $rep = "Y" ]] && \
psql -d $PGDATABASE -h $PGHOST -U $PGUSER  -c "DROP TABLE if exists admin.tmpstat1 CASCADE; create table admin.tmpstat1 (station TEXT);" && \
psql -d $PGDATABASE -h $PGHOST -U $PGUSER  -c "insert into admin.tmpstat1(station) VALUES ('$station');" && \
psql -d $PGDATABASE -h $PGHOST -U $PGUSER  -c "select distinct * from rall where station='$station' and channel_id=0;" && \
psql -d $PGDATABASE -h $PGHOST -U $PGUSER -c \
"UPDATE rall SET channel_id = set_channel_id_in_file_table (network,rall.station,location,channel,starttime,endtime)  FROM admin.tmpstat1 WHERE rall.station = admin.tmpstat1.station;" && \
psql -d $PGDATABASE -h $PGHOST -U $PGUSER  -c "select distinct * from rall where station='$station' and channel_id=0;" && \
psql -d $PGDATABASE -h $PGHOST -U $PGUSER  -c "select distinct * from rbud where station='$station' and channel_id=0;" && \
psql -d $PGDATABASE -h $PGHOST -U $PGUSER -c \
"UPDATE rbud SET channel_id = set_channel_id_in_file_table (network,rbud.station,location,channel,starttime,endtime)  FROM admin.tmpstat1 WHERE rbud.station = admin.tmpstat1.station;" && \
psql -d $PGDATABASE -h $PGHOST -U $PGUSER  -c "select distinct * from rbud where station='$station' and channel_id=0;" # && \
# psql -d $PGDATABASE -h $PGHOST -U $PGUSER  -c "reindex table rall; reindex table rbud;" 

esac
 




