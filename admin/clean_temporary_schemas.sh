#!/usr/bin/env bash
#
# File managed by salt and versioned in salt.
#
# Ce script nettoie les tables temporaires des schémas utilisés par les webservices station et dataselect
# Ces schémas se polluent de tables qui ne sont pas bien nettoyées entrainant une obésité de la base resifInv
#
# Ce script s'attend à ce que les variables d'environnements postgres soient correctement positionnées :
# - PGHOST : le serveur postgres (resif-pgprod.u-ga.fr par défaut)
# - PGPORT : 5432 par défaut
# - PGDATABASE : resifInv-Prod par défaut
# - PGUSER : resifinvprod par défaut
# - PGPASSWORD : le mot de passe. Pas de valeur par défaut.
# - $1 : le nom du schéma à nettoyer. Par défaut, "dataselect".
#
# Description de l'algorithme
# 1. Lister toutes les tables temporaires
# 2. Attendre 10 minutes
# 3. Supprimer les tables temporaires listées si elles existent encore
#


# 0. Mise en place des environnements et vérification des binaires
#
PGHOST="${PGHOST:-resif-pgprod.u-ga.fr}"
PGPORT="${PGPORT:-5432}"
PGUSER="${PGUSER:-resifinvprod}"
PGDATABASE="${PGDATABASE:-resifInv-Prod}"
schema="${1:-dataselect}"

case $schema in
    dataselect|stationrequest)
        ;;
    *)
        echo "I will not clean schema $schema. Only dataselect or stationrequest authorized."
        exit 0
        ;;
esac

psqlcmd="$(which psql) -qtA -h $PGHOST -p $PGPORT -U $PGUSER $PGDATABASE"
if [[ $? -ne 0 ]]; then
    echo "ERROR: command psql not in \$PATH"
    exit 1
fi

echo "$(date '+%Y-%m-%d %H:%M:%S') Cleaning schema $schema"

# 1. Lister les tables temporaires
tables_to_clean=$($psqlcmd -c "SELECT table_name FROM information_schema.tables WHERE table_schema = '$schema';")

if [[ "x$tables_to_clean" = "x" ]]; then
    echo "Nothing to clean."
    exit 0
fi

echo "Found $(wc -w <<< $tables_to_clean) temporary tables"

# 2. Attendre 5 minutes
echo "Waiting 5 minutes before cleaning"
sleep 300

# 3. Nettoyer les tables
sed -e "s/^/drop table $schema./" -e 's/$/;/' <<< "$tables_to_clean"  | $psqlcmd

echo "$(date '+%Y-%m-%d %H:%M:%S') Cleaning done"
remaining_tables=$($psqlcmd -c "SELECT count(*) FROM information_schema.tables WHERE table_schema = '$schema'; ")
echo "There are now ${remaining_tables} in schema $schema"
