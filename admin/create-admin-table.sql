-- creation de la table d'importation pour les networks 
-- mise a jour de la table des réseaux (non importée du stationXML)

-- DROP SCHEMA IF EXISTS stationrequest CASCADE; CREATE SCHEMA stationrequest;
-- DROP SCHEMA IF EXISTS admin CASCADE; CREATE SCHEMA admin;
-- DROP SCHEMA IF EXISTS dataselect CASCADE; CREATE SCHEMA dataselect;
create schema admin;

DROP TABLE IF EXISTS admin.networks_in cascade;
CREATE TABLE admin.networks_in (
	network		TEXT NOT NULL,	
	start_year	INTEGER NOT NULL DEFAULT 0 CHECK (start_year <= end_year),
	end_year		INTEGER NOT NULL DEFAULT 0 CHECK (end_year >= start_year),
	operator		TEXT DEFAULT 'RESIF (Reseau sismologique et geodesique francais)',
	name			TEXT NOT NULL  DEFAULT '',
	description	TEXT NOT NULL DEFAULT '',
	policy		TEXT DEFAULT 'closed' CHECK (policy IN ('open', 'closed')),		
	url		TEXT NOT NULL default 'http://seismology.resif.fr/',
	tr TEXT default NULL,
	doi TEXT default NULL,
	email TEXT NOT NULL default 'datacentre@resif.fr')
	;

DROP TABLE IF EXISTS admin.user_in cascade;
CREATE TABLE admin.user_in (
	network		TEXT NOT NULL,	
	start_year	INTEGER NOT NULL DEFAULT 0 CHECK (start_year <= end_year),
	end_year		INTEGER NOT NULL DEFAULT 0 CHECK (end_year >= start_year),
	name			TEXT NOT NULL  default '')
	;

DROP TABLE IF EXISTS admin.archive_in cascade;
CREATE TABLE admin.archive_in (source_file TEXT default NULL);

drop table if exists admin.archive_rall cascade;
create table admin.archive_rall(
	network_id   bigint default null,                      
	station_id   bigint default null  ,                   
	channel_id   bigint  default null ,                   
	source_file text     ,                  
	network      text    ,                   
	station      text    ,                    
	location     character varying(2)     ,   
	channel      character varying(3)     ,   
	starttime    timestamp without time zone ,
	endtime      timestamp without time zone ,
	quality      character varying(1)    ,  
	block_size  integer               ,  
	year         integer             
);


/*
DROP TABLE IF EXISTS admin.assembleddata_in cascade;
CREATE TABLE admin.assembleddata_in  (
)
*/
