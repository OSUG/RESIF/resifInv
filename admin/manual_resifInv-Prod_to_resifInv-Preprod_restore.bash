#!/bin/bash
# manual_resifInv-Prod_to_resifInv-Preprod_restore.bash
# 
pg_dump -U resifinvprod -h postgres-geodata.ujf-grenoble.fr -d resifInv-Prod --schema PUBLIC > ~/resifinv.sql
cat ~/resifinv.sql | sed 's/resifinvprod/resifinvdev/g' | sed '/REFRESH/d' >  ~/resifinv-pre-prod.sql

# sur resif10  /postgres
ssh root@resif10.u-ga.fr
su - postgres
dropdb resifInv-Preprod
createdb -O resifinvdev resifInv-Preprod
psql resifInv-Preprod
CREATE EXTENSION pg_trgm;
exit
exit
exit

# 
export PGHOST=resif-pgpreprod.u-ga.fr
export PGDATABASE="resifInv-Preprod"
export PGUSER="resifinvdev"
psql -f  ~/resifinv-pre-prod.sql


cd ~/resifInv/SQL
psql -f ./resifinv-fonction.sql
psql -f ./resifinv-view.sql
psql -f ./resifinv-foreign.sql