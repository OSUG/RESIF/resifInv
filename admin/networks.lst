# Ce fichier contient la definition des reseaux dont les donnees sont distribuees par le noeud B RESIF
# Les informations sur les reseaux doivent être renseignees prealablement a l'integration d'un dataless a resifInv
# Chaque ligne est :
#  - soit un commentaire (commence par #)
#  - soit une ligne de 11 champs textuel separes par des '|'
# Attention,il y a tres epu de verification syntaxique du fichier avant son chargement. 
# A tester donc dans la base de test 'resifInv-Test' avant chargement dans la base resivInv-Prod
# export RUNMODE=test
# ./update-resifinv-net.bash 
# 
# C1 : code FDSN 2 caracteres
# C2 : annee de debut du reseau
# C3 : annee de fin du reseau (2500 si reseau permanent)
# C4 : operateur de reseau (Agency au niveau network)
# C5 : nom du reseau (nom alternatif pour ce reseau)
# C6 : description du reseau
# C7 : open ou restricted
# C8 : URL du reseau
# C9 : TR ou #
# C10 : doi ou #
# C11: email ou #
#
# RESIF Permament networks
RA|1995|2500|Réseau accélérométrique permanent (Rap)|RAP|RESIF-RAP Accelerometric permanent network|open|https://rap.epos-france.fr|TR|10.15778/RESIF.RA|RA@resif.fr
FR|1962|2500|Réseau large-bande permanent (RLBP)|RLBP|RESIF and other Broad-band and accelerometric permanent networks in metropolitan France|open|https://rlbp.resif.fr|TR|10.15778/RESIF.FR|FR@resif.fr
RD|1962|2500|Commissariat à l'énergie atomique et aux énergies alternatives (CEA)||CEA/DASE broad-band permanent network|open|http://www.cea.fr|TR|10.15778/RESIF.RD|RD@resif.fr
MT|2006|2500|Observatoire multidisciplinaire des instabilités de versants (Omiv)|OMIV|OMIV French Landslide Observatory|open|http://www.ano-omiv.cnrs.fr|TR|10.15778/RESIF.MT|MT@resif.fr
ND|2010|2500|Institut de Recherche pour le Développement (IRD)|SISMOCAL|New-Caledonia broadband and accelerometric permanent networks|open|https://www.ird.fr|TR|10.15778/RESIF.ND|ND@resif.fr
CL|2000|2500|Corinth Rift Laboratory, CNRS/INSU|CRLNET|Corinth Rift Laboratory Network|open|http://crlab.eu/|TR|10.15778/RESIF.CL|CL@resif.fr
FO|1900|2500|French Associated Seismological Network|FRENCH_OTHER_PERMANENT|Seismic stations in France not belonging to the academic RESIF networks|open|#|TR|#|FO@resif.fr
# IPGP networks
G|1982|2500|Observatoire Geoscope|GEOSCOPE|GEOSCOPE - French Global Network of Seismological Broadband Stations|open|http://geoscope.ipgp.fr|TR|10.18715/GEOSCOPE.G|geoscope@ipgp.fr
PF|1980|2500|Observatoire volcanologique du Piton de la Fournaise (OVPF)|OVPF|Piton de la Fournaise Volcano Observatory Network (Reunion Island)|open|https://www.ipgp.fr/ovpf|TR|10.18715/REUNION.PF|#
WI|2008|2500|Observatoire volcanologique et sismologique de Guadeloupe (OVSG)|West-Indies|West Indies French Seismic Network|open|https://www.ipgp.fr/ovsg/|TR|10.18715/antilles.WI|datacenter@ipgp.fr
MQ|1935|2500|Observatoire volcanologique et sismologique de Martinique (OVSM)|OVSM|Martinique Seismic and Volcano Observatory Network|open|https://www.ipgp.fr/ovsm|TR|10.18715/martinique.MQ|#
GL|1950|2500|Observatoire volcanologique et sismologique de Guadeloupe (OVSG)|OVSG|Guadeloupe Seismic and Volcano Observatory Network|open|https://www.ipgp.fr/ovsg|TR|10.18715/guadeloupe.GL|#
QM|2019|2500|Réseau de surveillance volcanologique et sismologique de Mayotte (Revosima)|Al-Qamar|Comoros Archipelago Seismic and Volcano Network|open|https://www.ipgp.fr/revosima|TR|10.18715/mayotte.QM|#
#
# Temporary networks
YO|1998|2000|Parc national sismologique mobile (SisMob)|TRACK|France 1998,TRACK South Massif Central temporary experiment|open|https://sismob.epos-france.fr|#|10.15778/RESIF.YO1998|sismob@resif.fr
YR|1999|2002|Parc national sismologique mobile (SisMob)|HORN-OF-AFRICA|Ethiopia Yemen 1999,HORN_OF_AFRICA BB temporary experiment|open|https://sismob.epos-france.fr|#|10.15778/RESIF.YR1999|sismob@resif.fr
YB|2000|2001|Parc national sismologique mobile (SisMob)|ZAGROS|Iran 2000,ZAGROS Central Zagros lithospheric temporary transect|open|https://sismob.epos-france.fr|#|10.15778/RESIF.YB2000|sismob@resif.fr
ZI|2001|2005|Parc national sismologique mobile (SisMob)|PLUME|French Polynesia 2001,PLUME Polynesian Lithosphere and Upper Mantle temporary experiment|open|https://sismob.epos-france.fr|#|10.15778/RESIF.ZI2001|sismob@resif.fr
YT|2001|2001|Parc national sismologique mobile (SisMob)|KUNLUN|China Tibet 2001,KUNLUN West Kunlun temporary transect|open|https://sismob.epos-france.fr|#|10.15778/RESIF.YT2001|sismob@resif.fr
YX|2001|2002|Parc national sismologique mobile (SisMob)|CORINTH|Greece 2001,CORINTH temporary experiment|open|https://sismob.epos-france.fr|#|10.15778/RESIF.YX2001|sismob@resif.fr
YT|2003|2003|Parc national sismologique mobile (SisMob)|MOBAL|Mongolia Russia 2003,MOBAL Mongolia-Baikal temporary transect|open|https://sismob.epos-france.fr|#|10.15778/RESIF.YT2003|sismob@resif.fr
# ZJ|2003|2003|Parc national sismologique mobile (SisMob)|ALBORZ|Iran 2003,ALBORZ lithospheric temporary transect|closed|https://sismob.epos-france.fr|#|#|# => Exclu de l inventaire car pas d info, pas de meta et pas de données
ZF|2003|2004|Parc national sismologique mobile (SisMob)|BAM|Iran 2003,BAM French part of the BAM aftershok experiment|open|https://sismob.epos-france.fr|#|10.15778/RESIF.ZF2003|sismob@resif.fr
ZH|2003|2003|Parc national sismologique mobile (SisMob)|ZAGROS|Iran 2003,ZAGROS North Zagros lithospheric temporary transect|open|https://sismob.epos-france.fr|#|10.15778/RESIF.ZH2003|sismob@resif.fr
YZ|2004|2004|Parc national sismologique mobile (SisMob)|TABRIZ|Iran 2004,TABRIZ seismotectonic temporary experiment|open|https://sismob.epos-france.fr|#|10.15778/RESIF.YZ2004|sismob@resif.fr
Y4|2004|2007|Parc national sismologique mobile (SisMob)|ALPES-BB|France 2004,ALPES-BB temporary experiment|open|https://sismob.epos-france.fr|#|10.15778/RESIF.Y42004|sismob@resif.fr
XK|2007|2009|Parc national sismologique mobile (SisMob)|LAPNET|Finland 2007,LAPNET seismic temporary array|open|https://sismob.epos-france.fr|#|10.15778/RESIF.XK2007|sismob@resif.fr
XY|2007|2009|Parc national sismologique mobile (SisMob)|simbaadBB|Greece Turkey 2007,SIMBAAD Aegean-Anatolia temporary experiment - backbone|open|https://sismob.epos-france.fr|#|10.15778/RESIF.XY2007|sismob@resif.fr
XW|2007|2008|Parc national sismologique mobile (SisMob)|simbaadLW|Turkey 2007,SIMBAAD Aegean-Anatolia temporary experiment - western transect|open|https://sismob.epos-france.fr|#|10.15778/RESIF.XW2007|sismob@resif.fr
YI|2008|2009|Parc national sismologique mobile (SisMob)|simbaadLE|Turkey 2007,SIMBAAD Aegean-Anatolia temporary experiment - eastern transect|open|https://sismob.epos-france.fr|#|10.15778/RESIF.YI2008|sismob@resif.fr
# ZT|2008|2010|Parc national sismologique mobile (SisMob)|UTIKU|UTIKU landslides temporary experiment|closed|https://sismob.epos-france.fr|#|#|sismob@resif.fr
ZO|2008|2009|Parc national sismologique mobile (SisMob)|ARC-VANUATU|Vanuatu 2008,ARC-VANUATU (volcanoes) temporary experiment|open|https://sismob.epos-france.fr|#|10.15778/RESIF.ZO2008|sismob@resif.fr
ZS|2007|2008|Parc national sismologique mobile (SisMob)|TANZANIA|Tanzania 2007,TANZANIA BB temporary experiment|open|https://sismob.epos-france.fr|#|10.15778/RESIF.ZS2007|sismob@resif.fr
XJ|2009|2009|Parc national sismologique mobile (SisMob)|L_AQUILA|Italy 2009,L_AQUILA French part of the L'Aquila aftershock experiment|open|https://sismob.epos-france.fr|#|10.15778/RESIF.XJ2009|sismob@resif.fr
YA|2009|2011|Parc national sismologique mobile (SisMob)|UNDERVOLC|La Réunion Island 2009,UNDERVOLC temporary experiment|open|http://www.undervolc.fr/|#|10.15778/RESIF.YA2009|sismob@resif.fr
XS|2010|2011|Parc national sismologique mobile (SisMob)|CHILE_MAULE|Chili 2010,CHILE_MAULE French part of the aftershock experiment|open|https://sismob.epos-france.fr|#|10.15778/RESIF.XS2010|sismob@resif.fr
YB|2010|2010|Parc national sismologique mobile (SisMob)|HEART|Haiti 2010,HEART aftershock experiment|open|https://sismob.epos-france.fr|#|10.15778/RESIF.YB2010|sismob@resif.fr
Y9|2009|2012|Parc national sismologique mobile (SisMob)|GGAP|Mexico 2009,GGAP Guerrero Gap temporary array|open|https://sismob.epos-france.fr|#|10.15778/RESIF.Y92009|sismob@resif.fr
1A|2009|2012|Parc national sismologique mobile (SisMob)|ARLITA|Antartica 2009, ARLITA Eastern Antartica temporary experiment|open|http://dossier.univ-st-etienne.fr/arlita/www/centre.html|#|10.15778/RESIF.1A2009|sismob@resif.fr
7C|2009|2012|Parc national sismologique mobile (SisMob)|DORA|Yemen Ethiopia Eritrea 2009, DORA Afar temporary experiment|open|https://sismob.epos-france.fr|#|10.15778/RESIF.7C2009|sismob@resif.fr
X7|2010|2014|Parc national sismologique mobile (SisMob)|PYROPE|France Spain 2010, PYROPE PYRenean Observational Portable Experiment|open|http://w3.dtp.obs-mip.fr/RSSP/PYROPE/|#|10.15778/RESIF.X72010|sismob@resif.fr
YV|2011|2016|Parc national sismologique mobile (SisMob)|RHUMRUM|Terres australes et antarctiques françaises 2011,RHUMRUM temporary experiment|open|https://sismob.epos-france.fr|#|10.15778/RESIF.YV2011|sismob@resif.fr
4C|2011|2014|Parc national sismologique mobile (SisMob)|NERA_JRA1|Greece 2011, NERA_JRA1 temporary experiment|open|https://sismob.epos-france.fr|#|10.15778/RESIF.4C2011|sismob@resif.fr
3A|2008|2009|Parc national sismologique mobile (SisMob)|VANUATU|Vanuatu 2008, ARC-VANUATU (subduction) temporary experiment|open|https://sismob.epos-france.fr|#|10.15778/RESIF.3A2008|sismob@resif.fr
YP|2012|2013|Parc national sismologique mobile (SisMob)|CIFAlps|France Italy 2012, CIFAlps temporary experiment|open|https://sismob.epos-france.fr|#|10.15778/RESIF.YP2012|sismob@resif.fr
YR|2013|2015|Parc national sismologique mobile (SisMob)|DOMERAPI|Indonesia 2013, DOMERAPI temporary experiment|open|https://sismob.epos-france.fr|#|10.15778/RESIF.YR2013|sismob@resif.fr
YP|2014|2015|Parc national sismologique mobile (SisMob)|ARGOSTOLI|Greece 2014, ARGOSTOLI French part of the Argostoli aftershock experiment|open|https://sismob.epos-france.fr|#|10.15778/RESIF.YP2014|sismob@resif.fr
ZO|2014|2016|Parc national sismologique mobile (SisMob)|HiK-NET|Nepal 2014, HiK-NET Nepal temporary experiment|open|https://sismob.epos-france.fr|#|10.15778/RESIF.ZO2014|sismob@resif.fr
XG|2014|2014|Parc national sismologique mobile (SisMob)|BARCELONETTE|France 2014, BARCELONETTE aftershock experiment|open|https://sismob.epos-france.fr|#|10.15778/RESIF.XG2014|sismob@resif.fr
7H|2010|2025|Parc national sismologique mobile (SisMob)|MOUVEMENTS_GRAVITAIRES|France Switzerland 2014, MOUVEMENTS_GRAVITAIRES|open|https://sismob.epos-france.fr|#|10.15778/RESIF.7H2010|sismob@resif.fr
XP|2014|2014|Parc national sismologique mobile (SisMob)|VOLCARRAY|La Reunion Island 2014, VOLCARRAY Piton de la Fournaise temporary experiment |open|https://sismob.epos-france.fr|#|10.15778/RESIF.XP2014|sismob@resif.fr
Y2|2014|2014|Parc national sismologique mobile (SisMob)|SAFE_CO2|Norway 2015, SAFE_CO2 Norsard Svalbard temporary experiment|open|https://sismob.epos-france.fr|#|10.15778/RESIF.Y22014|sismob@resif.fr
ZU|2015|2017|Parc national sismologique mobile (SisMob)|OROGEN_X|France Spain 2015, OROGEN_X temporary experiment|open|https://sismob.epos-france.fr|#|10.15778/RESIF.ZU2015|sismob@resif.fr
YI|2015|2016|Parc national sismologique mobile (SisMob)|SAINT_GUERIN|France 2015, SAINT_GUERIN dam temporary experiment|open|https://sismob.epos-france.fr|#|10.15778/RESIF.YI2015|sismob@resif.fr
YJ|2015|2017|Parc national sismologique mobile (SisMob)|SLIDEQUAKES|Italy 2015, SLIDEQUAKES temporary experiment|closed|https://sismob.epos-france.fr|#|10.15778/RESIF.YJ2015|sismob@resif.fr
Z3|2015|2022|Parc national sismologique mobile (SisMob)|ALPARRAY|AlpArray Seismic Network (AASN) temporary component|open|https://sismob.epos-france.fr|#|10.12686/alparray/z3_2015|sismob@resif.fr
ZF|2015|2022|Parc national sismologique mobile (SisMob)|RIVIERE_DES_PLUIES|La Reunion Island 2015, RIVIERE_DES_PLUIES temporary experiment|open|https://sismob.epos-france.fr|#|10.15778/RESIF.ZF2015|sismob@resif.fr
ZH|2016|2019|Observatoire de la Côte d'Azur (OCA)|POSA|France 2016, POSA temporary experiment|open|https://www.oca.eu|#|10.15778/RESIF.ZH2016|sismob@resif.fr
8F|2016|2016|Parc national sismologique mobile (SisMob)|MARGATS|French Guiana 2016, MARGATS temporary experiment|open|https://sismob.epos-france.fr|#|10.15778/RESIF.8F2016|sismob@resif.fr
5F|2010|2011|Parc national sismologique mobile (SisMob)|CASE_IPY|France 2015, CASE_IPY temporary experiment|open|https://sismob.epos-france.fr|#|#|sismob@resif.fr
Z8|2016|2017|Parc national sismologique mobile (SisMob)|CHAMP_CAPTANT|France 2016, CHAMP_CAPTANT Lyon temporary experiment|closed|https://sismob.epos-france.fr|#|10.15778/RESIF.Z82016|sismob@resif.fr
YW|2017|2020|Parc national sismologique mobile (SisMob)|ESSAIM_MAURIENNE|France 2017, ESSAIM_MAURIENNE temporary experiment|open|https://sismob.epos-france.fr|#|10.15778/RESIF.YW2017|sismob@resif.f
ZK|2017|2018|Parc national sismologique mobile (SisMob-Nodes)|DENSE_ARRAY_LA_SOUFRIERE_GUADELOUPE|France Guadeloupe 2016, DENSE_ARRAY_LA_SOUFRIERE_GUADELOUPE temporary seismic array|closed|https://sismob.epos-france.fr|#|10.15778/RESIF.ZK2017|sismob@resif.fr
XT|2018|2020|Parc national sismologique mobile (SisMob)|CIFAlps2|France Italy 2018, CIFAlps2 temporary transect across the northwestern (European Alps)|open|https://sismob.epos-france.fr|#|10.15778/RESIF.XT2018|sismob@resif.fr
XF|2018|2019|Parc national sismologique mobile (SisMob)|HATARI|Tanzania 2018, HATARI Hazard in Tanzanian Rift temporary experiment|open|https://sismob.epos-france.fr|#|10.15778/RESIF.XF2018|sismob@resif.fr
ZO|2018|2018|Parc national sismologique mobile (SisMob-Nodes)|ARGENTIERE_ALPINE_GLACIER|France 2018, ARGENTIERE_ALPINE_GLACIER dense nodal temporary seismic array |open|https://sismob.epos-france.fr|#|10.15778/RESIF.ZO2018|sismob@resif.fr
6J|2018|2018|Parc national sismologique mobile (SisMob-Nodes)|NODES_ARGOSTOLI|Greece 2018, NODES_ARGOSTOLI spatial variation of ground structure|closed|https://sismob.epos-france.fr|#|#|sismob@resif.fr
ZE|2018|2018|Parc national sismologique mobile (SisMob-Nodes)|META_QUITO|Ecuador 2018, META_QUITO passive temporary dense network in Quito City|open|https://sismob.epos-france.fr|#|10.15778/RESIF.ZE2018|sismob@resif.fr
Z7|2018|2018|Parc national sismologique mobile (SisMob-Nodes)|RITTERSHOFFEN|France 2018, RITTERSHOFFEN dense nodal seismic array temporary experiment,Bas-Rhin|closed|https://sismob.epos-france.fr|#|10.15778/RESIF.Z72018|sismob@resif.fr
XW|2019|2022|Parc national sismologique mobile (SisMob)|ALBION2|France 2019, ALBION2 Noise monitoring of the groundwater in Albion area|closed|https://sismob.epos-france.fr|#|10.15778/RESIF.XW2019|sismob@resif.fr
1N|2015|2027|Parc national sismologique mobile (SisMob)|MT_CAMPAGNE|France 2015, MT_CAMPAGNE French Landslide Observatory temporary experiments|open|https://sismob.epos-france.fr|#|10.15778/RESIF.1N2015|sismob@resif.fr
1E|2008|2008|Parc national sismologique mobile (SisMob)|MOVRI|Greece 2008, MOVRI post seismic experiment after 8th June 2008 event Andravida|open|https://sismob.epos-france.fr|#|10.15778/RESIF.1E2008|sismob@resif.fr
2L|2018|2019|Parc national sismologique mobile (SisMob-Nodes)|MEMBACH|Belgium 2018, MEMBACH Dense nodal network|closed|https://sismob.epos-france.fr|#|#|sismob@resif.fr
1T|2018|2025|Parc national sismologique mobile (SisMob)|SISMAYOTTE|Mayotte 2018, SISMAYOTTE seismic monitoring of seismic sequence on land and offshore|closed|https://sismob.epos-france.fr|#|10.15778/RESIF.1T2018|sismob@resif.fr
1F|2018|2018|Parc national sismologique mobile (SisMob-Nodes)|RESOLVE Champ Captant|France 2018, RESOLVE temporary passive seismic noise monitoring of the water tables near Lyon|open|https://sismob.epos-france.fr|#|10.15778/RESIF.1F2018|sismob@resif.fr
XG|2019|2019|Parc national sismologique mobile (SisMob-Nodes)|ICEWAVEGUIDE|Norway 2019, ICEWAVEGUIDE dense nodal seismic array temporary experiment on the frozen Laguna of Vallunden|closed|https://sismob.epos-france.fr|#|10.15778/RESIF.XG2019|sismob@resif.fr
3C|2019|2021|Parc national sismologique mobile (SisMob)|LE_TEIL|France 2019, M5 LE_TEIL 20191111 post seismic (LeTeilPostSeismic) in the Rhone Valley|open|https://sismob.epos-france.fr|#|10.15778/RESIF.3C2019|sismob@resif.fr
4H|2018|2021|Réseau national de surveillance sismique (BCSF-Rénass)|MULHOUSE_AM|France 2018, MULHOUSE_AM Seismo Citizen temporary experiment|open|https://renass.unistra.fr|#|10.15778/RESIF.4H2018|sismo-citizen@resif.fr
8C|2019|2023|Parc national sismologique mobile (SisMob)|COURMAYEUR|France 2019, COURMAYEUR Monitoring swarms in the Mont-Blanc and Vallorcine area|open|https://sismob.epos-france.fr|#|10.15778/RESIF.8C2019|sismob@resif.fr
ZL|2019|2019|Parc national sismologique mobile (SisMob-Nodes)|SEISMORIV_SEVRAISSE|France 2019, SEISMORIV_SEVRAISSE dense nodal seismic array temporary experiment on the banks of the Sévraisse River (Alps)|closed|https://sismob.epos-france.fr|#|10.15778/RESIF.ZL2019|sismob@resif.fr
1D|2019|2020|Parc national sismologique mobile (SisMob-Nodes)|ARGG|France 2019, ARGG temporary experiment of nodes in Mont Blanc massif|open|https://sismob.epos-france.fr|#|10.15778/RESIF.1D2019|sismob@resif.fr
9C|2019|2026|Parc national sismologique mobile (SisMob)|COPIACO|Chile 2019, COPIACO temporary deployment around Copiapo|closed|https://sismob.epos-france.fr|#|10.15778/RESIF.9C2019|sismob@resif.fr
YP|2017|2018|Parc national sismologique mobile (SisMob)|MARGIN_AFAR|Ethiopian 2017, MARGIN_AFAR continental plateaus temporary experiment|open|https://sismob.epos-france.fr|#|10.15778/RESIF.YP2017|sismob@resif.fr
ZV|2018|2019|Parc national sismologique mobile (SisMob)|MAUPITI|French Polynesia 2018, MAUPITI seismic noise temporary experiment|open|https://sismob.epos-france.fr|#|10.15778/RESIF.ZV2018|sismob@resif.fr
XQ|2020|2025|Parc national sismologique mobile (SisMob)|CHAUVET|France 2020, CHAUVET temporary experiment for study on seismic hazards applied to the Chauvet Cave|closed|https://sismob.epos-france.fr|#|10.15778/RESIF.XQ2020|sismob@resif.fr
XZ|2020|2024|Parc national sismologique mobile (SisMob)|DEEP_TRIGGER|Chile 2020, DEEP_TRIGGER temporary experiment in the subduction zone Peru/Chile (Part Chile)|closed|https://sismob.epos-france.fr|#|10.15778/RESIF.XZ2020|sismob@resif.fr
XH|2018|2019|Parc national sismologique mobile (SisMob)|PRESTinCuba|Cuba, PRESTinCuba temporary network installed in the southeast of Cuba|open|https://sismob.epos-france.fr|#|10.15778/RESIF.XH2018|sismob@resif.fr
ZP|2020|2020|Parc national sismologique mobile (SisMob)|MARIE|France 2020, MARIE dense nodal seismic array temporary experiment for monitoring of a landslide in Alpes-Maritimes|closed|https://sismob.epos-france.fr|#|10.15778/RESIF.ZP2020|sismob@resif.fr
X1|2021|2024|Parc national sismologique mobile (SisMob)|Tour Perret|France 2021, Monitoring the Tour Perret in Grenoble|open|https://sismob.epos-france.fr|#|10.15778/RESIF.X12021|sismob@resif.fr
XO|2021|2022|Parc national sismologique mobile (SisMob)|Vuache|France 2021, Monitoring of seismic activity and noise around the Vuache Fault|closed|https://sismob.epos-france.fr|#|10.15778/RESIF.XO2021|sismob@resif.fr
ZN|2021|2021|Parc national sismologique mobile (SisMob-Nodes)|RESOLVE-Harmaliere|France 2021, High-resolution seismic imaging and monitoring of the Harmaliere landslide|open|https://sismob.epos-france.fr|#|10.15778/RESIF.ZN2021|sismob@resif.fr
1B|2020|2024|Parc national sismologique mobile (SisMob-Nodes)|STROM|Italy 2021, Nodes and broadband data associated with DAS experiment at Stromboli volcano|closed|https://sismob.epos-france.fr|#|10.15778/RESIF.1B2020|sismob@resif.fr
8N|2021|2025|Parc national sismologique mobile (SisMob)|SHM Hautbois|France 2021, Temporary instrumentation of the Hautbois Wooden High-Rise building in Grenoble (France)|open|https://sismob.epos-france.fr|#|10.15778/RESIF.8N2021|sismob@resif.fr
6F|2021|2022|Parc national sismologique mobile (SisMob-Nodes)|SismEauClim|France 2021, Temporary network installed to better understand karst aquifers and assess their response to flash floods|closed|https://sismob.epos-france.fr|#|10.15778/RESIF.6F2021|sismob@resif.fr
6B|2021|2024|Parc national sismologique mobile (SisMob)|DP-Peru|Peru 2021, DEEP_TRIGGER temporary experiment in the subduction zone Peru/Chile (Part Peru)|closed|https://sismob.epos-france.fr|#|10.15778/RESIF.6B2021|sismob@resif.fr
ZR|2020|2025|Parc national sismologique mobile (SisMob)|SEIS-ADELICE|Antarctica 2020, SEIS-ADELICE temporary experiment measuring the cryo-seismicity of the Astrolabe glacier (Terre Adelie, Antarctica)|closed|https://sismob.epos-france.fr|#|10.15778/RESIF.ZR2020|sismob@resif.fr
YZ|2022|2022|Parc national sismologique mobile (SisMob)|Tete Rousse|France 2022, Temporary experiment to study the ambient noise of the Tete Rousse glacier and the evolution of the filling/pressure of the cavity (France)|closed|https://sismob.epos-france.fr|#|10.15778/RESIF.YZ2022|sismob@resif.fr
4G|2007|2025|Parc sismometre fond de mer (INSU/IPGP)|EMSO-MOMAR|Portugal 2007, Local seismological network around the summit of Lucky Strike volcano (Azores)|open|https://parc-obs.insu.cnrs.fr|#|10.15778/RESIF.4G2007|sismo-help@resif.fr
Z4|2022|2022|Parc national sismologique mobile (SisMob-Nodes)|KRAFLA|Iceland 2022, Temporary nodes experiment deployed for imaging and monitoring the Krafla volcano in Iceland|closed|https://sismob.epos-france.fr|#|10.15778/RESIF.Z42022|sismob@resif.fr
9M|2022|2022|Parc national sismologique mobile (SisMob-Nodes)|Sierentz Post-Seismic|France 2022, Post-seismic deployment following the MLv4.8 event in Sierentz (France)|open|https://sismob.epos-france.fr|#|10.15778/RESIF.9M2022|sismob@resif.fr
XP|2023|2027|Parc national sismologique mobile (SisMob)|MACIV-BB|France 2023, Temporary experiment to carry out a seismic tomography of the crustal and mantle structures of the Massif Central. French component of the European initiative AdriaArray (France)|open|https://sismob.epos-france.fr|#|10.15778/RESIF.XP2023|sismob@resif.fr
Z4|2023|2023|Parc national sismologique mobile (SisMob)|La-Laigne Post-Seismic|France 2023, Post-seismic deployment following the La Laigne, Ml4.9 earthquake that occurred on Friday June 16 in western France (France)|open|https://sismob.epos-france.fr|#|10.15778/RESIF.Z42023|sismob@resif.fr
YR|2022|2025|Parc national sismologique mobile (SisMob)|LISISKER|The Kerguelen Islands 2022, Temporary experiment to analyze the detailed seismicity of the main Kerguelen archipelago and produce a tomography based on earthquakes and seismic noise (Indian Ocean)|closed|https://sismob.epos-france.fr|#|10.15778/RESIF.YR2022|sismob@resif.fr
9L|2022|2026|Parc national sismologique mobile (SisMob)|activeSW|Taiwan 2022, Temporary experiment on mud volcanoes in SW Taiwan to detect and characterize degassing events (Taiwan)|open|https://sismob.epos-france.fr|#|10.15778/RESIF.9L2022|sismob@resif.fr
9F|2023|2023|Parc national sismologique mobile (SisMob-Nodes)|GreenlandSeismic|Greenland 2023, Temporary nodes experiment deployed on the Insunnguata Sermia glacier in Greenland|closed|https://sismob.epos-france.fr|#|10.15778/RESIF.9F2023|sismob@resif.fr
XG|2020|2020|Parc national sismologique mobile (SisMob-Nodes)|DARE|France 2020, DARE project : Temporary Dense nodal seismic array in the Rhône Valley|open|https://sismob.epos-france.fr|#|10.15778/RESIF.XG2020|sismob@resif.fr
3T|2019|2019|Parc national sismologique mobile (SisMob-Nodes)|Pre-DARE|France 2019, Preliminary campaign at DARE project: Temporary Dense nodal seismic  array in the Rhône Valley|open|https://sismob.epos-france.fr|#|10.15778/RESIF.3T2019|sismob@resif.fr
XF|2024|2027|Parc national sismologique mobile (SisMob)|MACIV-Profiles|France 2024, Temporary experiment of seismic imaging of the lithospheric structure of the French Massif Central along 3 profiles to complete the MACIV-BB broadband network to be deployed in 2023 (France)|closed|https://sismob.epos-france.fr|#|10.15778/RESIF.XF2024|sismob@resif.fr
XR|2024|2026|Parc national sismologique mobile (SisMob)|K3-Lez|France 2024, Temporary experiment continuous seismic acquisition for hydrological monitoring purposes near the city of Montpellier (France)|closed|https://sismob.epos-france.fr|#|10.15778/RESIF.XR2024|sismob@resif.fr
XS|2024|2024|Parc national sismologique mobile (SisMob-Nodes)|MOFETTE_LAB|France 2024, Temporary nodes experiment deployed around the Escarot mofette volcanic site for CO2 emission monitoring (central France)|closed|https://sismob.epos-france.fr|#|10.15778/RESIF.XS2024|sismob@resif.fr
XT|2024|2024|Parc national sismologique mobile (SisMob-Nodes)|Vuache-2024|France 2024, Temporary nodes experiment deployed of a temporary node experiment to tomograph the Vuache faults (France)|closed|https://sismob.epos-france.fr|#|10.15778/RESIF.XT2024|sismob@resif.fr
3I|2020|2030|Parc national sismologique mobile (SisMob-Nodes)|OMIV_Nodes_Campagne|France 2020, Temporary nodes experiments deployed by French Landslide Observatory – OMIV (France)|open|https://sismob.epos-france.fr|#|#|sismob@resif.fr
