#!/bin/bash

#
# run PostgreSQL database dumps
#
# this script must be run every day via cron
#
# 04 Oct  16 - GA Ajout option --exclude-schema=
# 28 Sept 16 - GA Ajout de logs + Option --verbose
# 03 Jan 2019, exclusion des bases de Dev dans les dumps par base
# 14 Jan 2019 JS: correction de l'exclusion des Dev, ajout de messages pour zabbix
# 22 Juin 2022 JS: changement de politique de sauvegarde
# Oct 2022 JS: Simplification du script. Ajout d'un lock-wait-timeout pour éviter de bloquer la base trop longtemps

source /etc/profile
DUMPDIR=/mnt/nfs/summer/pgdumps/dumps
WORKDIR=$DUMPDIR/`date +"%Y-%j-%H%M%S"`
DAILY_DATABASES="eidastats postgres resifAuth resifInv-Prod resifintegration seedtree5"
[[ -d $DUMPDIR ]] || { echo $DUMPDIR does not exist ; exit ; }

function report_to_zabbix(){
	zabbix_sender -k pg_dump_log -o "$@" -z monitoring.osug.fr -s resif-pgprod.u-ga.fr  2>&1 > /dev/null
}

function pre_dump(){
	echo "=== Suspend wal_receive ==="
	psql -qtA -c "select pg_wal_replay_pause();"
	if [[ $? -eq 1 ]] ; then
		echo "Postgres not in recovery state. We should not dump the database."
		report_to_zabbix "Error: $(hostname) is not in recovery state"
		exit 1
	fi
}

function post_dump(){
	local dump_rc=$1
	echo "=== Resume wal_receive ==="
	psql -qtA -c "select pg_wal_replay_resume();"

	if [ $dump_rc -eq 0 ] ; then
		report_to_zabbix "pg_dump $db OK"
	else
		echo "    Backup failed (exit dump_rc : $dump_rc)"
		report_to_zabbix "pg_dump error on $db"
	fi
	# Let postgres some time to catchup on the wals
	# sleep 15
}

DUMP_OPTS="--no-password --compress=5 --format=directory --lock-wait-timeout=60000 --jobs=18"
mkdir -p $WORKDIR
pushd $WORKDIR || exit

#
# dump all roles
#
pre_dump
echo "=== Dumping all roles ==="
pg_dumpall --roles-only --no-password --clean --file=pg_dumpall-roles.sql
post_dump $?

DAYOFWEEK=$(date +%u)
# dump all databases on day #1
if [ $DAYOFWEEK -eq 1 ];  then
	DAILY_DATABASES=$(psql --tuples-only --no-align  -c "select datname from pg_catalog.pg_database where datname not like ('%dev') and datname not like '%Dev' and datname not like 'template%' order by datname")
fi
if [ $# -eq 1 ]; then
    DAILY_DATABASES=$1
fi
for db in $(echo "$DAILY_DATABASES"); do
	# resifInv-Prod is always special ...
	if [[ $db = "resifInv-Prod" ]]; then
		DUMP_OPTS="$DUMP_OPTS --exclude-schema='(admin|dataselect|stationrequest)'"
	fi
	echo "[$(date '+%Y-%m-%d %H:%M:%S')] Dumping $db"
	pre_dump
	pg_dump $DUMP_OPTS --exclude-schema='(admin|dataselect|stationrequest)' --file=$db $db
	post_dump $?
	if [[ $db = "resifInv-Prod" ]]; then
		echo "   copy resifInv-Prod dump to the metadata summer store"
		RSYNC_PASSWORD=n2tKE9VWdFH8Pks4 rsync --delete --recursive resifInv-Prod resifdump@rsync.resif.fr::METADATADUMP
	fi
done

popd || exit

echo "=== Cleaning oldest dumps ==="
find $DUMPDIR -maxdepth 1 -type d -ctime +15 | xargs rm -rf

echo "[$(date '+%Y-%m-%d %H:%M:%S')] Done"
