#!/usr/bin/env bash
#
# À partir d'une liste de fichiers dans l'archive, ce script enregistre les données dans la table rall
# si nécessaire, il les supprime dans rbud
#
# En stdin, une liste de noms de fichiers (sns les chemins)
# Sinon, on peut aussi fournir un fichier en paramètre, ça marche aussi
#
# Exemple : files.lst :
# RA.UBBR.00.HNN.D.2020.284
# RA.UBBR.00.HNZ.D.2020.284
# RA.UBQP.00.HNE.D.2020.284
# RA.UBQP.00.HNN.D.2020.284
# RA.UBQP.00.HNZ.D.2020.284
#
# cat files.lst | register_in_rall.sh
# ou bien
# register_in_rall.sh files.lst
#
# Utiliser la variable RUNMODE pour se connecter à différentes bases
#
case $RUNMODE in
    "production")
        PGHOST=resif11.u-ga.fr
        PGUSER=resifinv_ro
        PGDATABASE=resifInv-Prod
        ;;
    "test")
        PGHOST=resif-pgpreprod.u-ga.fr
        PGUSER=resifinvdev
        PGDATABASE=resifInv-Preprod
        ;;
    "*")
        PGHOST=resif-pgpreprod.u-ga.fr
        PGUSER=resifinvprod
        PGDATABASE=resifinvdev

esac

archivedir="/mnt/auto/archive/data/bynet/"
buddir="/mnt/auto/archive/rawdata/bud/"

while read line
do
  echo "---- Treating $line ----"
  regex='^([0-9A-Z]+)\.([0-9A-Z]+)\.(.*)\.([A-Z0-9]{3})\.([A-Z]).([12][09][0-9][0-9])\.([0-2][0-9][0-9])$'
  if [[ $line =~ $regex ]]
  then
      net=${BASH_REMATCH[1]}
      sta=${BASH_REMATCH[2]}
      loc=${BASH_REMATCH[3]}
      cha=${BASH_REMATCH[4]}
      year=${BASH_REMATCH[6]}
      day=${BASH_REMATCH[7]}

      if [[ ! "$loc" =~ [0-9]{2} ]]; then
        loc='--'
      fi

      archivepath=${archivedir}/$net/$year/$sta/$cha.D/$line
      if [ -f ${archivepath} ] ; then
          # On lance MSI et on récupère la date de début et de fin de la traceo
          # Ce gros awk permet de récupérer la date de début de la première trace et la date de fin de la dernière trace
          msi -tg -tf 2 -Q $archivepath > /tmp/$$_msi.out
          times_strings=$(awk 'BEGIN {count=0} /^[0-9A-Z]+_/ {if (count == 0) {printf("%s",$2); last=$3; count=1 } else { count += 1; last = $3 }} END { printf(" %s",last)}' /tmp/$$_msi.out)
          read -ra times <<< "$times_strings"
          # echo "  file starts at ${times[0]} and ends at ${times[1]}"
          # On cherche le code qualité, normalement, le premier trouvé est l'unique
          qual=$(awk -F '[_ ]'  '/^[0-9A-Z]+_/{print $5; exit}' /tmp/$$_msi.out)
          block_size=4096
          net_id=$(psql -qt -c "select set_network_id('$net', to_timestamp('${times[0]}') at time zone 'UTC' );")
          net_id=$(($net_id+0))
          sta_id=$(psql -qt -c "select station_id from station where network_id=$net_id and station='$sta' and to_timestamp('${times[0]}') AT time zone 'UTC' between starttime and endtime;")
          sta_id=$(($sta_id+0))
          echo "  -- select channel_id from channel where station_id=$sta_id and channel='$cha' and to_timestamp('${times[0]}') AT time zone 'UTC' between starttime and endtime"
          cha_id=$(psql -qt -c "select channel_id from channel where station_id=$sta_id and location='$loc' and channel='$cha' and to_timestamp('${times[0]}') AT time zone 'UTC' between starttime and endtime")
          cha_id=$((cha_id+0))
          avail='f'
          policy=$(psql -qt -c "select policy from networks where network_id=$net_id"|tr -d ' ')
          if [[ "$policy" -eq "open" ]]; then
              avail='t'
          fi
          sqlreq="INSERT INTO rall VALUES (nextval('rall_id_seq'), $net_id, $sta_id, $cha_id, '$line', '$net', '$sta', '$loc', '$cha', date_trunc('second', to_timestamp('${times[0]}')) at time zone 'UTC', date_trunc('second', to_timestamp('${times[1]}')) at time zone 'UTC', '$qual', '$block_size', '$year', '$avail')"
          echo "$sqlreq"
      else
        echo "  -- $archivepath not in archive"
      fi
      budpath=${buddir}/$year/$net/$sta/$cha.D/$line
      if [ -f ${buddir} ]; then
          echo "  -- Raw file still in bud"
      else
          echo "  -- Cleaning entry in rbud table"
          sqlreq="DELETE FROM rbud WHERE source_file='$line'"
          echo "$sqlreq"
      fi

  else
      echo "  -- Malformed line, skip it"
  fi
done < "${1:-/dev/stdin}"
