#!/bin/bash
set -u
#set -x
export "RESIFMDI_HOME"="`dirname $0`"

# check RUNMODE exists
trap "echo RUNMODE environnement variable must be set to 'test' or 'production'" EXIT
echo $RUNMODE > /dev/null
trap - EXIT

while getopts "f:" flag; do
    case $flag in
        f) export dfile=$OPTARG ;;
    esac
done
shift $((OPTIND-1))

if [[ -z $dfile ]]  ; then
	exit 1
fi

case $RUNMODE in
	"production") 
	export PGDATABASE="resifInv-Prod"
	export PGUSER="resifinvprod"
	export PGHOST="postgres-geodata.ujf-grenoble.fr"
	;;
	"test")
	export PGDATABASE="resifInv-Preprod"
	export PGUSER="resifinvdev"
	export PGHOST="resif-pgpreprod.u-ga.fr"
	;;
esac	
echo -n  "Remove data from $PGDATABASE  using $dfile ? (Y/N)"
read rep
[[ $rep = "Y" ]] && \
psql -d $PGDATABASE -c "DROP TABLE IF EXISTS admin.remove_network CASCADE ;  CREATE TABLE  admin.remove_network ( file TEXT ); INSERT INTO admin.remove_network (file) VALUES ('$dfile'); " && \
psql -d $PGDATABASE -f $RESIFMDI_HOME/remove-xml-file-from-resifinv.sql 



