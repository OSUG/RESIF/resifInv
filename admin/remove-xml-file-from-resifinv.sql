begin;

begin work;

lock table station;
lock table channel;
lock table networks;
lock table comment;
lock table latitude;
lock table longitude;
lock table distance;
lock table site;
lock table operator;
lock table equipment;
lock table type_channel_at_channel;
lock table response;
lock table sensitivity;
lock table waterlevel;
lock table identifier;
lock table external_reference;
lock table dip;
lock table azimuth;
lock table samplerate;
lock table clockdrift;


-- DELETE FROM transaction_integration WHERE code = 'OEJE4171';
DELETE FROM station USING admin.remove_network
WHERE admin.remove_network.file = source_file;


DELETE FROM channel USING admin.remove_network
WHERE admin.remove_network.file = source_file;

DELETE FROM response USING admin.remove_network
WHERE admin.remove_network.file = source_file;

DELETE FROM sensitivity USING admin.remove_network
WHERE admin.remove_network.file = source_file;

DELETE FROM latitude USING admin.remove_network
WHERE admin.remove_network.file = source_file;

DELETE FROM longitude USING admin.remove_network
WHERE admin.remove_network.file = source_file;

DELETE FROM distance USING admin.remove_network
WHERE admin.remove_network.file = source_file;

DELETE FROM comment USING admin.remove_network
WHERE admin.remove_network.file = source_file;

DELETE FROM agency USING admin.remove_network
WHERE admin.remove_network.file = source_file;

DELETE FROM operator USING admin.remove_network
WHERE admin.remove_network.file = source_file;

DELETE FROM equipment USING admin.remove_network
WHERE admin.remove_network.file = source_file;

DELETE FROM site USING admin.remove_network
WHERE admin.remove_network.file = source_file;

DELETE FROM type_channel_at_channel USING admin.remove_network
WHERE admin.remove_network.file = source_file;

DELETE FROM external_reference USING admin.remove_network
WHERE admin.remove_network.file = source_file;

DELETE FROM waterlevel  USING admin.remove_network
WHERE admin.remove_network.file = source_file;

DELETE FROM identifier  USING admin.remove_network
WHERE admin.remove_network.file = source_file;


UPDATE networks SET nb_station = sum_station(network_id) ;
UPDATE station SET nb_channel = sum_channel(station_id);
;

end;

REFRESH MATERIALIZED VIEW channel_light;
REFRESH MATERIALIZED VIEW CONCURRENTLY ws_common;
REFRESH MATERIALIZED VIEW CONCURRENTLY ws_station_text;
REFRESH MATERIALIZED VIEW CONCURRENTLY ws_channel_text;
REFRESH MATERIALIZED VIEW CONCURRENTLY ws_network_text;
REFRESH MATERIALIZED VIEW CONCURRENTLY ws_network_xml;
REFRESH MATERIALIZED VIEW CONCURRENTLY ws_station_xml;
REFRESH MATERIALIZED VIEW CONCURRENTLY ws_channel_xml;


