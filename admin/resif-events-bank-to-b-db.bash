#!/bin/bash
# resif-events-bank-to-b-db.bash
# Mise a jour de la table resifInv-Prod.PUBLIC.assembleddata avec les archives d'éevenements pour l'année
# Les archive d'évenement sont stockées sur SUMMER sous /mnt/auto/archive/metadata/portalproducts/events
#
. $HOME/.bash_profile
set -u
trap "echo RUNMODE environnement variable must be set to 'test' or 'production'" EXIT
echo $RUNMODE > /dev/null
trap - EXIT
case $RUNMODE in
	"production") 
	export PGDATABASE="resifInv-Prod"
	export ARCHIVE="/mnt/auto/archive/metadata/portalproducts/events/"
	export PGUSER="resifinvprod"
	;;
	"test")
	export PGDATABASE="resifInv-Preprod"
	export PGUSER="resifinvdev"
	export ARCHIVE="/mnt/auto/archive/metadata/portalproducts/events/"
	export PGHOST="resif-pgpreprod.u-ga.fr"
	;;
esac	
while getopts "y:" flag; do
    case $flag in
        y) YEAR=$OPTARG ;;
    esac
done
shift $((OPTIND-1))
if [[ -z $YEAR ]] 
	then
		echo "Syntax: resif_events-bank-to-b.bash -y YYYY, exit 2"
		exit 2
fi
echo "Start updating $PGDATABASE.PUBLIC.assembleddata  with RUNMODE=$RUNMODE and YEAR=$YEAR "
cd $ARCHIVE
[[ $? -ne 0 ]] && \
		echo "cannot go to $ARCHIVE, exiting..." && \
		exit 2
cat /dev/null >/tmp/levent_$YEAR 
if [ -d RAP_${YEAR} ] ; then
find RAP_${YEAR} -name '*.txt' -print | while read event; do
text_file=$event
archive_file=`echo $event | sed -e 's/txt/tar.bz2/'`
eventtitle=`basename $event`
name=`echo $eventtitle | cut -d'_' -f 1 | sed -e 's/.000000//'`
eventdate=`echo $eventtitle | cut -d'_' -f 1`
# 2012.02.25T02:53:23.000000_45.710000_6.980000.txt
# 2012.02.25T02:53:23.000000_45.709999_6.980000.txt
latitude=`echo $eventtitle | cut -d'_' -f 2`
longitude=`echo $eventtitle | cut -d'_' -f 3 | cut -d'.' -f 1-2`
magnitude=`grep magnitude= $event | cut -d'=' -f 2`
magtype=`grep magnitude_type $event | cut -d'=' -f 2`
description=`grep description $event | cut -d'=' -f 2`
depth=`grep depth $event | cut -d'=' -f 2`
provider=`grep provider $event | cut -d'=' -f 2`
pga=`grep pga $event | cut -d'=' -f 2`
if [ -f $archive_file ] ; then 
size=`ls -al $archive_file | awk '{print $5}' `
echo "$name|$eventdate|$latitude|$longitude|$depth|$magnitude|$magtype|$description|$provider|$pga|RAP|$text_file|$archive_file|$size" >> /tmp/levent_$YEAR 
fi
done
fi
cd $ARCHIVE
if [ -d RESIF_${YEAR} ] ; then
find RESIF_${YEAR} -name '*.txt' -print | while read event; do
text_file=$event
archive_file=`echo $event | sed -e 's/txt/tar.bz2/'`
eventtitle=`basename $event`
name=`echo $eventtitle | cut -d'_' -f 1 | sed -e 's/.000000//'`
eventdate=`echo $eventtitle | cut -d'_' -f 1`
latitude=`echo $eventtitle | cut -d'_' -f 2`
longitude=`echo $eventtitle | cut -d'_' -f 3 | cut -d'.' -f 1-2`
magnitude=`grep magnitude= $event | cut -d'=' -f 2`
magtype=`grep magnitude_type $event | cut -d'=' -f 2`
description=`grep description $event | cut -d'=' -f 2`
depth=`grep depth $event | cut -d'=' -f 2`
provider=`grep provider $event | cut -d'=' -f 2`
pga=`grep pga $event | cut -d'=' -f 2`
if [ -f $archive_file ] ; then 
size=`ls -al $archive_file | awk '{print $5}' `
echo "$name|$eventdate|$latitude|$longitude|$depth|$magnitude|$magtype|$description|$provider|$pga|RESIF|$text_file|$archive_file|$size"  >> /tmp/levent_$YEAR 
fi
done
fi
psql -c "delete FROM admin.assembleddata_in;"
cat /tmp/levent_$YEAR | psql -c "copy admin.assembleddata_in from STDIN with delimiter '|' ; select * from admin.assembleddata_in;"
psql -c "delete FROM PUBLIC.assembleddata WHERE to_char(event_date,'YYYY') = '$YEAR';"
psql -c "INSERT INTO PUBLIC.assembleddata (name, event_date, latitude, longitude , depth	, magnitude , magtype, description, agency,pga, collector, text_file, archive_file, size) SELECT DISTINCT * from  admin.assembleddata_in;"
