
BEGIN;
DELETE FROM geology 
USING "geologyIn" g
WHERE 
	g.network = geology.network AND
	g.station = geology.station AND
	g.start_year = geology.start_year
;
DELETE FROM "vault"
USING "vaultIn" v
WHERE 
	v.network = vault.network AND
	v.station = vault.station AND
	v.start_year = vault.start_year
;
INSERT INTO "geology"(network,start_year,station,geology) SELECT DISTINCT *  FROM "geologyIn";
INSERT INTO "vault"(network,start_year,station,vault) SELECT DISTINCT * FROM "vaultIn";


/*
        S: sediment based on surface geological observations
        R: rock based on surface geological observations
        A: rock or stiff geological formation (Vs30>800 m/s)
        B: stiff deposits of sand, gravel or overconsolidated clays (Vs30>360m/s and Vs30<800 m/s)
        C: deep deposit of medium dense sand, gravel or medium stiff clays (Vs30>180m/s and Vs30<360 m/s)
        D: loose cohesionless soil deposits (Vs30<180 m/s)
        E: soil made up of superficial alluvial layer, with a thickness ranging from 5 to 20m with Vs30 value in class C and D ranges covering stiffer deposits (class A)
        S1: deposits consisting of a level of soft clays with a thickness of at least 10m, with a high plasticity indices and a high water content (Vs30<100m/s)
        S2: liquefiable soils and soil profiles not included in the soil classes A-E or S1
        BHd: Borehole sensors. d=depth in meters
        Bm.n: Sensors in building (m=total number of stories of the building including ground floor, n=floor where the instrument is installed)
        O: other
        C:Cement
*/
/* POUR LE RAP selon Emeline

S: sediments based on surface geological observations
R: rock based on surface geological observations
A: rock or stiff geological formation (Vs30>800 m/s)
B: stiff deposits of sand, gravel or overconsolidated clays (Vs30>360 m/s and <800 m/s)
C: deep deposits of dense or medium-dense sand, gravel or stiff clay (Vs30>180 m/s and <360 m/s)
D: loose cohesionless soil deposits (Vs30<180 m/s)
E: superficial alluvial layer, with thickness ranging 5-20 m and with Vs30 value in class C or D, covering stiffer material of class A
O: other
*/ 

UPDATE "geology" SET "geology"  = 'S:sediment based on surface geological observations' 
	WHERE "geology" = 'S' and network in ('FR','RA');
UPDATE "geology" SET "geology"  = 'R:rock based on surface geological observations'
	WHERE "geology" = 'R' and network in ('FR','RA');
UPDATE "geology" SET "geology"  = 'A:rock or stiff geological formation (Vs30&gt;800 m/s)'
	WHERE "geology" = 'A' and network in ('FR','RA');
UPDATE "geology" SET "geology"  = 'B:stiff deposits of sand, gravel or overconsolidated clays (Vs30&gt;360m/s and Vs30&lt;800 m/s)'
	WHERE "geology" = 'B' and network in ('FR','RA');
UPDATE "geology" SET "geology"  = 'deep deposit of medium dense sand, gravel or medium stiff clays (Vs30&gt;180m/s and Vs30&lt;360 m/s)'
	WHERE "geology" = 'C' and network in ('FR','RA');
UPDATE "geology" SET "geology"  = 'loose cohesionless soil deposits (Vs30&lt;180 m/s)'
	WHERE "geology" = 'D' and network in ('FR','RA');
UPDATE "geology" SET "geology"  = 'E:superficial alluvial layer, with thickness ranging 5-20 m and with Vs30 value in class C or D, covering stiffer material of class A'
	WHERE "geology" = 'D' and network in ('FR','RA');
UPDATE "geology" SET "geology"  = 'O: other'
	WHERE "geology" = 'O' and network in ('FR','RA');
UPDATE "geology" SET "geology"  = 'S1:deposits consisting of a level of soft clays with a thickness of at least 10m, with a high plasticity indices and a high water content'
	WHERE "geology" = 'S1' and network in ('FR','RA');
UPDATE "geology" SET "geology"  = 'S2:liquefiable soils and soil profiles not included in the soil classes A-E or S1'
	WHERE "geology" = 'S2' and network in ('FR','RA');
UPDATE "geology" SET "geology"  = 'BH:Borehole sensors'
	WHERE "geology" like 'Bh.%' and network in ('FR','RA');
UPDATE "geology" SET "geology"  = 'B:Sensors in building'
	WHERE "geology" like 'B%.%' AND "geology" NOT like 'Bh%' and network in ('FR','RA');
UPDATE "geology" SET "geology"  = 'cement'
	WHERE "geology" = 'cement' and network NOT in ('FR','RA');
UPDATE "geology" SET "geology"  = 'rock'
	WHERE "geology" = 'rock' and network NOT in ('FR','RA');


/*

UPDATE "geology" SET "geology"  = 'S:sediment based on surface geological observations' 
	WHERE "geology" = 'S' and network in ('FR','RA');
UPDATE "geology" SET "geology"  = 'R:rock based on surface geological observations'
	WHERE "geology" = 'R' and network in ('FR','RA');
UPDATE "geology" SET "geology"  = 'A:rock or stiff geological formation'
	WHERE "geology" = 'A' and network in ('FR','RA');
UPDATE "geology" SET "geology"  = 'B:stiff deposits of sand, gravel or overconsolidated clays'
	WHERE "geology" = 'B' and network in ('FR','RA');
UPDATE "geology" SET "geology"  = 'deep deposit of medium dense sand, gravel or medium stiff clays'
	WHERE "geology" = 'C' and network in ('FR','RA');
UPDATE "geology" SET "geology"  = 'loose cohesionless soil deposits'
	WHERE "geology" = 'D' and network in ('FR','RA');
UPDATE "geology" SET "geology"  = 'E:superficial alluvial layer, with thickness ranging 5-20 m and with Vs30 value in class C or D, covering stiffer material of class A'
	WHERE "geology" = 'D' and network in ('FR','RA');
UPDATE "geology" SET "geology"  = 'O: other'
	WHERE "geology" = 'O' and network in ('FR','RA');
UPDATE "geology" SET "geology"  = 'S1:deposits consisting of a level of soft clays with a thickness of at least 10m, with a high plasticity indices and a high water content'
	WHERE "geology" = 'S1' and network in ('FR','RA');
UPDATE "geology" SET "geology"  = 'S2:liquefiable soils and soil profiles not included in the soil classes A-E or S1'
	WHERE "geology" = 'S2' and network in ('FR','RA');
UPDATE "geology" SET "geology"  = 'BH:Borehole sensors'
	WHERE "geology" like 'Bh.%' and network in ('FR','RA');
UPDATE "geology" SET "geology"  = 'B:Sensors in building'
	WHERE "geology" like 'B%.%' AND "geology" NOT like 'Bh%' and network in ('FR','RA');
UPDATE "geology" SET "geology"  = 'cement'
	WHERE "geology" = 'cement' and network NOT in ('FR','RA');
UPDATE "geology" SET "geology"  = 'rock'
	WHERE "geology" = 'rock' and network NOT in ('FR','RA');
*/

REFRESH MATERIALIZED VIEW PUBLIC."metaresponse" WITH DATA;
REFRESH MATERIALIZED VIEW PUBLIC."metaresponse_mtc" WITH DATA;

END;
