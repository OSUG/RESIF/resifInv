#!/bin/bash
#
# Restauration et notification d'une base de données.
# Ce script restaure le dernier dump de resifInv-Prod vers une base resifInv-Preprod

# nom de la base à charger
dbprod=resifInv-Prod
# nom de la base a restaurer
dbpreprod=resifInv-Preprod

# parametre de la fonction report_to_zabbix
zabbix_key=pg_restore_log
zabbix_server=monitoring.osug.fr
zabbix_host=$(hostname)

# On s'attend à trouver les dumps ici:
LOCALDIR=$(dirname $0)
DUMPDIR=$LOCALDIR/../../dumps

function report_to_zabbix(){
    zabbix_sender -k $zabbix_key -o "$@" -z $zabbix_server -s $zabbix_host 2>&1 > /dev/null
}

# On récupere le dump qui a moins de 24 heures
DUMPDB=$(find $DUMPDIR -maxdepth 2 -type d -name $dbprod -mtime -1| tail -1)
[[ -d $DUMPDB ]] || {
    echo $dbprod does not exist
    report_to_zabbix "failed: $dbprod not found in $DUMPDIR"
    exit 1
}
#
# Restauration de la base resifInv-Prod
pg_errors=$(pg_restore --clean --format d --jobs 10 -d resifInv-Preprod $DUMPDB | tail -1)
if [ $? -ne 0 ]; then
    report_to_zabbix "$pg_errors"
fi
psql -d $dbpreprod -c "reassign owned by resifinvprod to resifinvdev"

# Pour tester la fraicheur de la base, on peut regarder l'enregistrement le plus récent dans rbud
# select endtime from rbud where endtime != 'infinity' order by endtime desc limit 1;
