#!/bin/bash
# Mise a jour de la table des informations Geology et Vault. On cherche ces informations sur les machines des noeuds A resif6 et resif7 
# 
# set -x
set -u
export "RESIFMDI_HOME"="`dirname $0`"

# check RUNMODE exists
trap "echo RUNMODE environnement variable must be set to 'test' or 'production'" EXIT
echo $RUNMODE > /dev/null
trap - EXIT

case $RUNMODE in
	"production") 
	export PGDATABASE="resifInv-Prod"
	export PGUSER=resifinvprod
	export PGHOST=postgres-geodata.ujf-grenoble.fr
	;;
	"test")
	export PGDATABASE="resifInv-Test"
	export PGUSER=resifinvdev
	export PGHOST=resif10.u-ga.fr
	;;
esac	
# chargement de la table des réseaux
#
cd $RESIFMDI_HOME
scp rap@resif-vm47.u-ga.fr:LST/User_Files/input/typesol.lst typesol.RAP.lst
scp Z32015@resif-vm48.u-ga.fr:LST/User_Files/input/typesol.lst typesol.Z3.lst
scp Z32015@resif-vm48.u-ga.fr:LST/User_Files/input/vault.lst vault.Z3.lst
echo "Updating $PGDATABASE with  geology and vault"
awk '{printf("RA|1995|%s|%s\n",$1,$2)}' typesol.RAP.lst > geology.lst
awk '{printf("FR|1994|%s|%s\n",$1,$2)}' typesol.RAP.lst >> geology.lst
awk '{printf("Z3|2015|%s|%s\n",$1,$2)}' typesol.Z3.lst >> geology.lst
awk '{printf("Z3|2015|%s|%s %s\n",$1,$2,$3)}' vault.Z3.lst > vault.lst
psql -d $PGDATABASE -c "DELETE FROM \"geologyIn\";DELETE FROM \"vaultIn\";"
cat geology.lst | psql -d $PGDATABASE -c "copy \"geologyIn\" from stdin using delimiters '|' ;"
cat vault.lst | psql -d $PGDATABASE -c "copy \"vaultIn\" from stdin using delimiters '|' ;"

psql -d $PGDATABASE -f ./resifinv-update-geology-vault.sql
