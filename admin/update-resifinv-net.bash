#!/bin/bash
# Mise a jour de la table des réseau 
# 
#set -x
set -u
export "RESIFMDI_HOME"="`dirname $0`"

# check RUNMODE exists
trap "echo RUNMODE environnement variable must be set to 'test' or 'production'" EXIT
echo $RUNMODE > /dev/null
trap - EXIT

case $RUNMODE in
	"production") 
	export PGDATABASE="resifInv-Prod"
	export PGUSER="resifinvprod"
	export PGHOST="resif-pgprod.u-ga.fr"
	;;
	"test")
	export PGDATABASE="resifInv-Preprod"
	export PGUSER="resifinvdev"
	export PGHOST="resif-pgpreprod.u-ga.fr"
	;;
esac	
# chargement de la table des réseaux
#
# récuperation de la table des réseaux
psql -d $PGDATABASE -f ./create-admin-table.sql 2>/dev/null

cd $RESIFMDI_HOME
echo "Updating $PGDATABASE with ./networks.lst"
psql -d $PGDATABASE -c "DELETE FROM admin.networks_in ; ALTER TABLE admin.networks_in DROP COLUMN IF EXISTS fullnet CASCADE; ALTER TABLE admin.networks_in DROP COLUMN IF EXISTS freetime CASCADE;"
cat ./networks.lst | sed "/^\#/d" | awk 'BEGIN {FS="|"} {if (NF=11) printf("%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s\n", $1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11) }'  | \
psql -d $PGDATABASE -c "copy admin.networks_in from stdin using delimiters '|';"
psql -d $PGDATABASE -f ./update-resifinv-network.sql
echo "Adding more network operators from ./more_network_operators.lst"
cat ./more_network_operators.lst | while read line ;do
	if [[ $line =~ ^# ]]; then
		continue
	fi
	if [[ $line =~ ^(.*)\|(.*)\|(.*)\|(.*)$ ]]; then
		echo "More operator : ${BASH_REMATCH[3]} for network ${BASH_REMATCH[1]}_${BASH_REMATCH[2]}"
		# En SQL, si on a une apostrophe dans l'opérateur, il faut la doubler
		devaluated_operator=$(sed -e "s/'/''/g" <<< ${BASH_REMATCH[3]})
		netid=$(psql -qtA -d $PGDATABASE -c "select network_id from networks where network='${BASH_REMATCH[1]}' and start_year=${BASH_REMATCH[2]}" )
		echo "NETID: " $netid
		already_registered=$(psql -qtA -d $PGDATABASE -c "select count(*) from operator where level='n' and operator = '$devaluated_operator' and network_id = $netid")
		if [[ $already_registered -gt 0 ]]; then
			echo "Network operator already registered $already_registered time. Skipping."
		else
			psql -d $PGDATABASE -c "insert into operator (operator_id, operator, network_id, level, xml, source_file) values (
								nextval('operator_id_seq'),
								'$devaluated_operator',
								( select network_id from networks where network='${BASH_REMATCH[1]}' and start_year=${BASH_REMATCH[2]} ),
								'n',
								format('<Operator><Agency>%s</Agency><WebSite>%s</WebSite></Operator>', '$devaluated_operator', '${BASH_REMATCH[4]}'),
								'resifinv'
								)"
		fi
	fi
done

psql -d $PGDATABASE -c "
REFRESH MATERIALIZED VIEW channel_light ;
REFRESH MATERIALIZED VIEW CONCURRENTLY ws_common ;
REFRESH MATERIALIZED VIEW CONCURRENTLY ws_station_text;
REFRESH MATERIALIZED VIEW CONCURRENTLY ws_channel_text;
REFRESH MATERIALIZED VIEW CONCURRENTLY ws_network_text;
REFRESH MATERIALIZED VIEW CONCURRENTLY ws_network_xml;
REFRESH MATERIALIZED VIEW CONCURRENTLY ws_station_xml;
REFRESH MATERIALIZED VIEW CONCURRENTLY ws_channel_xml;"
