-- update-resifinv-network.sql
-- mise a jour de la table des réseaux (non importée du dataless)

BEGIN WORK;
/*
CREATE TABLE networks(
	network_id 	BIGINT DEFAULT 0 PRIMARY KEY,
	transaction_id	BIGINT DEFAULT 0 REFERENCES transaction_integration(transaction_id) ON DELETE SET DEFAULT,
	network		TEXT NOT NULL,	
	start_year	INTEGER NOT NULL DEFAULT 0 CHECK (start_year <= end_year),
	end_year		INTEGER NOT NULL DEFAULT 0 CHECK (end_year >= start_year),
	operator		TEXT DEFAULT 'RESIF (Reseau sismologique et geodesique francais)',
	name			TEXT NOT NULL  DEFAULT '',
	description	TEXT NOT NULL DEFAULT '',
	policy		TEXT DEFAULT 'closed' CHECK (policy IN ('open', 'closed')),		
	url		TEXT NOT NULL default 'http://seismology.resif.fr/',
	fullnet	VARCHAR(6) NOT NULL DEFAULT 'XX',
	starttime	TIMESTAMP WITHOUT TIME ZONE DEFAULT '-infinity',
	endtime	TIMESTAMP WITHOUT TIME ZONE DEFAULT 'infinity',
	nbstat INTEGER DEFAULT 0,
	tr TEXT default NULL,
	doi TEXT default NULL,
	email TEXT NOT NULL default 'datacentre@resif.fr', 
	UNIQUE (network, start_year))
	;
*/

LOCK TABLE networks;		-- tables des réseaux
LOCK TABLE comment;		-- tables des commentaires
LOCK TABLE identifier;  -- table des identifiers
LOCK TABLE operator;  	-- table des operateurs
LOCK TABLE admin.networks_in;		
--
-- Mise a jour du fullnet dans networksIn
--
-- ALTER TABLE admin.networks_in DROP COLUMN IF EXISTS fullnet CASCADE; 
ALTER TABLE admin.networks_in ADD COLUMN fullnet TEXT; 

UPDATE admin.networks_in SET fullnet = network WHERE network in ('G','WI','PF','GL','MQ','FR','RA','RD','CL','ND','MT','XX');
UPDATE admin.networks_in SET fullnet = replace(network||to_char(start_year,'99999'),' ','')  
WHERE network NOT in ('G','WI','PF','GL','MQ','FR','RA','RD','CL','ND','MT','XX');


DROP TABLE IF EXISTS temp_netid CASCADE;
CREATE TEMPORARY TABLE temp_netid (net_id BIGINT) on commit drop;

insert into temp_netid (net_id) 
select distinct max(networks.network_id) 
from networks; -- network_id n'est pour le moment pas une sequence !!!

-- on cree une sequence temporaire qui demarre à 1
DROP sequence if exists n_id_seq cascade;
create sequence n_id_seq minvalue 1 start 1;

INSERT INTO  networks (
   network_id,
	network, 
	start_year,
	end_year,
	altcode,
	description, 
	policy
	) 
SELECT DISTINCT 
   temp_netid.net_id + nextval('n_id_seq'), 
	i.network, 
	i.start_year,
	i.end_year,
	i.name,
	i.description, 
	i.policy
from admin.networks_in i, temp_netid
WHERE (i.network, i.start_year) NOT IN (SELECT DISTINCT n.network, n.start_year FROM networks n);

-- select * from networks ;
UPDATE networks 
SET end_year = i.end_year 
FROM admin.networks_in i
WHERE i.network = networks.network and i.start_year = networks.start_year;
UPDATE networks 
SET altcode = i.name 
FROM admin.networks_in i 
WHERE i.network = networks.network and i.start_year = networks.start_year;
UPDATE networks 
SET description = i.description 
FROM admin.networks_in i
WHERE i.network = networks.network and i.start_year = networks.start_year;
UPDATE networks 
SET policy = i.policy 
FROM admin.networks_in i
WHERE i.network = networks.network and i.start_year = networks.start_year;


UPDATE networks SET starttime = TO_TIMESTAMP(start_year || '-01-01 00:00:00','YYYY-MM-DD HH24:MI:SS');
-- UPDATE networks SET starttime = TO_TIMESTAMP(start_year || '-07-01 00:00:00','YYYY-MM-DD HH24:MI:SS') where network = 'Z3';
UPDATE networks SET starttime = TO_TIMESTAMP(start_year || '-06-14 00:00:00','YYYY-MM-DD HH24:MI:SS') where network = '1E' and start_year = '2008';

UPDATE networks SET endtime = 'infinity'::TIMESTAMP WITHOUT TIME ZONE WHERE endtime > '2500-01-01'::TIMESTAMP WITHOUT TIME ZONE ;
UPDATE networks SET endtime = TO_TIMESTAMP( end_year || '-12-31 23:59:59.999999','YYYY-MM-DD HH24:MI:SS.US') WHERE endtime < '2500-01-01'::TIMESTAMP WITHOUT TIME ZONE ;
-- UPDATE networks SET endtime = TO_TIMESTAMP( end_year || '-07-31 00:00:00','YYYY-MM-DD HH24:MI:SS') where network = 'Z3';
UPDATE networks SET endtime = TO_TIMESTAMP( end_year || '-12-31 00:00:00','YYYY-MM-DD HH24:MI:SS') where network = 'Z3';
UPDATE networks SET endtime = TO_TIMESTAMP( end_year || '-10-10 23:59:59.999999','YYYY-MM-DD HH24:MI:SS.US') where network = '1E' and end_year = '2008';
UPDATE networks SET endtime = TO_TIMESTAMP( end_year || '-12-31 23:59:59.999999','YYYY-MM-DD HH24:MI:SS.US') where network = '1T' and end_year = '2020';
UPDATE networks SET endtime = TO_TIMESTAMP( end_year || '-12-31 23:59:59.999999','YYYY-MM-DD HH24:MI:SS.US') where network = '3C' and end_year = '2020';

-- mise a jour de la table des identifiers
--
delete from identifier 
where level = 'n' and 
type = 'DOI' and
xml like '%DOI%';

insert into identifier (identifier_id, network_id, station_id, channel_id, level, type, identifier, source_file, xml)
select distinct 
nextval('identifier_id_seq'), 
n.network_id,
0, 0, 'n', 'DOI', i.doi, 'resifinv', 
'<Identifier type="DOI">'||i.doi||'</Identifier>'
from networks n, admin.networks_in i
where i.doi != '#' and
i.network = n.network and i.start_year = n.start_year;

-- que faire de agency et de operator ? 
/*
resifInv-Prod=> \d operator;
                Table "public.operator"
   Column    |  Type  | Collation | Nullable | Default 
-------------+--------+-----------+----------+---------
 operator_id | bigint |           | not null | 0
 agency_id   | bigint |           |          | 0
 operator    | text   |           |          | 
 network_id  | bigint |           |          | 0
 station_id  | bigint |           |          | 0
 level       | text   |           |          | 
 source_file | text   |           |          | 
 xml         | text   |           |          | 
Indexes:
    "operator_pkey" PRIMARY KEY, btree (operator_id)
    "ix_operator_001" btree (network_id)
    "ix_operator_002" btree (station_id)
    "ix_operator_003" btree (source_file)
    "ix_operator_004" btree (level)
    "ix_operator_005" btree (network_id, level)
    "ix_operator_006" btree (station_id, level)
Foreign-key constraints:
    "operator_agency_id_fkey" FOREIGN KEY (agency_id) REFERENCES agency(agency_id) ON DELETE SET DEFAULT
    "operator_network_id_fkey" FOREIGN KEY (network_id) REFERENCES networks(network_id) ON DELETE SET DEFAULT
    "operator_station_id_fkey" FOREIGN KEY (station_id) REFERENCES station(station_id) ON DELETE SET DEFAULT

*/

delete from operator where level = 'n';
insert into operator (operator_id, operator, network_id, level, source_file, xml)
select distinct 
nextval('operator_id_seq'), 
i.operator, n.network_id,'n','resifinv',
format('<Operator><Agency>%s</Agency><WebSite>%s</WebSite></Operator>', i.operator, i.url)
from networks n, admin.networks_in i
where
i.network = n.network and i.start_year = n.start_year;

update networks set nb_station = sum_station(network_id);
update station set nb_channel = sum_channel(station_id);

END;
