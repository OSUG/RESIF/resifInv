#!/bin/bash
# Mise a jour de la table des réseau 
# 
set -x
set -u
export "RESIFMDI_HOME"="`dirname $0`"

# check RUNMODE exists
trap "echo RUNMODE environnement variable must be set to 'test' or 'production'" EXIT
echo $RUNMODE > /dev/null
trap - EXIT
while getopts "f:y:" flag; do
    case $flag in
        f) export file=$OPTARG ;;
        y) export year=$OPTARG ;;
    esac
done
shift $((OPTIND-1))
if [[ ! -f $file ]]  ; then
	exit 1
fi

case $RUNMODE in
	"production") 
	export PGDATABASE="resifInv-Prod"
	export PGUSER="resifinvprod"
	export PGHOST="postgres-geodata.ujf-grenoble.fr"
	;;
	"test")
	export PGDATABASE="resifInv-Preprod"
	export PGUSER="resifinvdev"
	export PGHOST="resif-pgpreprod.u-ga.fr"
	;;
esac	
# chargement de la table des réseaux
# récuperation de la table des réseaux
# for dir in `ls`; do cd $dir; find . -type f -print > ~/work/$dir.lst; cd ..; done
#
cd $RESIFMDI_HOME
echo "Updating $PGDATABASE archive inventory  with ./${year}"
psql -d $PGDATABASE -f ./create-admin-table.sql
psql -d $PGDATABASE -c "DELETE FROM admin.archive_in cascade;"
cat ./$file | psql -d $PGDATABASE -c "copy admin.archive_in from stdin;"
# psql -d $PGDATABASE -c "delete from rall where year = '$year' and channel_id = 0;")
psql -d $PGDATABASE -f ./update-resifinv-orphanfile.sql
