-- update resifInv with orphanfile
-- 
delete from admin.archive_rall;
insert into admin.archive_rall(source_file)
 select basename(i.source_file,'/')  from admin.archive_in i;

update admin.archive_rall set network = split_part(source_file,'.',1);
update admin.archive_rall set station = split_part(source_file,'.',2);
update admin.archive_rall set location = split_part (source_file,'.',3);
update admin.archive_rall set location = '--' where location ='';
update admin.archive_rall set channel = split_part (source_file,'.',4);
update admin.archive_rall set year = split_part (source_file,'.',6)::integer;
update admin.archive_rall set starttime = to_date ( split_part (source_file,'.',6) || '.' ||split_part (source_file,'.', 7), 'YYYY.DDD');
update admin.archive_rall set endtime = starttime + cast ('1 days' as interval);

CREATE OR REPLACE FUNCTION public.set_channel_id(text, text, text, text, timestamp without time zone, timestamp without time zone)  RETURNS bigint AS $$
	DECLARE 
	net		ALIAS FOR $1;		-- code reseau FDSN de la station
	sta	ALIAS FOR $2;		-- code station
	loc		ALIAS FOR $3;		-- location
	chan		ALIAS FOR $4;		-- channel
	cstart	ALIAS FOR $5;		-- une date
	cend		ALIAS FOR $6;		-- date d'arret du channel
	id  		BIGINT ;					-- station_id pour ce channel
	idt		BIGINT ;
	BEGIN
	idt  := (
			select DISTINCT c.channel_id 
			FROM ws_common c
			WHERE 
			(c.channel = chan and
			c.location = loc and 
			c.station = sta and
			c.network = net and 
			cstart BETWEEN c.cstarttime AND c.cendtime)
			OR
			(c.channel = chan and
			c.location = loc and 
			c.station = sta and
			c.network = net and 
			cend BETWEEN c.cstarttime AND c.cendtime)
			ORDER BY c.channel_id DESC
			LIMIT 1);
	IF (idt IS NULL) THEN id:=0; ELSE id:=idt; END IF;
	RETURN id;
	END; 
$$  LANGUAGE 'plpgsql';

update admin.archive_rall set channel_id = set_channel_id (network, station, location, channel, starttime, endtime);
update admin.archive_rall set network_id = set_network_id(network, starttime);
-- update admin.archive_rall set channel_id = set_station_id_in_rbud(network, station, starttime, endtime);

select count(*) from admin.archive_rall; 
select count(*) from admin.archive_rall where channel_id = 0;
