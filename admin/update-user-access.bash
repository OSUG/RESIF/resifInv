#!/bin/bash
set -u
# set -x
export "RESIFMDI_HOME"="`dirname $0`"

cd $RESIFMDI_HOME

echoerr() { echo "$@" 1>&2; }

# check RUNMODE exists
trap "echo RUNMODE environnement variable must be set to 'test' or 'production'" EXIT
echo $RUNMODE > /dev/null
trap - EXIT
case $RUNMODE in
	"production") 
	export PGDATABASE="resifInv-Prod"
	export PGUSER="resifinvprod"
	export PGHOST="resif-pgprod.u-ga.fr"
	;;
	"test")
	export PGDATABASE="resifInv-Preprod"
	export PGUSER="resifinvdev"
	export PGHOST="resif-pgpreprod.u-ga.fr"
	;;
esac	
psql -d $PGDATABASE -f ./create-admin-table.sql 2>/dev/null
echo "Updating $PGDATABASE with $RESIFMDI_HOME/restricted-acces.lst"
psql -d $PGDATABASE -c "DELETE FROM admin.user_in ;"
cat ./restricted-acces.lst | sed -e "/^\#/d" -e "/^$/d" | psql  -d $PGDATABASE -c "copy admin.user_in from stdin using delimiters '|';"
psql -d $PGDATABASE -f ./update-user-access.sql


