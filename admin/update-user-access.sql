-- resif-user-access.sql
-- mise a jour de la table aut_user qui contient les ID des utilisateurs authoentifiés, pour l'ensemble
-- des datasets auxquels ils ont droit
-- CPequegnat 2019.03.26 : la table aut_user a été renommée resif_user dans le cadre de la migration de resiffInv pour l'authentification fédérée de EIDA

BEGIN;

-- LOCK TABLE "admin.user_in";		-- tables des user
LOCK TABLE "resif_users";		-- tables des user
DELETE FROM "resif_users";
INSERT INTO  resif_users(network, start_year, end_year, name)
SELECT DISTINCT 
a.network, 
a.start_year,
a.end_year,
a.name
from admin.user_in a 
;

-- Ajout de resifdc un utilisateur ayant accès à tout

insert into resif_users(network, start_year, end_year, name) select distinct n.network,n.start_year,n.end_year,'resifdc' from networks as n;

UPDATE resif_users SET network_id = set_network_id_bis(network, start_year, end_year);

END;
