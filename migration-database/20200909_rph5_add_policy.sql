CREATE TYPE enum_type AS ENUM ( 'open', 'closed' );
ALTER TABLE rph5 ADD COLUMN policy enum_type;
UPDATE rph5 SET policy=networks.policy::enum_type FROM networks WHERE networks.network_id=rph5.network_id;
ALTER TABLE rph5 ADD COLUMN samplerate text;