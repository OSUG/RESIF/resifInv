/*
Patch relatif à l'affichage des commentaires level network, redondants.
Modifications similaires à celle qui a permis de traiter la redondances des opérateurs au niveau réseau.
Le bug est du au fait qu'il y a un operateur de réseau par station dans la table comment (du au mecanisme d'insertion par station), et que le même commentaire
doit etre factorisé pour toutes les stations du reseau
*/

--
-- vue publique pour distinguer les commentaires de réseaux des commentaires de station et de channel,
-- déclaration reportée dans resifinv/SQL/resif-view.sql
--
DROP VIEW if exists public.network_comment CASCADE;
CREATE VIEW public.network_comment AS
SELECT DISTINCT 
c.network_id, 
c.xml
FROM comment c
where c.xml != '' and c.network_id != 0 and c.level = 'n';
--
-- fonction publique pour priduire les commentaires aux format XML,
-- déclaration reportée dans resifinv/SQL/resif-fonction.sql
--

CREATE OR REPLACE FUNCTION public."comment-xml" (BIGINT, TEXT) RETURNS TEXT AS $$
	DECLARE
	cid ALIAS FOR $1;		-- id for a network, a station or a  channel
	clevel ALIAS FOR $2; --'n', 's', 'c'
	xml TEXT; -- RETURN value
	ccomment "comment"%ROWTYPE; 
	ncomment "network_comment"%ROWTYPE; 
	BEGIN
	xml:='';
	CASE 	
		WHEN  (clevel = 'n')  THEN
 		 	FOR ncomment IN SELECT distinct * FROM public.network_comment n WHERE n.network_id  =  cid
				LOOP
					xml := xml||ncomment.xml;
				END LOOP;
		WHEN (clevel = 's')  THEN
 		 	FOR ccomment IN SELECT distinct * FROM public.comment c WHERE c.station_id  =  cid and c.level = clevel
				LOOP
					xml := xml||ccomment.xml;
				END LOOP;
		ELSE  	
 		 	FOR ccomment IN SELECT distinct * FROM public.comment s WHERE s.channel_id  =  cid and s.level = clevel
				LOOP
					xml := xml||ccomment.xml;
				END LOOP ;
		END CASE;
		RETURN xml;
END;
$$ LANGUAGE plpgsql;

REFRESH MATERIALIZED VIEW  channel_light ;
REFRESH MATERIALIZED VIEW CONCURRENTLY ws_common ;
REFRESH MATERIALIZED VIEW CONCURRENTLY ws_station_text;
REFRESH MATERIALIZED VIEW CONCURRENTLY ws_channel_text;
REFRESH MATERIALIZED VIEW CONCURRENTLY ws_network_text;
REFRESH MATERIALIZED VIEW CONCURRENTLY ws_network_xml;
REFRESH MATERIALIZED VIEW CONCURRENTLY ws_station_xml;
REFRESH MATERIALIZED VIEW CONCURRENTLY ws_channel_xml;

