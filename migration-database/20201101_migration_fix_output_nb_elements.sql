/*
Afficher le nombre de station dans un réseau et le nombre de channel dans une station en fonction de ws_common, afin de garantir le bon nombre d'éléments dans l'output.
*/

--
-- fonction publique pour compter le nombre de station par réseau selon leur présence dans ws_common (stations 'complètes')
-- on prend en compte le champ station et non le champ station_id, ce qui signifie que on affiche le nombre de stations effectives,
-- toute 'epoch de station confindues'
-- déclaration reportée dans resifinv/SQL/resif-fonction.sql
--
CREATE OR REPLACE  FUNCTION  public.sum_station_ws_common(BIGINT) RETURNS INTEGER AS $$
	DECLARE
		netid					ALIAS FOR $1;				--  id du reseau
		nbstat				INTEGER ;		      
	BEGIN
		nbstat := 0;
		SELECT COUNT(DISTINCT w.station)
				FROM public.ws_common w
				WHERE w.network_id = netid	
		INTO nbstat;
		RETURN nbstat ;
	END;
$$ LANGUAGE 'plpgsql';

--
-- fonction publique pour compter le nombre de channels par station selon leur présence dans ws_common (channels 'complets')
-- déclaration reportée dans resifinv/SQL/resif-fonction.sql

CREATE OR REPLACE  FUNCTION  public.sum_channel_ws_common(BIGINT) RETURNS INTEGER AS $$
	DECLARE
		staid				ALIAS FOR $1;				--  id de la station
		nbchan			INTEGER ;		      
	BEGIN
		nbchan := 0; 
		SELECT COUNT(DISTINCT w.channel_id)
				FROM public.ws_common w
				WHERE w.station_id = staid	
		INTO nbchan;
		RETURN nbchan ;
	END;
$$ LANGUAGE 'plpgsql';

--
-- Fonctions pour le formatage textuel d'un réseau, avec le nombre de ses stations qui sont sont dans ws_common
--
-- #Network|Description|StartTime|EndTime|TotalStations
--
CREATE OR REPLACE FUNCTION public."network-txt" (BIGINT) RETURNS TEXT AS $$
	DECLARE 
	netid	ALIAS FOR $1;		-- code reseau FDSN
	txt TEXT; 					-- RETURN
	BEGIN
	txt :=	n.network ||'|'|| n.description ||'|'|| 
		TO_CHAR(n.starttime,'YYYY-MM-DD') || 'T'|| TO_CHAR(n.starttime,'HH24:MI:SS') ||'|'||
		-- TODO : à remplacer lorsque l'ancien portail sera hors de service
		-- CASE WHEN n.endtime = 'infinity' THEN  '' 
			-- ELSE TO_CHAR(n.endtime,'YYYY-MM-DD') || 'T'|| TO_CHAR(n.endtime,'HH24:MI:SS') END 
		CASE WHEN n."endtime" = 'infinity' THEN  '2500-12-31T23:59:59' 
		   ELSE TO_CHAR(n."endtime",'YYYY-MM-DD') || 'T'|| TO_CHAR(n."endtime",'HH24:MI:SS') END 
		||'|'|| public.sum_station_ws_common(netid)
	FROM public.networks n 
	WHERE n.network_id = netid;
	RETURN txt;
	END; 
$$ LANGUAGE 'plpgsql';

--
-- Fonctions pour le formatage xml d'un réseau, avec le nombre de ses stations qui sont sont dans ws_common
--
--
CREATE OR REPLACE FUNCTION public."network-xml" (BIGINT) RETURNS TEXT AS $$
	DECLARE 
	netid	ALIAS FOR $1;		-- code reseau FDSN
	xml TEXT; -- RETURN
	BEGIN
	xml :='<Network code="' || n.network || '"' ||
		CASE WHEN (n.altcode is not null ) THEN ' alternateCode="' || n.altcode ||  '"' ELSE '' END ||
		CASE WHEN (n.histcode is not null ) THEN ' historicalCode="' || n.histcode ||  '"' ELSE '' END ||
		' startDate="' || TO_CHAR(n.starttime,'YYYY-MM-DD') || 'T'|| TO_CHAR(n.starttime,'HH24:MI:SS') || '"' ||
		-- CASE WHEN (n.endtime = 'infinity'::TIMESTAMP WITHOUT TIME ZONE)  THEN '' 
		-- ELSE
		' endDate="' ||
		  CASE WHEN (n."endtime" = 'infinity'::TIMESTAMP WITHOUT TIME ZONE)  THEN '2500-12-31T23:59:59"'  
				ELSE TO_CHAR(n."endtime",'YYYY-MM-DD') || 'T'|| TO_CHAR(n."endtime",'HH24:MI:SS') || '"'  END || 
		' restrictedStatus="' || n.policy || '">'  ||
		CASE WHEN (n.description is not null ) THEN '<Description>' || n.description || '</Description>' ELSE '' END ||
		public."identifier-xml" (n.network_id, 'n') || 
		public."comment-xml" (n.network_id, 'n') || 
	   -- public."operator-xml" (n.network_id, 'n') || 
	   public."operator-xml" (n.network_id, 'n', n.network,n.description) || 
	'<TotalNumberStations>' || public.sum_station_ws_common (netid) || '</TotalNumberStations>' 
-- la fin de la structure network ne peut être construite en statique, la mise a jour se fait dynamiquement
	FROM networks n WHERE n.network_id = netid;
	RETURN xml;
	END; 
$$ LANGUAGE 'plpgsql';

-----------------------------------------------------------------------------------
-- part 1 level station, extenddedattribute = true

CREATE OR REPLACE FUNCTION public."station-xml1" (BIGINT) RETURNS TEXT AS $$
	DECLARE 
	staid	ALIAS FOR $1;		
	xml TEXT; 
	BEGIN
	xml := '<Station code="' || s.station || '"' || 
	CASE WHEN (s.altcode is not null ) THEN ' alternateCode="' || s.altcode ||  '"' ELSE '' END ||
	CASE WHEN (s.histcode is not null ) THEN ' historicalCode="' || s.histcode ||  '"' ELSE '' END ||
	' startDate="' || TO_CHAR(s.starttime,'YYYY-MM-DD') || 'T'|| TO_CHAR(s.starttime,'HH24:MI:SS') || '"' || 
	-- CASE WHEN (s.endtime = 'infinity'::TIMESTAMP WITHOUT TIME ZONE)  THEN '' 
 		-- 	ELSE 
 	-- ' endDate="' || TO_CHAR(s.endtime,'YYYY-MM-DD') || 'T'|| TO_CHAR(s.endtime,'HH24:MI:SS.US') || '"'  END || 
 	' endDate="' || 
	CASE WHEN s."endtime" = 'infinity' THEN '2500-12-31T23:59:59"' 
		ELSE TO_CHAR(s."endtime",'YYYY-MM-DD') || 'T'|| TO_CHAR(s."endtime",'HH24:MI:SS') || '"' END  || 
	 ' restrictedStatus="' || n.policy  ||  '"' || 
    ' resif:alternateNetworkCodes="' || public.fullnet(n.network_id) || '">' ||
	CASE WHEN (s.description is not null ) THEN '<Description>' || s."description" || '</Description>' ELSE '' END  ||
	public."comment-xml" (s.station_id, 's')  || 
	public."latitude-xml" (s.latitude_id, 's')  ||
	public."longitude-xml" (s.longitude_id, 's')  ||
	public."distance-xml" (s.elevation_id, 's', 'e') ||
	-- public."waterlevel-xml" (s.waterlevel_id, 's')   || 
	CASE WHEN s.site_id = 0 THEN '<Site><Name>' || s.station || '</Name></Site>' ELSE public."site-xml" (s.site_id) END ||
	-- public."vault-xml" (s.vault_id) ||
	public."geology-xml" (s.geology_id)  ||
	-- Pour ne pas effectuer la réécriture de la liste des operator :
	-- public."operator-xml" (s.station_id, 's') || 
	-- Pour effet la réécriture de l'operateur
	public."operator-xml" (s.station_id, 's', n.network, n.description) || 
	-- public."equipment-xml" (s.station_id, 's')  || 
	CASE WHEN (s.creationdate is not null) 
		THEN   '<CreationDate>' || TO_CHAR(s."creationdate",'YYYY-MM-DD') || 'T'|| TO_CHAR(s."creationdate",'HH24:MI:SS') || '</CreationDate>' ELSE 
	   '<CreationDate>' || TO_CHAR(s."starttime",'YYYY-MM-DD') || 'T'|| TO_CHAR(s."starttime",'HH24:MI:SS') || '</CreationDate>' END ||
	/* CASE WHEN (s.terminationdate is not null) 
		THEN '<TerminationDate>' || TO_CHAR(s."terminationdate",'YYYY-MM-DD') || 'T'|| TO_CHAR(s."terminationdate",'HH24:MI:SS') || 'Z</TerminationDate>' ELSE '' END  || 
		*/
	'<TotalNumberChannels>' || public.sum_channel_ws_common(staid) || '</TotalNumberChannels>' 
	FROM networks n, 
	station s 
	WHERE s.station_id = staid  
	and n.network_id = s.network_id 
	; 
	RETURN xml;
	END; 
$$ LANGUAGE 'plpgsql';
--
-- -- part 1 level station, extenddedattribute = false
--
CREATE OR REPLACE FUNCTION public."station-xml11" (BIGINT) RETURNS TEXT AS $$
	DECLARE 
	staid	ALIAS FOR $1;	
	xml TEXT; 
	BEGIN
	xml := '<Station code="' || s.station || '"' || 
	CASE WHEN (s.altcode is not null ) THEN ' alternateCode="' || s.altcode ||  '"' ELSE '' END ||
	CASE WHEN (s.histcode is not null ) THEN ' historicalCode="' || s.histcode ||  '"' ELSE '' END ||
	' startDate="' || TO_CHAR(s.starttime,'YYYY-MM-DD') || 'T'|| TO_CHAR(s.starttime,'HH24:MI:SS') || '"' || 
	-- CASE WHEN (s.endtime = 'infinity'::TIMESTAMP WITHOUT TIME ZONE)  THEN '' 
 			-- ELSE 
 			-- ' endDate="' || TO_CHAR(s.endtime,'YYYY-MM-DD') || 'T'|| TO_CHAR(s.endtime,'HH24:MI:SS.US') || '"'  END || 
 	' endDate="' || 
	CASE WHEN s."endtime" = 'infinity' THEN '2500-12-31T23:59:59"' 
		ELSE TO_CHAR(s."endtime",'YYYY-MM-DD') || 'T'|| TO_CHAR(s."endtime",'HH24:MI:SS') || '"' END || 
	' restrictedStatus="' || n.policy  ||  '">' || 
	CASE WHEN (s.description is not null ) THEN '<Description>' || s."description" || '</Description>' ELSE '' END ||
	public."comment-xml" (s.station_id, 's')  || 
	public."latitude-xml" (s.latitude_id, 's')  ||
	public."longitude-xml" (s.longitude_id, 's')  ||
	public."distance-xml" (s.elevation_id, 's', 'e') ||
	-- public."waterlevel-xml" (s.waterlevel_id, 's')   || 
	CASE WHEN s.site_id = 0 THEN '<Site><Name>' || s.station || '</Name></Site>' ELSE public."site-xml" (s.site_id) END ||
	-- public."vault-xml" (s.vault_id) ||
	public."geology-xml" (s.geology_id)  ||
	-- Pour ne pas effectuer la réécriture de la liste des operator :
	-- public."operator-xml" (s.station_id, 's') || 
	-- Pour effet la réécriture de l'operateur
	public."operator-xml" (s.station_id, 's',n.network,n.description) || 
	--public."equipment-xml" (s.station_id, 's')  || 
	CASE WHEN (s.creationdate is not null) 
		THEN   '<CreationDate>' || TO_CHAR(s."creationdate",'YYYY-MM-DD') || 'T'|| TO_CHAR(s."creationdate",'HH24:MI:SS') || '</CreationDate>' ELSE 
	   '<CreationDate>' || TO_CHAR(s."starttime",'YYYY-MM-DD') || 'T'|| TO_CHAR(s."starttime",'HH24:MI:SS') || '</CreationDate>' END ||
	   /*
	CASE WHEN (s.terminationdate is not null and s.endtime !=  'infinity'::TIMESTAMP WITHOUT TIME ZONE) 
			THEN '<TerminationDate>' || TO_CHAR(s."terminationdate",'YYYY-MM-DD') || 'T'|| TO_CHAR(s."terminationdate",'HH24:MI:SS') || 'Z</TerminationDate>' ELSE '' END ||
			*/
	'<TotalNumberChannels>' || public.sum_channel_ws_common(staid)  || '</TotalNumberChannels>' 
FROM 	networks n, station s
	WHERE 	s.station_id = staid AND n.network_id = s.network_id  ; 
	RETURN xml;
	END; 
$$ LANGUAGE 'plpgsql';

REFRESH MATERIALIZED VIEW  channel_light ;
REFRESH MATERIALIZED VIEW CONCURRENTLY ws_common ;
REFRESH MATERIALIZED VIEW CONCURRENTLY ws_station_text;
REFRESH MATERIALIZED VIEW CONCURRENTLY ws_channel_text;
REFRESH MATERIALIZED VIEW CONCURRENTLY ws_network_text;
REFRESH MATERIALIZED VIEW CONCURRENTLY ws_network_xml;
REFRESH MATERIALIZED VIEW CONCURRENTLY ws_station_xml;
REFRESH MATERIALIZED VIEW CONCURRENTLY ws_channel_xml;

