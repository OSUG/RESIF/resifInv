--- [Pequegnat 2021-03-26]
--- Attention a l'dre des argment dans une clause LIKE. Corrige le bugdes caractères joket non expansé dans le cas 
--- du reseau
--- construction de la clause where pour le paramètre network=
--( )
CREATE OR REPLACE function public.network_clause() RETURNS TEXT AS $$
DECLARE
clause TEXT;
netall integer;    -- # of different network code in the request
netstar integer;    -- # of '*' for network in the request
netlike integer;
BEGIN
clause := ' AND w.network LIKE t.network '; 
netall :=  (select count (distinct t.network) from temp_req t);
netstar := (select count (distinct t.network) from temp_req t where t.network = '*');
netlike := 
   (select count (distinct t.network) from temp_req t where t.network like '%*%') +
   (select count (distinct t.network) from temp_req t where t.network like '*%') +
   (select count (distinct t.network) from temp_req t where t.network like '%*') +
   (select count (distinct t.network) from temp_req t where t.network like '%?%') +
   (select count (distinct t.network) from temp_req t where t.network like '%?') +
   (select count (distinct t.network) from temp_req t where t.network like '?%');
CASE
   -- il n'y a qu'un seul code network et il est égal = '*'
   WHEN ((netall = 1) and (netstar = 1)) THEN
      clause := '';
   -- il y a plusieurs codes network et aucun d'entre eux ne contient de joker
   WHEN (netlike = 0)  THEN
--      clause := ' AND t.network = w.network ';
     clause := ' AND w.network = t.network '; 
   ELSE
--      clause := ' AND t.network LIKE w.network ';
	clause := ' AND w.network LIKE t.network '; 
END CASE;
return clause;
END; $$ 
LANGUAGE 'plpgsql';