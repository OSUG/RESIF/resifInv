/*
Migration du contenu de la table des opérateurs pour conformité à Gatito

Cette migration concerne uniquement les résaux temporaires.

Les intitulés doivent être conforms à Gatito :
  https://gitlab.com/resif/gatito/-/blob/master/resif/operator.yaml


Opération planifiée le 3 mai 2022

Pour tester :

  resif-pgpreprod.u-ga.fr resifinvdev@resifInv-Preprod=> delete from operator ;
  [postgres@resif10 ~]$ pg_restore --clean -d resifInv-Preprod -t operator  /mnt/nfs/summer/pgdumps/dumps/dump-2022-115-00\:00\:01/resifInv-Prod.dump
  resif-pgpreprod.u-ga.fr resifinvdev@resifInv-Preprod=> \i migration-database/20220503_gatito_operators.sql


*/

begin;

create or replace function new_operator(text,text,text,text) returns bigint[] as $$
-- Pour chaque opérateur dont l'intitulé correspond au pattern ($1), cette fonction crée un nouvel opérateur avec le titre ($2), le site web ($3) et l'email ($4), lié à la même station.
-- La fonction se limite aux opérateurs des réseaux temporaires. Identifiés sur le source_file
-- La fonction renvoie la liste des operator_id qui correspondent au pattern.
-- params:
-- - pattern : un pattern pour rechercher les opérateurs
-- - title : le nouveau titre de l'opérateur
-- - website : le site web de l'opérateur
-- - email : l'email de l'opérateur
declare
    pattern alias for $1;
    title alias for $2;
    website alias for $3;
    email alias for $4;
    op record;
    op_to_delete bigint[];
begin
        raise notice 'Begin of new_operator(%,%,%,%)', pattern, title, website, email;
        for op in select operator_id, station_id, source_file from operator as o where o.operator ~ pattern and source_file ~ '^[0-9XYZ][0-9A-Z]'
        loop
            raise notice 'Operator to transform for % : %', pattern, op;
            op_to_delete := op_to_delete || op.operator_id;
            insert into operator (operator_id, operator, station_id, level, source_file, xml)
            values ((select max(operator_id)+1 from operator),
                    title,
                    op.station_id,
                    's',
                    op.source_file,
                    format('<Operator><Agency>%s</Agency><Contact><Email>%s</Email></Contact><WebSite>%s</WebSite></Operator>', title, email, website)
                    );
        end loop;
        raise notice 'End of new_operator';
        return op_to_delete;
end;
$$ language plpgsql;

create or replace function migrate_gatito() returns bigint[] AS $$
    declare
    pattern text;
    title text;
    website text;
    email text;
    op_to_delete bigint[];
    op record;
    begin
        -- D'abbord, pour tous les réseaux temporaire, je mets le bon opérateur Sismob.
        -- Cela devrait augmenter les chances que cet opérateur apparaisse en premier
        -- Attention, on ne corrige pas Z3 ni ZR qui est déjà OK
        -- TODO Station AST04 fini avec SisMob en double. Pourquoi ?
        for op in select distinct station_id, source_file from operator as o where source_file ~ '^[0-9XYZ][0-9A-Z]' and not source_file ~ 'Z[3R]'
        loop
            insert into operator (operator_id, operator, station_id, level, source_file, xml)
            values ((select max(operator_id)+1 from operator),
                    'Parc national sismologique mobile (SisMob)',
                    op.station_id,
                    's',
                    op.source_file,
                    '<Operator><Agency>Parc national sismologique mobile (SisMob)</Agency><Contact><Email>sismob@resif.fr</Email></Contact><WebSite>https://sismob.resif.fr</WebSite></Operator>'
                    );
        end loop;
        -- Par la suite, 'SisMob' ne fait jamais partie des patterns, donc on est tout bon.

        -- OCA
        -- RESIF-SISMOB, OCA
        -- GEOAZUR
        title:='Observatoire de la Côte d''Azur (OCA)';
        website:='https://www.oca.eu';
        email:='sismo-help@resif.fr';
        pattern:='(GEOAZUR|OCA)';
        op_to_delete := new_operator(pattern, title, website, email);
        raise notice 'Operator to delete: %', op_to_delete;
        delete from operator where operator_id = any(op_to_delete);
        --
        -- CEREMA
        -- RESIF-SISMOB, CEREMA
        title := 'Centre d''études et d''expertise sur les risques, l''environnement, la mobilité et l''aménagement (Cerema)';
        website := 'https://www.cerema.fr';
        email:='sismo-help@resif.fr';
        pattern:='(Cerema|CEREMA)';
        op_to_delete := new_operator(pattern, title, website, email);
        raise notice 'Operator to delete: %', op_to_delete;
        delete from operator where operator_id = any(op_to_delete);
        --
        -- Observatoire volcanologique du Piton de la Fournaise (OVPF, IPGP)
        -- Pour celui là, on scinde en 2 opérateurs, OVPF et IPGP
        pattern:='Observatoire volcanologique du Piton de la Fournaise \(OVPF, IPGP\)';
        title := 'Observatoire volcanologique du Piton de la Fournaise (OVPF)';
        website := 'https://www.ipgp.fr/ovpf';
        email:='';
        raise notice '%', pattern;
        op_to_delete := new_operator(pattern, title, website, email);

        title := 'Institut de physique du globe de Paris (IPGP)';
        website := 'https://www.ipgp.fr';
        email:='';
        op_to_delete := new_operator(pattern, title, website, email);
        raise notice 'Operator to delete: %', op_to_delete;
        delete from operator where operator_id = any(op_to_delete);
        --
        -- GEOMAR OBS facility (GEOMAR_OBS) >>> à regarder
        pattern:='GEOMAR OBS facility \(GEOMAR_OBS\)';
        title := 'GEOMAR Helmholtz Centre for Ocean Research Kiel (Geomar)';
        website := 'https://www.geomar.de';
        email := '';
        raise notice '%', pattern;
        op_to_delete := new_operator(pattern, title, website, email);
        delete from operator where operator_id = any(op_to_delete);
        --
        -- INSU-IPGP OBS facility (INSU-IPGP_OBS) >>> à regarder
        pattern:='INSU-IPGP OBS facility \(INSU-IPGP_OBS\)';
        title := 'Parc sismomètre fond de mer INSU/IPGP';
        website := 'https://parc-obs.insu.cnrs.fr';
        raise notice '%', pattern;
        op_to_delete := new_operator(pattern, title, website, email);
        delete from operator where operator_id = any(op_to_delete);
        --
        -- DEPAS OBS facility (DEPAS_OBS) >>> à regarder
        pattern:='DEPAS OBS facility \(DEPAS_OBS\)';
        title := 'The German Instrument Pool for Amphibian Seismology (DEPAS)';
        website := 'https://www.awi.de/en/science/geosciences/geophysics/methods-and-tools/ocean-bottom-seismometer/depas.html';
        email := '';
        raise notice '%', pattern;
        op_to_delete := new_operator(pattern, title, website, email);
        delete from operator where operator_id = any(op_to_delete);
        --
        -- VOLCARRAY temporary experiment (VOLCARRAY,ISTERRE,OVPF,IPGP,SNOV)
        pattern := 'VOLCARRAY';
        title := 'Institut des sciences de la Terre (ISTerre)';
        website := 'https://www.isterre.fr';
        raise notice '%', pattern;
        op_to_delete := new_operator(pattern, title, website, email);

        title := 'Observatoire volcanologique du Piton de la Fournaise (OVPF)';
        website := 'https://www.ipgp.fr/ovpf';
        op_to_delete := new_operator(pattern, title, website, email);

        title := 'Service d’Observation en Volcanologie (SNOV)';
        website := 'https://opgc.uca.fr/volcanologie';
        raise notice '%', pattern;
        op_to_delete := new_operator(pattern, title, website, email);

        title := 'Institut de physique du globe de Paris (IPGP)';
        website := 'https://www.ipgp.fr';
        email:='';
        op_to_delete := new_operator(pattern, title, website, email);
        delete from operator where operator_id = any(op_to_delete);

        -- Institut des Sciences de la Terre (ISTERRE)
        -- RESIF-SISMOB, ISTERRE
        -- RESIF-SISMOB, ISTerre
        -- Mouvements gravitaires (M-GRAVITAIRES,RESIF-SISMOB)
        pattern := '(ISTERRE|ISTerre|gravitaires)';
        title := 'Institut des sciences de la Terre (ISTerre)';
        website := 'https://www.isterre.fr';
        raise notice '%', pattern;
        op_to_delete := new_operator(pattern, title, website, email);
        delete from operator where operator_id = any(op_to_delete);

        --
        -- Observatoire des sciences de l'univers de Grenoble (OSUG)
        pattern := 'OSUG';
        title := 'Observatoire des sciences de l''Univers de Grenoble (Osug)';
        website := 'https://www.osug.fr';
        op_to_delete := new_operator(pattern, title, website, email);
        delete from operator where operator_id = any(op_to_delete);
        --
        -- RESIF-SISMOB, IPGS
        -- RESIF-SISMOB, IPGP >> ajouter l'IPGP
        pattern := 'RESIF-SISMOB, IPGP';
        title := 'Institut de physique du globe de Paris (IPGP)';
        website := 'https://www.ipgp.fr';
        email:='';
        op_to_delete := new_operator(pattern, title, website, email);
        raise notice 'Operator to delete: %', op_to_delete;
        delete from operator where operator_id = any(op_to_delete);
        --
        -- Ecole et Observatoire des Sciences de la Terre (EOST)
        pattern := 'EOST';
        title := 'École et observatoire des sciences de la Terre (EOST)';
        website := 'https://eost.unistra.fr';
        email:='';
        op_to_delete := new_operator(pattern, title, website, email);
        raise notice 'Operator to delete: %', op_to_delete;
        delete from operator where operator_id = any(op_to_delete);
        --
        -- OMIV
        pattern := 'OMIV';
        title := 'Observatoire multidisciplinaire des instabilités de versants (Omiv)';
        website := 'http://www.ano-omiv.cnrs.fr';
        email := '';
        op_to_delete := new_operator(pattern, title, website, email);
        delete from operator where operator_id = any(op_to_delete);
        --
        -- IRSN
        pattern := 'IRSN';
        title := 'Institut de radioprotection et de sûreté nucléaire (IRSN)';
        website := 'https://www.irsn.fr';
        email := '';
        op_to_delete := new_operator(pattern, title, website, email);
        delete from operator where operator_id = any(op_to_delete);
        --
        -- ALPARRAY
        pattern := 'ALPARRAY';
        title := 'AlpArray-FR (AlpArray)';
        website := 'https://www.isterre.fr/english/research-observation/research-projects/national-research-agency-projects-1163/article/alparray-fr.html';
        email := 'sismob@resif.fr';
        op_to_delete := new_operator(pattern, title, website, email);
        delete from operator where operator_id = any(op_to_delete);
        --
        --
        --
        -- Tous ceux-là, c'est à remplacer par sismob :
        -- W-ALPS 2004-2007 temporary experiment (RESIF-SISMO
        -- Guerrero Gap (Mexico) temporary array (RESIF-SISMO
        -- PLUME Polynesian Lithosphere and Upper Mantle temp
        -- HORN of AFRICA BB temporary experiment (RESIF-SISM
        -- LAPNET/POLENET seismic temporary array (RESIF-SISM
        -- ARC-VANUATU (volcanoes) temporary experiment (RESI
        -- ARLITA Temporary experiment in eastern Antartica (
        -- NERA-JRA1-A
        -- SIMBAAD Aegean-Anatolia temporary experiment - bac
        -- DORA Afar temporary experiment (RESIF-SISMOB)
        -- RESIF-SISMOB, LCE >> Mettre seulement SISMOB
        -- RESIF-SISMOB, EDF
        -- RESIF-SISMOB, IRSN
        -- RESIF-SISMOB
        -- RESIF - SISMOB mobile antenna
        -- Parc national d'instruments sismologiques mobiles terrestres de l'INSU (RESIF-SISMOB)
        delete from operator where operator ~ '(SISMOB|SIMBAAD|DORA|NERA|ARLITA|W-ALPS|Guerrero|PLUME|HORN|LAPNET|VANUATU)';

        -- SISMO DES ECOLES >>>>> À maintenir, et ajouter SISMOB donc ne rien lui faire
        -- IGGCAS - ISTERRE - INGV >> À ignorer, David renvoie la manip
        -- DP >> à renvoyer par David

    return op_to_delete;
    end;
$$ language plpgsql;

-- On corrige certaines fonctions pour ne plus faire appel à la réécriture
\ir ../SQL/resifinv-fonction.sql
-- On corrige tous les opérateurs
select migrate_gatito();
-- On supprime les fonctions qui ne serviront plus
drop function migrate_gatito();
drop function new_operator(text,text,text,text);
drop function if exists "operator-xml"(BIGINT, TEXT, TEXT, TEXT);
-- On rafraichit les vues qui contiennent des opérateurs
REFRESH MATERIALIZED VIEW CONCURRENTLY ws_network_xml;
REFRESH MATERIALIZED VIEW CONCURRENTLY ws_station_xml;

commit;
