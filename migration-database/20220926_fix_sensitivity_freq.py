#!/usr/bin/env python3

# Ce script a été lancé en prod le 26/09
# Il corrige la valeur de frequency dans la table sensitivity à partir du morceau de XML

import xml.etree.ElementTree as ET
import psycopg2

# Activer la ligne suivante pour passer en prod
# dburi = "postgresql://resifinvprod@resif-pgprod.u-ga.fr/resifInv-Prod"

with psycopg2.connect(dburi) as conn:
    with conn.cursor() as curs:
        curs.execute("select sensitivity_id,xml from sensitivity")
        for sens_id,sens_xml in curs.fetchall():
            try:
                tree = ET.fromstring(sens_xml)
                freq = float(tree.find('./Frequency').text)
                curs2 = conn.cursor()

                curs2.execute("update sensitivity set frequency = %s where sensitivity_id = %s", (freq, sens_id))
            except Exception as e:
                continue
        curs.execute("refresh materialized view ws_channel_text")
