 -- creation de la table contenant les fichiers au format PH5
 -- pour importer

DROP TABLE IF exists rph5 cascade;
CREATE TABLE rph5 (
  rph5_id      BIGSERIAL PRIMARY KEY,
  network_id   bigint default 0 REFERENCES networks(network_id) ON DELETE SET DEFAULT,
  station_id   bigint default 0 REFERENCES station(station_id) ON DELETE SET DEFAULT,
  channel_id   bigint default 0 REFERENCES channel(channel_id) ON DELETE SET DEFAULT,
  source_file  text,
  network      varchar(10),
  station      varchar(10),             
  location     varchar(10) default '',         
  channel      varchar(10),  
  starttime    timestamp without time zone,
  endtime      timestamp without time zone,
  iarray       varchar(10),  
  quality      character varying(1) default 'D',
  availability boolean default false,
  updated_at   timestamp without time zone default now();
);
