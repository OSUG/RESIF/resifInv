/*
Date : 26 mars 2019

Création d'une table à l'usage du webservice /auth pour l'authentification EIDA.

1. Renommage de la table aut_user -> resif_users

2. Création d'une table eida_temp_users 

3. Création d'une vue 'aut_user' comme l'union de resif_users et eida_temp_users avec les même champs que la table aut_user initiale, sans la colonne user_idx

4. Mettre les droits sur aut_user

Script à exécuter en tant que resifInv-(Dev|Prod)
*/


BEGIN;

ALTER TABLE aut_user RENAME TO resif_users;

CREATE TABLE eida_temp_users (network_id bigint, network text, start_year integer, end_year integer, name text);

GRANT ALL ON TABLE eida_temp_users TO eidawsauth;

CREATE VIEW aut_user AS SELECT a.network_id, a.network, a.start_year, a.end_year, a.name FROM resif_users a UNION SELECT b.network_id, b.network, b.start_year, b.end_year, b.name FROM eida_temp_users b;

END;

-- passer sur la base de donnée resifAuth, en tant que resifAuth

ALTER TABLE users ADD COLUMN expires_at timestamp;

