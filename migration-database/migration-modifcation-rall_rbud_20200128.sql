/*
Mise a jour de resifInv-Prod table rall et rbud
*/

alter table rall drop column availability cascade;
alter table rbud drop column availability cascade;

alter table rall add column availability BOOLEAN default TRUE;
alter table rbud add column availability BOOLEAN default TRUE;

CREATE OR REPLACE FUNCTION set_data_availability (TEXT, INTEGER) RETURNS BOOLEAN AS $$
-- un fichier herite de la policy du network auquel il appartient
	DECLARE 
	net		   ALIAS FOR $1;		-- code reseau FDSN de la station
	annee			ALIAS FOR $2;				-- annnée de la date de début de fichier
	avl  			BOOLEAN ;				-- true if open data
	status		TEXT;
	BEGIN
	avl='false';
	select distinct n.policy 
					from networks n
					where
					n.network = net and 
					annee between n.start_year and n.end_year
	INTO status;
	case WHEN (status = 'open') THEN 
			avl := 'true'; 
	ELSE avl:='false';
	END CASE ;
	return avl;
	END; $$
LANGUAGE 'plpgsql';

update rall set availability = set_data_availability (network, year) ;
update rbud set availability = set_data_availability (network, year );

CREATE OR REPLACE FUNCTION public.set_channel_id(text, text, text, text, timestamp without time zone, timestamp without time zone)  RETURNS bigint AS $$
	DECLARE 
	net		ALIAS FOR $1;		-- code reseau FDSN de la station
	sta	ALIAS FOR $2;		-- code station
	loc		ALIAS FOR $3;		-- location
	chan		ALIAS FOR $4;		-- channel
	cstart	ALIAS FOR $5;		-- une date
	cend		ALIAS FOR $6;		-- date d'arret du channel
	id  		BIGINT ;					-- station_id pour ce channel
	idt		BIGINT ;
	BEGIN
	idt  := (
			select DISTINCT c.channel_id 
			FROM ws_common c
			WHERE 
			(c.channel = chan and
			c.location = loc and 
			c.station = sta and
			c.network = net and 
			cstart BETWEEN c.cstarttime AND c.cendtime)
			OR
			(c.channel = chan and
			c.location = loc and 
			c.station = sta and
			c.network = net and 
			cend BETWEEN c.cstarttime AND c.cendtime)
			ORDER BY c.channel_id DESC
			LIMIT 1);
	IF (idt IS NULL) THEN id:=0; ELSE id:=idt; END IF;
	RETURN id;
	END; 
$$  LANGUAGE 'plpgsql';

update rall set channel_id = set_channel_id (network, station, location, channel, starttime, endtime);
update rbud set channel_id = set_channel_id (network, station, location, channel, starttime, endtime);
