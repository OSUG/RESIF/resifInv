avertir le comtec de possibles retards dans l intégration des données et de possibles instabilités au portail et aux WEBservices
stopper l intégration sur resif-vm39
stopper le inotify sur resif-vm34

CP : mettre a jour repair et inotify sur resif-vm34
CP verifier les fichiers .pgpass sur vm38, vm39, vm34

creer le role 

-------------------------------------------------------
CREATE ROLE resifinvprod PASSWORD 'systun50SER' + LOGIN
-------------------------------------------------------

cloner resifInv-prod2 

-------------------------------------------------------
dropdb resifInv-Prod;
CREATE DATABASE "resifInv-Prod" TEMPLATE "resifInv-prod2";
ALTER DATABASE "resifinv-Prod" OWNER TO resifinvprod

psql "resifInv-Prod"

-- doit etre fait par le role postgres           

ALTER TABLE  "R_ALL_1" OWNER TO resifinvprod;
ALTER TABLE  "R_Bud_1" OWNER TO resifinvprod;
ALTER TABLE  "admin-file-rall-per-year" OWNER TO resifinvprod;
ALTER TABLE  "admin-file-to-check"     OWNER TO resifinvprod;
ALTER TABLE  "admin-file-to-delete"    OWNER TO resifinvprod;
ALTER TABLE  "aut_user" OWNER TO resifinvprod;
ALTER TABLE  "aut_userIn"              OWNER TO resifinvprod;
ALTER TABLE  aut_user_1              OWNER TO resifinvprod;
ALTER sequence  aut_user_user_id_seq    OWNER TO resifinvprod;
ALTER TABLE  blk30   OWNER TO resifinvprod;
ALTER sequence  blk30_stock_id_seq    OWNER TO resifinvprod;
ALTER TABLE  blk31   OWNER TO resifinvprod;
ALTER sequence  blk31_comment_id_seq       OWNER TO resifinvprod;
ALTER TABLE  blk33   OWNER TO resifinvprod;
ALTER sequence  blk33_dico_id_seq          OWNER TO resifinvprod;
ALTER TABLE  blk34   OWNER TO resifinvprod;
ALTER sequence  blk34_unit_id_seq          OWNER TO resifinvprod;
ALTER TABLE  blk51_1 OWNER TO resifinvprod;
ALTER TABLE  blk59_1 OWNER TO resifinvprod;
ALTER TABLE  channel OWNER TO resifinvprod;
ALTER view  "channelComment"     OWNER TO resifinvprod;
ALTER TABLE  channel_1               OWNER TO resifinvprod;
ALTER TABLE  channel_comment         OWNER TO resifinvprod;
ALTER sequence  channel_comment_channel_comment_id_seq  OWNER TO resifinvprod;
ALTER sequence  channel_id_seq                         OWNER TO resifinvprod;
ALTER TABLE  "clean_DE_MDE"            OWNER TO resifinvprod;
ALTER TABLE  "clean_DI_MDI"            OWNER TO resifinvprod;
ALTER  view    comment                   OWNER TO resifinvprod;
ALTER TABLE  evalresp_parameter      OWNER TO resifinvprod;
ALTER TABLE  file_to_delete          OWNER TO resifinvprod;
ALTER TABLE  geology OWNER TO resifinvprod;
ALTER TABLE  "geologyIn"              OWNER TO resifinvprod;
ALTER SEQUENCE  geology_geolid_seq      OWNER TO resifinvprod;
ALTER TABLE  institution             OWNER TO resifinvprod;
ALTER TABLE  "institutionIn"           OWNER TO resifinvprod;
ALTER TABLE  int_user OWNER TO resifinvprod;
ALTER materialized view  metachannel              OWNER TO resifinvprod;
ALTER materialized view  metanetwork                            OWNER TO resifinvprod;
ALTER materialized view  metaresponse                            OWNER TO resifinvprod;
ALTER materialized view  metastation                             OWNER TO resifinvprod;
ALTER TABLE  "netOperators"            OWNER TO resifinvprod;
ALTER TABLE  "netOperatorsIn"          OWNER TO resifinvprod;
ALTER TABLE  net_operators           OWNER TO resifinvprod;
ALTER sequence  network_id_seq          OWNER TO resifinvprod;
ALTER TABLE  networks OWNER TO resifinvprod;
ALTER TABLE  "networksIn"              OWNER TO resifinvprod;
ALTER TABLE  networks_1              OWNER TO resifinvprod;
ALTER view  networkview OWNER TO resifinvprod;
ALTER TABLE  rall    OWNER TO resifinvprod;
ALTER sequence  rall_id_seq   OWNER TO resifinvprod;
ALTER TABLE  rbud    OWNER TO resifinvprod;
ALTER sequence  rbud_id_seq    OWNER TO resifinvprod;
ALTER TABLE  "removeNetwork"           OWNER TO resifinvprod;
ALTER TABLE  "respIn"  OWNER TO resifinvprod;
ALTER TABLE  resp_1  OWNER TO resifinvprod;
ALTER TABLE  response OWNER TO resifinvprod;
ALTER sequence  response_id_seq       OWNER TO resifinvprod;
ALTER sequence  s1                    OWNER TO resifinvprod;
ALTER TABLE  station OWNER TO resifinvprod;
ALTER view  "stationComment"       OWNER TO resifinvprod;
ALTER TABLE  station_1               OWNER TO resifinvprod;
ALTER TABLE  station_comment         OWNER TO resifinvprod;
ALTER sequence  station_comment_station_comment_id_seq OWNER TO resifinvprod;
ALTER sequence  station_id_seq     OWNER TO resifinvprod;
ALTER TABLE  temp_scan_bud           OWNER TO resifinvprod;
ALTER TABLE  temp_xnetwork           OWNER TO resifinvprod;
ALTER sequence  transaction_id_seq   OWNER TO resifinvprod;
ALTER TABLE  transaction_integration OWNER TO resifinvprod;
ALTER TABLE  vault   OWNER TO resifinvprod;
ALTER TABLE  "vaultIn" OWNER TO resifinvprod;
ALTER sequence vault_vaultid_seq    OWNER TO resifinvprod;


ALTER function "getDecoderKeyNumber"(integer) OWNER TO resifinvprod;
ALTER function  "isint"(text) OWNER TO resifinvprod;
ALTER function  "maxdateStation"(character varying, character varying, real, real, real, text, character varying, character varying, character varying, text) OWNER TO resifinvprod;
ALTER function  "mindateStation"(character varying, character varying, real, real, real, text, character varying, character varying, character varying, text) OWNER TO resifinvprod;

ALTER function "mindatechan"(character varying) OWNER TO resifinvprod;
ALTER function  "network-xml"(text,integer,integer) OWNER TO resifinvprod;
ALTER function  "network-xml"(bigint) OWNER TO resifinvprod;
ALTER function  "nextStatChannelComment"(integer) OWNER TO resifinvprod;
ALTER function  "isyear"(text) OWNER TO resifinvprod;
ALTER function  "nextStatStation"(integer) OWNER TO resifinvprod;
ALTER function  "nextStatStationComment"(integer) OWNER TO resifinvprod;
ALTER function  "set-request-type"() OWNER TO resifinvprod;
ALTER function  "setMinAvailability"(text,integer,text,text,text,text) OWNER TO resifinvprod;
ALTER function  "set_endChannelDataAvailability"(text,text,text,text) OWNER TO resifinvprod;
ALTER function  "set_endChannelDataAvailability"(text,text,text,text,text) OWNER TO resifinvprod;
ALTER function  "set_endChannelDataAvailability"(text,text,text,text,integer) OWNER TO resifinvprod;
ALTER function  "set_endChannelDataAvailability"(text,text,text,text,integer,text) OWNER TO resifinvprod;
ALTER function  "reverse"(text) OWNER TO resifinvprod;
ALTER function  "isjulianday"(text) OWNER TO resifinvprod;
ALTER function  "set_endtime_for_network"(text,integer) OWNER TO resifinvprod;
ALTER function  "set_startChannelDataAvailability"(text, text, text, text) OWNER TO resifinvprod;
ALTER function  "set_startChannelDataAvailability"(text, text, text, text,text) OWNER TO resifinvprod;
ALTER function  "set_startChannelDataAvailability" (text, text, text, text, integer, text) OWNER TO resifinvprod;
ALTER function  "set_startChannelDataAvailability"(text, text, text, integer,text) OWNER TO resifinvprod;
ALTER function  "set_startChannelDataAvailability"(text, text, text, text,integer) OWNER TO resifinvprod;
ALTER function  "set_startime_for_network"(text,integer) OWNER TO resifinvprod;
ALTER function  set_station_id (text, text, text, text, timestamp without time zone, timestamp without time zone) OWNER TO resifinvprod;
ALTER function  "station-comment-xml"(text,text) OWNER TO resifinvprod;
ALTER function  "totalNumberChanStat"(text, text) OWNER TO resifinvprod;
ALTER function  "basename"(text, character varying) OWNER TO resifinvprod;
ALTER function  "sum_stations_in_network"(bigint) OWNER TO resifinvprod;
ALTER function  "sum_stations_comment"(bigint) OWNER TO resifinvprod;
ALTER function  "sum_channels_in_station"(bigint,bigint) OWNER TO resifinvprod;
ALTER function  "sum_channel_comment"(bigint) OWNER TO resifinvprod;
ALTER function  "distanceEpicentrale_deg"(ouble precision, double precision, double precision, double precision) OWNER TO resifinvprod;
ALTER function  "geology-xml"(integer, integer, text, integer, text) OWNER TO resifinvprod;
ALTER function  "vault-xml"(nteger, integer, text, integer, text) OWNER TO resifinvprod;
ALTER function  "normalize-agency-xml"(text) OWNER TO resifinvprod;
ALTER function  "normalize-owner-xml"(text) OWNER TO resifinvprod;
ALTER function  "normalize-site-xml"(text) OWNER TO resifinvprod;
ALTER function  "get_transaction_id"(text) OWNER TO resifinvprod;
ALTER function  "format_location"(text) OWNER TO resifinvprod;
ALTER function  "network-xml"(bigint) OWNER TO resifinvprod;
ALTER function  "network-txt"(bigint) OWNER TO resifinvprod;
ALTER function  "station-txt"(bigint) OWNER TO resifinvprod;
ALTER function  "channel-txt"(bigint) OWNER TO resifinvprod;
ALTER function  "station-xml11"(bigint) OWNER TO resifinvprod;
ALTER function  "station-xml1"(bigint) OWNER TO resifinvprod;
ALTER function  "station-xml2"(bigint) OWNER TO resifinvprod;
ALTER function  "channel-xml1"(bigint) OWNER TO resifinvprod;
ALTER function  "channel-xml2"(bigint) OWNER TO resifinvprod;
ALTER function  "channel-xml3"(bigint) OWNER TO resifinvprod;
ALTER function "set_network_id"(text, text, timestamp without time zone, timestamp without time zone) OWNER TO resifinvprod;
ALTER function  "set_station_id"(text, text, text, text, timestamp without time zone, timestamp without time zone) OWNER TO resifinvprod;
ALTER function  "set_channel_id"(text, text, text, text, timestamp without time zone, timestamp without time zone) OWNER TO resifinvprod;
ALTER function  "set_network_id_bis"(text, integer, integer) OWNER TO resifinvprod;
ALTER function  "set_channel_id_in_file_table"(text, text, text, text, timestamp without time zone, timestamp without time zone) OWNER TO resifinvprod;

ALTER function  "agencyNomenclatureXml"(text,text) OWNER TO resifinvprod;
ALTER function  "distanceEpicentrale_deg"(double precision, double precision, double precision, double precision) OWNER TO resifinvprod;
ALTER function  "vault-xml" (integer, integer, text, integer, text) OWNER TO resifinvprod;

-------------------------------------------------------
CP vérifier rapidement les owners sur PgAdmin

sur resif-vm38 :
stopper le tomcat ?
cd ws-extract-scripts
git pull
relancer tomcat.

CP détruire à la mano les schema reztant dans resifInv-Prod et qui appartiendrait à bdsis
CP : lancer le ws-test sur resif-vm40 avec runmode = production


sur resif-vm34 : relancer l intégration continue, checker, lancer repair

sur resif-vm39 : relancer le worker

CP : annuler la double integration continue sur resif-vm34 avant de détruire resifInv-prod2
