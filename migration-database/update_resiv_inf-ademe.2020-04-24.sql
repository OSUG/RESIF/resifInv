-- 2020-04-24 : remise a jour des id dans les tables rall et rbud pour les station de l'ademe, après réinjections massive des stations.xml
--

update rall set channel_id = set_channel_id (network,station,location,channel,starttime,endtime) where station = 'WALT';
update rall set station_id = set_station_id (network,station,starttime,endtime) where station = 'WALT';
update rall set network_id = set_network_id (network,starttime,endtime) where station = 'WALT';

update rall set channel_id = set_channel_id (network,station,location,channel,starttime,endtime) where station = 'VOEL';
update rall set station_id = set_station_id (network,station,starttime,endtime) where station = 'VOEL';
update rall set network_id = set_network_id (network,starttime,endtime) where station = 'VOEL';

update rall set channel_id = set_channel_id (network,station,location,channel,starttime,endtime) where station = 'VDH4';
update rall set station_id = set_station_id (network,station,starttime,endtime) where station = 'VDH4';
update rall set network_id = set_network_id (network,starttime,endtime) where station = 'VDH4';

update rall set channel_id = set_channel_id (network,station,location,channel,starttime,endtime) where station = 'OPS';
update rall set station_id = set_station_id (network,station,starttime,endtime) where station = 'OPS';
update rall set network_id = set_network_id (network,starttime,endtime) where station = 'OPS';

update rall set channel_id = set_channel_id (network,station,location,channel,starttime,endtime) where station = 'NEEW';
update rall set station_id = set_station_id (network,station,starttime,endtime) where station = 'NEEW';
update rall set network_id = set_network_id (network,starttime,endtime) where station = 'NEEW';


update rall set channel_id = set_channel_id (network,station,location,channel,starttime,endtime) where station = 'ILLK';
update rall set station_id = set_station_id (network,station,starttime,endtime) where station = 'ILLK';
update rall set network_id = set_network_id (network,starttime,endtime) where station = 'ILLK';

update rall set channel_id = set_channel_id (network,station,location,channel,starttime,endtime) where station = 'ILLF';
update rall set station_id = set_station_id (network,station,starttime,endtime) where station = 'ILLF';
update rall set network_id = set_network_id (network,starttime,endtime) where station = 'ILLF';

update rall set channel_id = set_channel_id (network,station,location,channel,starttime,endtime) where station = 'HOHE';
update rall set station_id = set_station_id (network,station,starttime,endtime) where station = 'HOHE';
update rall set network_id = set_network_id (network,starttime,endtime) where station = 'HOHE';

update rall set channel_id = set_channel_id (network,station,location,channel,starttime,endtime) where station = 'CIEL';
update rall set station_id = set_station_id (network,station,starttime,endtime) where station = 'CIEL';
update rall set network_id = set_networl_id (network,starttime,endtime) where station = 'CIEL';

update rall set channel_id = set_channel_id (network,station,location,channel,starttime,endtime) where station = 'BETS';
update rall set station_id = set_station_id (network,station,starttime,endtime) where station = 'BETS';
update rall set network_id = set_network_id (network,starttime,endtime) where station = 'BETS';


update rbud set channel_id = set_channel_id (network,station,location,channel,starttime,endtime) where station = 'WALT';
update rbud set station_id = set_station_id (network,station,starttime,endtime) where station = 'WALT';
update rbud set network_id = set_network_id (network,starttime,endtime) where station = 'WALT';

update rbud set channel_id = set_channel_id (network,station,location,channel,starttime,endtime) where station = 'VOEL';
update rbud set station_id = set_station_id (network,station,starttime,endtime) where station = 'VOEL';
update rbud set network_id = set_network_id (network,starttime,endtime) where station = 'VOEL';

update rbud set channel_id = set_channel_id (network,station,location,channel,starttime,endtime) where station = 'VDH4';
update rbud set station_id = set_station_id (network,station,starttime,endtime) where station = 'VDH4';
update rbud set network_id = set_network_id (network,starttime,endtime) where station = 'VDH4';

update rbud set channel_id = set_channel_id (network,station,location,channel,starttime,endtime) where station = 'OPS';
update rbud set station_id = set_station_id (network,station,starttime,endtime) where station = 'OPS';
update rbud set network_id = set_network_id (network,starttime,endtime) where station = 'OPS';

update rbud set channel_id = set_channel_id (network,station,location,channel,starttime,endtime) where station = 'NEEW';
update rbud set station_id = set_station_id (network,station,starttime,endtime) where station = 'NEEW';
update rbud set network_id = set_network_id (network,starttime,endtime) where station = 'NEEW';


update rbud set channel_id = set_channel_id (network,station,location,channel,starttime,endtime) where station = 'ILLK';
update rbud set station_id = set_station_id (network,station,starttime,endtime) where station = 'ILLK';
update rbud set network_id = set_network_id (network,starttime,endtime) where station = 'ILLK';

update rbud set channel_id = set_channel_id (network,station,location,channel,starttime,endtime) where station = 'ILLF';
update rbud set station_id = set_station_id (network,station,starttime,endtime) where station = 'ILLF';
update rbud set network_id = set_network_id (network,starttime,endtime) where station = 'ILLF';

update rbud set channel_id = set_channel_id (network,station,location,channel,starttime,endtime) where station = 'HOHE';
update rbud set station_id = set_station_id (network,station,starttime,endtime) where station = 'HOHE';
update rbud set network_id = set_network_id (network,starttime,endtime) where station = 'HOHE';

update rbud set channel_id = set_channel_id (network,station,location,channel,starttime,endtime) where station = 'CIEL';
update rbud set station_id = set_station_id (network,station,starttime,endtime) where station = 'CIEL';
update rbud set network_id = set_network_id (network,starttime,endtime) where station = 'CIEL';

update rbud set channel_id = set_channel_id (network,station,location,channel,starttime,endtime) where station = 'BETS';
update rbud set station_id = set_station_id (network,station,starttime,endtime) where station = 'BETS';
update rbud set network_id = set_network_id (network,starttime,endtime) where station = 'BETS';


-- pur terminer, reindexer les deux tables
reindex table rall;
reindex table rbud;


