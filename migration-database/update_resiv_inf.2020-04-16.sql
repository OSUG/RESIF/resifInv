-- modification de la vue : ws_station-xml pour ajout d'une balise ExternalReference
-- cf resifInv/SQL/resif-view.sql

BEGIN work;

CREATE OR REPLACE FUNCTION "reference-xml" (BIGINT, TEXT) RETURNS TEXT AS $$
	DECLARE
	cid ALIAS FOR $1;		-- id for a network, a station
	clevel ALIAS FOR $2; --'n', 's', 'c'
	xml TEXT; -- RETURN
	creference external_reference%ROWTYPE; 
	BEGIN
	xml:='';
	CASE 	
		WHEN  (clevel = 'n')  THEN
 		 	FOR creference IN SELECT * FROM external_reference WHERE  external_reference.network_id  =  cid and external_reference.level = 'n'
				LOOP
					xml := xml||  creference.xml ;
				END LOOP;
		WHEN (clevel = 's')  THEN
 		 	FOR creference IN SELECT * FROM external_reference WHERE external_reference.station_id  =  cid and external_reference.level = 's' 
				LOOP
					xml := xml|| creference.xml ;
				END LOOP;
		ELSE  	-- (clevel = 'c')
 		 	FOR creference IN SELECT * FROM external_reference WHERE external_reference.channel_id  =  cid and  external_reference.level = 'c' 
				LOOP
					xml := xml || creference.xml ;
				END LOOP ;
		END CASE;
		RETURN xml;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION "station-xml22" (BIGINT) RETURNS TEXT AS $$
DECLARE 
staid	ALIAS FOR $1;	
xml TEXT; 
BEGIN
xml := '<ExternalReference>' || "reference-xml" (staid, 's') || '</ExternalReference>';
RETURN xml;
END; 
$$ LANGUAGE 'plpgsql';

DROP MATERIALIZED VIEW if exists  ws_station_xml;
CREATE MATERIALIZED VIEW  ws_station_xml  AS SELECT DISTINCT
w.channel_id,
-- "network-xml" (w.network_id)::TEXT AS xmlnet1,
"station-xml1"(w.station_id)::TEXT AS xmlstat1,
"station-xml11"(w.station_id)::TEXT AS xmlstat11,
"station-xml22"(w.station_id)::TEXT AS xmlstat22
FROM ws_common w
WHERE "station-xml1"(w.station_id)::TEXT != '';

select * from ws_station_xml limit 10;

end;
