"""
Add column expires_at in eida_temp_users
"""

from yoyo import step

steps = [
    step("alter table eida_temp_users add column if not exists expires_at timestamp default null",
    "alter table eida_temp_users drop column expires_at")
]
